package grand.shopness.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;


import timber.log.Timber;

public class MyAppInfo {
    public void getScreenSize(Activity context){
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Timber.d("width: "+ width + "px");
        Timber.d("height" + height + "px");
    }
}
