package grand.shopness.util;

import android.app.Activity;
import android.content.Context;

import com.forms.sti.progresslitieigb.ProgressLoadingJIGB;

public class MyAnimation {
    private Activity mContext;

    private MyAnimation(Activity mContext) {
        this.mContext = mContext;
    }

    public static MyAnimation getInstance(Activity mContext) {
        return new MyAnimation(mContext);
    }

    public void startAnimFullScreen(Integer src, String msg, Integer duration) {
        ProgressLoadingJIGB.startLoadingJIGB(mContext, src, msg, duration);
    }

    public void stopAnimation() {
        ProgressLoadingJIGB.finishLoadingJIGB(mContext);
    }
}
