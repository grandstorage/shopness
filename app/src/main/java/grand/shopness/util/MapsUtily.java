package grand.shopness.util;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import io.reactivex.disposables.Disposable;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import timber.log.Timber;

public class MapsUtily {

    GoogleMap mMap;

    private MapsUtily(GoogleMap mMap) {
        this.mMap = mMap;
    }

    public static MapsUtily getInstance(GoogleMap mMap) {
        return new MapsUtily(mMap);
    }

    public void showCurrentLocationBtn(boolean show) {
        try {
            if (show)
                mMap.setMyLocationEnabled(true);
            else
                mMap.setMyLocationEnabled(false);
        } catch (SecurityException e) {
            Timber.e(e);
        }
    }

    public void showZoomBtn(boolean show) {
        if (show)
            mMap.getUiSettings().setZoomControlsEnabled(true);
        else
            mMap.getUiSettings().setZoomControlsEnabled(false);
    }

    public void showCompassBtn(boolean show) {
        if (show)
            mMap.getUiSettings().setCompassEnabled(true);
        else
            mMap.getUiSettings().setCompassEnabled(false);
    }


}
