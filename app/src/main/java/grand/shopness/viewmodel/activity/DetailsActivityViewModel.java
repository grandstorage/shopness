package grand.shopness.viewmodel.activity;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;

public class DetailsActivityViewModel extends BaseViewModel {


    public void onFilterClick(){
        setValue(Codes.FILTER_SCREEN);
    }


    public void onSearchClick(){
        setValue(Codes.TOOLBAR_SEARCH_CLICK);
    }


}
