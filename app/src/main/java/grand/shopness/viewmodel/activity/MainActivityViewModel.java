package grand.shopness.viewmodel.activity;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.model.mainactivity.MainActivityItem;

public class MainActivityViewModel extends BaseViewModel {
    private MainActivityItem mainActivityItem;


    public MainActivityViewModel() {
        this.mainActivityItem = new MainActivityItem();
        setValue(Codes.HOME_SCREEN);
        setItems();
    }

    private void setItems() {
        if (UserPreferenceHelper.isLogged()) {
            mainActivityItem.setName(UserPreferenceHelper.getUserLoginDetails().getName());
            mainActivityItem.setEmail(UserPreferenceHelper.getUserLoginDetails().getEmail());
            mainActivityItem.setImage(UserPreferenceHelper.getUserLoginDetails().getUserImage());
        }
        notifyChange();
    }

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, String imagePath) {
        ConnectionHelper.loadImage(imageView,  imagePath);
    }

    @Bindable
    public MainActivityItem getMainActivityItem() {
        return mainActivityItem;
    }

    public void onSearchClick() {
        setValue(Codes.TOOLBAR_SEARCH_CLICK);
    }

}