package grand.shopness.viewmodel;

import java.util.concurrent.TimeUnit;

import grand.shopness.application.MyApplication;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class SplashScreenViewModel extends BaseViewModel {
    private Disposable disposable;

    public SplashScreenViewModel() {

    }

    public void startApp() {
        disposable = Completable.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe(this::doAfterDelay);
    }

    private void doAfterDelay() {
        if(UserPreferenceHelper.isLogged()) {
            setValue(Codes.HOME_SCREEN);
        }else {
            setValue(Codes.START_SCREEN);
        }
        disposable.dispose();
    }

}
