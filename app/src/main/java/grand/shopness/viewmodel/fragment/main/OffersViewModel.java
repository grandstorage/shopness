package grand.shopness.viewmodel.fragment.main;

import android.annotation.SuppressLint;
import android.location.LocationManager;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.Objects;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.offers.OfferRequest;
import grand.shopness.model.offers.OffersResponse;
import grand.shopness.view.adapter.parent.OffersAdapter;
import io.reactivex.disposables.Disposable;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import timber.log.Timber;

public class OffersViewModel extends BaseViewModel {

    private OffersAdapter offersAdapter;
    private OffersResponse offersResponse;
    private OfferRequest offerRequest;
    public ObservableBoolean isLoading = new ObservableBoolean();
    private Disposable disposable;
    private ReactiveLocationProvider locationProvider;

    public OffersViewModel() {
        offersResponse = new OffersResponse();
        offersAdapter = new OffersAdapter();
        offerRequest = new OfferRequest();
    }

    public void setLocationProvider(ReactiveLocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    @SuppressLint("MissingPermission")
    public void startPopulateData() {
        if (!UserPreferenceHelper.isLogged()) {
            disposable = locationProvider.getLastKnownLocation()
                    .subscribe(location -> {
                        Timber.e("location: %s", location);
                        offerRequest.setLat(location.getLatitude());
                        offerRequest.setLng(location.getLongitude());
                        getData();
                        disposable.dispose();
                    });
        } else {
            offerRequest.setLat(Double.parseDouble(UserPreferenceHelper.getUserLoginDetails().getLat()));
            offerRequest.setLng(Double.parseDouble(UserPreferenceHelper.getUserLoginDetails().getLng()));
            getData();
        }
    }

    private void getData() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                offersResponse = ((OffersResponse) response);
                switch (offersResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getOffersAdapter().updateDataList(offersResponse.getData().getAllOfferShops());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(offersResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.GET_OFFERS, offerRequest, OffersResponse.class);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }

    @BindingAdapter({"adapter"})
    public static void getOfferAdapterBinding(RecyclerView recyclerView, OffersAdapter offersAdapter) {
        //you can set layout manager here
        ((GridLayoutManager) Objects.requireNonNull(recyclerView.getLayoutManager()))
                .setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        switch (offersAdapter.getItemViewType(position)) {
                            case 0: //header
                                return 2;
                            case 1: //item
                                return 1;
                            default:
                                return 1;
                        }
                    }
                });
        recyclerView.setAdapter(offersAdapter);
    }


    @Bindable
    public OffersAdapter getOffersAdapter() {
        return this.offersAdapter == null ? this.offersAdapter = new OffersAdapter() : this.offersAdapter;
    }


}
