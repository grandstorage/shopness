package com.routh.model.shops.details;

import com.google.gson.annotations.SerializedName;

public class ProductsItem{

	@SerializedName("product_desc")
	private String productDesc;

	@SerializedName("is_food")
	private int isFood;

	@SerializedName("price")
	private int price;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("product_id")
	private int productId;

	@SerializedName("product_name")
	private String productName;



	public void setProductDesc(String productDesc){
		this.productDesc = productDesc;
	}

	public String getProductDesc(){
		return productDesc;
	}

	public void setIsFood(int isFood){
		this.isFood = isFood;
	}

	public int getIsFood(){
		return isFood;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@Override
 	public String toString(){
		return 
			"ProductsItem{" + 
			"product_desc = '" + productDesc + '\'' + 
			",is_food = '" + isFood + '\'' + 
			",price = '" + price + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",product_id = '" + productId + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}";
		}
}