package com.routh.model.shops;

import com.google.gson.annotations.SerializedName;

public class AllShopsItem{

	@SerializedName("address")
	private String address;

	@SerializedName("arrival_time")
	private String arrivalTime;

	@SerializedName("distance")
	private int distance;

	@SerializedName("shipping")
	private String shipping;

	@SerializedName("mini_charge")
	private String miniCharge;

	@SerializedName("id")
	private int id;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("shop_image")
	private String shopImage;

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setArrivalTime(String arrivalTime){
		this.arrivalTime = arrivalTime;
	}

	public String getArrivalTime(){
		return arrivalTime;
	}

	public void setDistance(int distance){
		this.distance = distance;
	}

	public int getDistance(){
		return distance;
	}

	public void setShipping(String shipping){
		this.shipping = shipping;
	}

	public String getShipping(){
		return shipping;
	}

	public void setMiniCharge(String miniCharge){
		this.miniCharge = miniCharge;
	}

	public String getMiniCharge(){
		return miniCharge;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setShopName(String shopName){
		this.shopName = shopName;
	}

	public String getShopName(){
		return shopName;
	}

	public void setShopImage(String shopImage){
		this.shopImage = shopImage;
	}

	public String getShopImage(){
		return shopImage;
	}

	@Override
 	public String toString(){
		return 
			"AllShopsItem{" + 
			"address = '" + address + '\'' + 
			",arrival_time = '" + arrivalTime + '\'' + 
			",distance = '" + distance + '\'' + 
			",shipping = '" + shipping + '\'' + 
			",mini_charge = '" + miniCharge + '\'' + 
			",id = '" + id + '\'' + 
			",shop_name = '" + shopName + '\'' + 
			",shop_image = '" + shopImage + '\'' + 
			"}";
		}
}