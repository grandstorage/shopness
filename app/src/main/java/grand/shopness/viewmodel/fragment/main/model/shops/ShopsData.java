package com.routh.model.shops;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ShopsData{

	@SerializedName("AllShops")
	private List<AllShopsItem> allShops;

	public void setAllShops(List<AllShopsItem> allShops){
		this.allShops = allShops;
	}

	public List<AllShopsItem> getAllShops(){
		return allShops;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"allShops = '" + allShops + '\'' + 
			"}";
		}
}