package com.routh.model.sendrequest;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendRequestRequest implements Serializable {

    @SerializedName("sender_user_name")
    private String senderName;
    @SerializedName("sender_location")
    private String senderLocation;
    @SerializedName("sender_phone")
    private String senderPhone;
    @SerializedName("sender_lat")
    private double senderLat;
    @SerializedName("sender_lng")
    private double senderLng;
    @SerializedName("receive_user_name")
    private String receiveName;
    @SerializedName("receive_location")
    private String receiveLocation;
    @SerializedName("receive_phone")
    private String receivePhone;
    @SerializedName("receive_lat")
    private double receiveLat;
    @SerializedName("receive_lng")
    private double receiveLng;
    @SerializedName("order_details")
    private String orderDetails;
    @SerializedName("cost")
    private String cost;

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderLocation() {
        return senderLocation;
    }

    public void setSenderLocation(String senderLocation) {
        this.senderLocation = senderLocation;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public double getSenderLat() {
        return senderLat;
    }

    public void setSenderLat(double senderLat) {
        this.senderLat = senderLat;
    }

    public double getSenderLng() {
        return senderLng;
    }

    public void setSenderLng(double senderLng) {
        this.senderLng = senderLng;
    }

    public String getReceiveName() {
        return receiveName;
    }

    public void setReceiveName(String receiveName) {
        this.receiveName = receiveName;
    }

    public String getReceiveLocation() {
        return receiveLocation;
    }

    public void setReceiveLocation(String receiveLocation) {
        this.receiveLocation = receiveLocation;
    }

    public String getReceivePhone() {
        return receivePhone;
    }

    public void setReceivePhone(String receivePhone) {
        this.receivePhone = receivePhone;
    }

    public double getReceiveLat() {
        return receiveLat;
    }

    public void setReceiveLat(double receiveLat) {
        this.receiveLat = receiveLat;
    }

    public double getReceiveLng() {
        return receiveLng;
    }

    public void setReceiveLng(double receiveLng) {
        this.receiveLng = receiveLng;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }


}
