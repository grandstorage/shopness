package com.routh.model.order.details.status;

import java.util.ArrayList;

import com.routh.R;
import com.routh.application.BaseApplication;
import com.routh.base.UserPreferenceHelper;

/**
 * Created by MahmoudAyman on 8/23/2019.
 **/
public class OrderStatusResponse {

    private int id;
    private String title;
    private boolean checked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public OrderStatusResponse() {
    }

    private OrderStatusResponse(int id, String title, boolean checked) {
        this.id = id;
        this.title = title;
        this.checked = checked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String getString(int stringRes) {
        return BaseApplication.getInstance().getString(stringRes);
    }

    public ArrayList<OrderStatusResponse> getOrderStatusResponses() {
        ArrayList<OrderStatusResponse> list = new ArrayList<>();
        list.add(new OrderStatusResponse(1, getString(R.string.search_for_captain), getChecked(1)));
        list.add(new OrderStatusResponse(2, getString(R.string.captain_received_order), getChecked(2)));
        list.add(new OrderStatusResponse(4, getString(R.string.order_on_way), getChecked(4)));
        list.add(new OrderStatusResponse(6, getString(R.string.recevied), getChecked(6)));
        return list;
    }

    private boolean getChecked(int i) {
        return UserPreferenceHelper.getOrderStatusId() == i;
    }


}
