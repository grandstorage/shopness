package com.routh.model.shops.filter;

import com.google.gson.annotations.SerializedName;

import com.routh.model.shops.ShopsData;

public class FilterShopsResponse{

	@SerializedName("data")
	private ShopsData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setShopsData(ShopsData data){
		this.data = data;
	}

	public ShopsData getShopsData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"FilterShopsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}