package com.routh.model.shops;

import com.google.gson.annotations.SerializedName;

public class ShopsRequest {

    @SerializedName("service_id")
    private int serviceId;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;


    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
