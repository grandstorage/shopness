package com.routh.model.shops.details;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ShopDetailsResponse{

	@SerializedName("data")
	private List<ShopDetailItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<ShopDetailItem> data){
		this.data = data;
	}

	public List<ShopDetailItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ShopDetailsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}