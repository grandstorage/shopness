package com.routh.model.order.details.summary;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderSummaryResponse{

	@SerializedName("data")
	private List<OrderSummaryData> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<OrderSummaryData> data){
		this.data = data;
	}

	public List<OrderSummaryData> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderSummaryResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}