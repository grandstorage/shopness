package grand.shopness.viewmodel.fragment.main;

import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.android.volley.Request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.shops.mainshops.response.BannerItem;
import grand.shopness.model.shops.mainshops.response.MainShopsResponse;
import grand.shopness.view.adapter.parent.MainShopsCategoryAdapter;
import grand.shopness.view.adapter.parent.MainShopsSliderAdapt;
import timber.log.Timber;

public class MainShopsViewModel extends BaseViewModel {

    private MainShopsCategoryAdapter categoryAdapter;
    private MainShopsResponse mainShopsResponse;
    private MainShopsSliderAdapt mainShopsSliderAdapt;
    public MainShopsViewModel() {
        mainShopsResponse = new MainShopsResponse();
        categoryAdapter = new MainShopsCategoryAdapter();
        startPopulateData();
    }

    public MainShopsResponse getMainShopsResponse() {
        return mainShopsResponse;
    }

    @BindingAdapter({"android:adapter"})
    public static void getShopsCategBinding(RecyclerView recyclerView, MainShopsCategoryAdapter allShopsAdapter) {
        //you can set layout manager here
        ((GridLayoutManager) Objects.requireNonNull(recyclerView.getLayoutManager()))
                .setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (allShopsAdapter.getItemViewType(position)) {
                    case 0:
                        return 2;
                    case 1:
                        return 1;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setAdapter(allShopsAdapter);
    }
    @BindingAdapter({"adapter"})

    public static void getSliderBinding(ViewPager viewPager, MainShopsSliderAdapt sliderAdapt) {
        viewPager.setAdapter(sliderAdapt);
    }

    @Bindable
    public MainShopsSliderAdapt getMainShopsSliderAdapt(){
        return this.mainShopsSliderAdapt == null ? this.mainShopsSliderAdapt = new MainShopsSliderAdapt() : this.mainShopsSliderAdapt;
    }


    @Bindable
    public MainShopsCategoryAdapter getCategoryAdapter() {
        return this.categoryAdapter == null ? this.categoryAdapter = new MainShopsCategoryAdapter() : this.categoryAdapter;
    }

    public void onViewAllClick(){
        setValue(Codes.ON_ALL_CLICK);
    }

    private void startPopulateData() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                mainShopsResponse = ((MainShopsResponse) response);
                switch (mainShopsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getCategoryAdapter().updateDataList(mainShopsResponse.getData().getService());
                        initImageSlider(mainShopsResponse.getData().getBanner());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(mainShopsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.SERVICES, new Object(), MainShopsResponse.class);
    }

    private void initImageSlider(List<BannerItem> bannerItems) {
        getMainShopsSliderAdapt().updateData(bannerItems);
        final int[] currentPager = {0};
        try {
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPager[0] == bannerItems.size()) {
                    currentPager[0] = 0;
                }
                SparseIntArray map = new SparseIntArray();
                map.put(Codes.MOVE_SLIDER, currentPager[0]++);
                setValue(map);
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 3000);
        } catch (Exception e) {
            Timber.e(e);
        }
    }


}
