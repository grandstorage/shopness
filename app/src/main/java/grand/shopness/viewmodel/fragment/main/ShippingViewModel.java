package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.shipping.ShippingRequest;
import grand.shopness.model.shipping.ShippingResponse;
import timber.log.Timber;

public class ShippingViewModel extends BaseViewModel {

    private ShippingRequest shippingRequest;
    public ObservableBoolean enableBtn = new ObservableBoolean();
    public ShippingViewModel() {
        enableBtn.set(true);
        shippingRequest = new ShippingRequest();
        getInfo();
        notifyChange();
    }

    private void getInfo() {
        try {
            String[] splitName = UserPreferenceHelper.getUserLoginDetails().getName().split("\\s+");
            shippingRequest.setFirstName(splitName[0]);
            shippingRequest.setLastName(splitName[1]);
        }catch (Exception e) {
            Timber.e(e);
            shippingRequest.setFirstName(UserPreferenceHelper.getUserLoginDetails().getName());
        }
        shippingRequest.setLocation(UserPreferenceHelper.getUserLoginDetails().getLocation());
        shippingRequest.setLat(Double.valueOf(UserPreferenceHelper.getUserLoginDetails().getLat()));
        shippingRequest.setLng(Double.valueOf(UserPreferenceHelper.getUserLoginDetails().getLng()));
        shippingRequest.setPhone(UserPreferenceHelper.getUserLoginDetails().getPhone());
        shippingRequest.setAddressId(UserPreferenceHelper.getAddressId());
        Timber.e(shippingRequest.toString());
    }

    @Bindable
    public ShippingRequest getShippingRequest() {
        return shippingRequest;
    }


    public void onSubmitBtnClick(){
        shippingRequest.setName(shippingRequest.getFirstName() + " " + shippingRequest.getLastName());
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
               ShippingResponse shippingResponse = ((ShippingResponse) response);
                switch (shippingResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        UserPreferenceHelper.setAddressId(shippingResponse.getAddressId());
                        setValue(Codes.PAYMENT_SCREEN);
                        break;
                    case WebServices.FAILED:
                        setMessage(shippingResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.UPDATE_ADDRESS, shippingRequest, ShippingResponse.class);
    }






}
