package com.routh.model.shops.details;

import com.google.gson.annotations.SerializedName;

public class CategoryItem{

	@SerializedName("category_name")
	private String categoryName;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("pivot")
	private Pivot pivot;

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setPivot(Pivot pivot){
		this.pivot = pivot;
	}

	public Pivot getPivot(){
		return pivot;
	}

	@Override
 	public String toString(){
		return 
			"CategoryItem{" + 
			"category_name = '" + categoryName + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",pivot = '" + pivot + '\'' + 
			"}";
		}
}