package com.routh.model.cart.update;

import com.google.gson.annotations.SerializedName;

public class UpdateItemInCartRequest {
    @SerializedName("product_id")
    private int productId;

    @SerializedName("qty")
    private int qty;

    @SerializedName("cart_item_id")
    private int cartItemId;

    @SerializedName("additions")
    private String additions;

    @SerializedName("without_extra")
    private String withoutExtra;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    public String getAdditions() {
        return additions;
    }

    public void setAdditions(String additions) {
        this.additions = additions;
    }

    public String getWithoutExtra() {
        return withoutExtra;
    }

    public void setWithoutExtra(String withoutExtra) {
        this.withoutExtra = withoutExtra;
    }
}
