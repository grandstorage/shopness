package grand.shopness.viewmodel.fragment.login;

import android.view.View;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.login.request.LoginRequest;
import grand.shopness.model.login.response.LoginData;
import grand.shopness.model.login.response.LoginResponse;

public class LoginViewModel extends BaseViewModel {

    private LoginRequest loginRequest;

    public LoginViewModel() {
        loginRequest = new LoginRequest();
    }

    public void onSkipClick() {
        setValue(Codes.PRESS_SKIP);
    }

    public void onSignupClick() {
        setValue(Codes.REGISTER_SCREEN);
    }

    public void onSubmitClick() {
        notifyChange(); // to get data from xml
        goLogin();
    }

    public void onForgotPasswordClick() {
        setValue(Codes.FORGOT_PASSWORD_SCREEN);
    }

    public LoginRequest getLoginRequest() {
        return loginRequest;
    }

    public void setLoginRequest(LoginRequest loginRequest) {
        this.loginRequest = loginRequest;
    }

    private void goLogin() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                LoginResponse loginResponse = (LoginResponse) response;
                switch (loginResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        LoginData userItem = loginResponse.getData();
                        UserPreferenceHelper.saveUserDetails(userItem);
                        setMessage(loginResponse.getMessage());
                        setValue(Codes.HOME_SCREEN);
                        break;
                    case WebServices.NOT_ACTIVE:
                        setMessage(loginResponse.getMessage());
                        UserPreferenceHelper.setForgotPass(false);
                        setValue(Codes.ENTER_CODE_SCREEN);
                        break;
                    case WebServices.FAILED:
                        setMessage(loginResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.LOGIN, loginRequest, LoginResponse.class);
    }


}
