package grand.shopness.viewmodel.fragment.main;

import android.view.View;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.android.volley.Request;

import java.lang.reflect.Field;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.cart.view.CartResponse;
import grand.shopness.util.SwipeToDeleteCallback;
import grand.shopness.view.adapter.parent.CartAdapter;

public class CartViewModel extends BaseViewModel {

    private CartAdapter cartAdapter;
    private CartResponse cartResponse;
    public ObservableBoolean isLoading = new ObservableBoolean();

    public CartViewModel() {
        cartAdapter = new CartAdapter();
        startPopulateData();
    }

    @Bindable
    public CartResponse getCartResponse() {
        return cartResponse;
    }

    @BindingAdapter({"android:adapter"})
    public static void getFaqBinding(RecyclerView recyclerView, CartAdapter cartAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(cartAdapter);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(cartAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }

    @Bindable
    public CartAdapter getCartAdapter() {
        return this.cartAdapter == null ? this.cartAdapter = new CartAdapter() : this.cartAdapter;
    }

    private void startPopulateData() {
        cartResponse = new CartResponse();
        notifyChange();
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                cartResponse = ((CartResponse) response);
                switch (cartResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getCartAdapter().updateDataList(cartResponse.getData().getCart());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(cartResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_CART, new Object(), CartResponse.class);
    }

    public void onBtnPlaceOrderClick(){
        UserPreferenceHelper.setTotalAmount(cartResponse.getData().getPrice());
        setValue(Codes.CHECKOUT_SCREEN);
    }

}
