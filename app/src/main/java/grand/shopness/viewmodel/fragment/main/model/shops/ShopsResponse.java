package com.routh.model.shops;

import com.google.gson.annotations.SerializedName;

public class ShopsResponse{

	@SerializedName("data")
	private ShopsData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setShopsData(ShopsData data){
		this.data = data;
	}

	public ShopsData getShopsData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ShopsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}