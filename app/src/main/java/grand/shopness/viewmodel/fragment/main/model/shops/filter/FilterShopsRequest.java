package com.routh.model.shops.filter;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class FilterShopsRequest implements Serializable {
    @SerializedName("filter_id")
    private ArrayList<Integer> listOfFilterId;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;

    @SerializedName("service_id")
    private int service_id;


    public ArrayList<Integer> getListOfFilterId() {
        return listOfFilterId;
    }

    public void setListOfFilterId(ArrayList<Integer> listOfFilterId) {
        this.listOfFilterId = listOfFilterId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    @Override
    public String toString(){
        return
                "FilterShopsRquest{" +
                        "ids = '" + listOfFilterId.toString() + '\'' +
                        ",lat + long = '" + lat + " + " + lng + '\'' +
                        ",serviceId = '" + service_id + '\'' +
                        "}";
    }

}
