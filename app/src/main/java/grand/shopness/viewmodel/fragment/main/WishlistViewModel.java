package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.lang.reflect.Field;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.favourite.view.WishListResponse;
import grand.shopness.util.SwipeToDeleteCallback;
import grand.shopness.view.adapter.parent.FavouriteAdapter;
import timber.log.Timber;

public class WishlistViewModel extends BaseViewModel {


    private FavouriteAdapter favouriteAdapter;
    private WishListResponse favRespnse;
    public ObservableBoolean isLoading = new ObservableBoolean();

    public WishlistViewModel() {
        favouriteAdapter = new FavouriteAdapter();
        startPopulateData();
    }

    @BindingAdapter({"adapter"})
    public static void getFavAdapterBinding(RecyclerView recyclerView, FavouriteAdapter allShopsAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(allShopsAdapter);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }

    @Bindable
    public FavouriteAdapter getFavAdapter() {
        return this.favouriteAdapter == null ? this.favouriteAdapter = new FavouriteAdapter() : this.favouriteAdapter;
    }

    private void startPopulateData() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                favRespnse = ((WishListResponse) response);
                switch (favRespnse.getStatus()) {
                    case WebServices.SUCCESS:
                        getFavAdapter().updateDataList(favRespnse.getData().getWhishlistItems());
                        break;
                    case WebServices.FAILED:
                        setMessage(favRespnse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_WISHLIST, new Object(), WishListResponse.class);


    }

}
