package grand.shopness.viewmodel.fragment.main;

import androidx.databinding.ObservableBoolean;

import java.util.ListIterator;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;

public class ChatViewModel extends BaseViewModel {


    public ObservableBoolean isLoading = new ObservableBoolean();

    public void onRefresh() {
        isLoading.set(true);
//        startPopulateData();
    }


    public void onChatBtnClick(){
        setValue(Codes.CHAT_SCREEN);
    }

}
