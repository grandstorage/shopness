package grand.shopness.viewmodel.fragment.login;

import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.sendcode.SendCodeRequest;
import grand.shopness.model.sendcode.SendCodeResponse;

public class SendCodeViewModel extends BaseViewModel {
    private SendCodeRequest sendCodeRequest;

    public SendCodeViewModel() {
        sendCodeRequest = new SendCodeRequest();
    }

    @Bindable
    public SendCodeRequest getSendCodeRequest() {
        return sendCodeRequest;
    }

    public void onBackClick() {
        setValue(Codes.PRESS_BACK);
    }

    public void onSaveClick() {
        notifyChange();
        submit();
    }

    private void submit() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SendCodeResponse sendCodeResponse = (SendCodeResponse) response;
                switch (sendCodeResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        SendCodeResponse userItem = sendCodeResponse;
                        UserPreferenceHelper.saveUserDetails(userItem);
                        setMessage(sendCodeResponse.getMessage());
                        if (!UserPreferenceHelper.getForgotPassScreen())
                            setValue(Codes.HOME_SCREEN);
                        else
                            setValue(Codes.CHANGE_PASSWORD_SCREEN);
                        break;
                    case WebServices.FAILED:
                        setMessage(sendCodeResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.SEND_CODE, sendCodeRequest, SendCodeResponse.class);
    }

}
