package com.routh.model.order.details.summary;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderSummaryItem{

	@SerializedName("order_status")
	private String orderStatus;

	@SerializedName("total")
	private int total;

	@SerializedName("order_products")
	private List<OrderProductsItem> orderProducts;

	@SerializedName("shipping_fees")
	private String shippingFees;

	@SerializedName("id")
	private int id;

	@SerializedName("subtotal_fees")
	private String subtotalFees;

	@SerializedName("status")
	private int status;

	public void setOrderStatus(String orderStatus){
		this.orderStatus = orderStatus;
	}

	public String getOrderStatus(){
		return orderStatus;
	}

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setOrderProducts(List<OrderProductsItem> orderProducts){
		this.orderProducts = orderProducts;
	}

	public List<OrderProductsItem> getOrderProducts(){
		return orderProducts;
	}

	public void setShippingFees(String shippingFees){
		this.shippingFees = shippingFees;
	}

	public String getShippingFees(){
		return shippingFees;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSubtotalFees(String subtotalFees){
		this.subtotalFees = subtotalFees;
	}

	public String getSubtotalFees(){
		return subtotalFees;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"order_status = '" + orderStatus + '\'' + 
			",total = '" + total + '\'' + 
			",order_products = '" + orderProducts + '\'' + 
			",shipping_fees = '" + shippingFees + '\'' + 
			",id = '" + id + '\'' + 
			",subtotal_fees = '" + subtotalFees + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}