package com.routh.model.cart.add;

import com.google.gson.annotations.SerializedName;

public class AddToCartRequest {
    @SerializedName("product_id")
    private int productId;

    @SerializedName("qty")
    private int qty;

    @SerializedName("cart_item_id")
    private int cart_item_id;

    @SerializedName("plus")
    private int plus;

    @SerializedName("additions")
    private String additions;

    @SerializedName("without_extra")
    private String withoutExtra;


    public String getAdditions() {
        return additions;
    }

    public void setAdditions(String additions) {
        this.additions = additions;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getCart_item_id() {
        return cart_item_id;
    }

    public void setCart_item_id(int cart_item_id) {
        this.cart_item_id = cart_item_id;
    }

    public int getPlus() {
        return plus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }

    public String getWithoutExtra() {
        return withoutExtra;
    }

    public void setWithoutExtra(String withoutExtra) {
        this.withoutExtra = withoutExtra;
    }
}
