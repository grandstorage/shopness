package com.routh.model.register;



import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("user_id")
	private int userId;

	@SerializedName("jwt_token")
	private String jwtToken;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setJwtToken(String jwtToken){
		this.jwtToken = jwtToken;
	}

	public String getJwtToken(){
		return jwtToken;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"user_id = '" + userId + '\'' + 
			",jwt_token = '" + jwtToken + '\'' + 
			"}";
		}
}