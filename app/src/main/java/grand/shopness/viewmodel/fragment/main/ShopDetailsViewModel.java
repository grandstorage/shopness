package grand.shopness.viewmodel.fragment.main;

import android.view.View;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.google.android.material.button.MaterialButton;

import java.lang.reflect.Field;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.shops.details.ShopDetailsItem;
import grand.shopness.model.shops.details.ShopDetailsResponse;
import grand.shopness.model.shops.details.follow.FollowRequest;
import grand.shopness.model.shops.details.follow.FollowResponse;
import grand.shopness.model.shops.details.products.ProductsResponse;
import grand.shopness.view.adapter.parent.ShopDetailsCategoryAdapter;
import grand.shopness.view.adapter.parent.ShopDetailsProductAdapter;
import timber.log.Timber;

public class ShopDetailsViewModel extends BaseViewModel {
    private ShopDetailsCategoryAdapter categoryAdapter;
    private ShopDetailsProductAdapter productAdapter;
    private ShopDetailsResponse detailsResponse;
    private ShopDetailsItem shopDetailsItem;
    private boolean isProgressVisible;
    private FollowResponse followResponse;
    private FollowRequest followRequest;
    private int shopId;
    public ObservableBoolean isLoading = new ObservableBoolean();

    public ShopDetailsViewModel(int shopId) {
        this.shopId = shopId;
        categoryAdapter = new ShopDetailsCategoryAdapter();
        productAdapter = new ShopDetailsProductAdapter();
        followRequest = new FollowRequest();
        startPopulateAllData(shopId);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateAllData(shopId);
    }


    @Bindable
    public ShopDetailsItem getShopDetailsItem() {
        return shopDetailsItem;
    }

    @BindingAdapter({"categoryAdapter"})
    public static void getCategoryBinding(RecyclerView recyclerView, ShopDetailsCategoryAdapter allShopsAdapter) {
        recyclerView.setAdapter(allShopsAdapter);
    }

    @BindingAdapter({"productAdapter"})
    public static void getProductsBinding(RecyclerView recyclerView, ShopDetailsProductAdapter allShopsAdapter) {
        recyclerView.setAdapter(allShopsAdapter);
    }

    @BindingAdapter({"app:backgroundTint"})
    public static void getButtonBinding(MaterialButton button, int res) {
        button.setBackgroundColor(res);
    }

    @Bindable
    public ShopDetailsCategoryAdapter getCategoryAdapter() {
        return this.categoryAdapter == null ? this.categoryAdapter = new ShopDetailsCategoryAdapter() : this.categoryAdapter;
    }

    @Bindable
    public ShopDetailsProductAdapter getProductAdapter() {
        return this.productAdapter == null ? this.productAdapter = new ShopDetailsProductAdapter() : this.productAdapter;
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imagePath) {
        ConnectionHelper.loadImage(view, imagePath);
    }

    public void onFollowClick() {
        if (!isProgressVisible)
            startFollow();
    }

    private void startFollow() {
        followRequest.setId(detailsResponse.getData().get(0).getId());
        Timber.e("sssss:     " + followRequest.getId());
        accessLoadingBar(View.VISIBLE);
        isProgressVisible = true;

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                followResponse = ((FollowResponse) response);
                switch (followResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        detailsResponse.getData().get(0).setIsFollow(followResponse.getIsFollow());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(detailsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isProgressVisible = false;
            }
        }).requestJsonObject(Request.Method.POST, WebServices.FOLLOW, followRequest, FollowResponse.class);
    }

    public void onReviewsClick() {
        setValue(Codes.REVIEWS_SCREEN);
    }

    private void startPopulateAllData(int shopId) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                detailsResponse = ((ShopDetailsResponse) response);
                switch (detailsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        shopDetailsItem = detailsResponse.getData().get(0);
                        getCategoryAdapter().updateDataList(detailsResponse.getData().get(0).getCategory());
                        getProductAdapter().updateDataList(detailsResponse.getData().get(0).getProducts());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(detailsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_SHOPS + "shop_id=" + shopId, new Object(), ShopDetailsResponse.class);

    }

    public void getProductsData(int id) {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ProductsResponse productsResponse = ((ProductsResponse) response);
                switch (productsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getProductAdapter().updateDataList(productsResponse.getData());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(productsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_PRODUCTS + "category_id=" + id, new Object(), ProductsResponse.class);
    }
}
