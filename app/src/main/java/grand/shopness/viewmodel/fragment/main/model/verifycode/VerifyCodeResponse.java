package com.routh.model.verifycode;

import com.google.gson.annotations.SerializedName;

import com.routh.model.login.UserData;

public class VerifyCodeResponse{

	@SerializedName("data")
	private UserData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(UserData data){
		this.data = data;
	}

	public UserData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"VerifyCodeResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}