package grand.shopness.viewmodel.fragment.main;

import android.view.View;
import androidx.databinding.Bindable;
import com.android.volley.Request;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.faq.FaqResponse;
import grand.shopness.model.privacy.PrivacyResponse;
import timber.log.Timber;

public class PrivacyTermsViewModel extends BaseViewModel {
    private PrivacyResponse privacyResponse;

    public PrivacyTermsViewModel() {
        privacyResponse = new PrivacyResponse();
        startPopulateData();
    }

    @Bindable
    public PrivacyResponse getPrivacyResponse() {
        return privacyResponse;
    }

    private void startPopulateData() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                privacyResponse = ((PrivacyResponse) response);
                switch (privacyResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(privacyResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.PRIVACY_TERMS, new Object(), PrivacyResponse.class);


    }
}
