package com.routh.model.cart;

import com.google.gson.annotations.SerializedName;

public class GetOrderIdRequest {
    @SerializedName("shop_id")
    private int shop_id;

    @SerializedName("shipping_fees")
    private int shippingFees;

    public int getShippingFees() {
        return shippingFees;
    }

    public void setShippingFees(int shippingFees) {
        this.shippingFees = shippingFees;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }
}
