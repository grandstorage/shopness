package grand.shopness.viewmodel.fragment.main;

import android.annotation.SuppressLint;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.shops.allshops.request.AllShopsRequest;
import grand.shopness.model.shops.allshops.response.AllShopsResponse;
import grand.shopness.view.adapter.parent.NearestShopAdapter;
import io.reactivex.disposables.Disposable;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NearestShopsViewModel extends BaseViewModel {

    private NearestShopAdapter nearestShopAdapter;
    private AllShopsResponse allShopsResponse;
    private AllShopsRequest allShopsRequest;
    public ObservableBoolean isLoading = new ObservableBoolean();
    private ReactiveLocationProvider locationProvider;
    private Disposable disposable;

    public void setLocationProvider(ReactiveLocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    public NearestShopsViewModel() {
        allShopsRequest = new AllShopsRequest();
    }

    @BindingAdapter({"adapter"})
    public static void getNearestShopsBinding(RecyclerView recyclerView, NearestShopAdapter nearestShopAdapter) {
        recyclerView.setAdapter(nearestShopAdapter);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }


    @Bindable
    public NearestShopAdapter getNearestShopAdapter() {
        return this.nearestShopAdapter == null ? this.nearestShopAdapter = new NearestShopAdapter() : this.nearestShopAdapter;
    }


    @SuppressLint("MissingPermission")
    public void startPopulateData() {
        accessLoadingBar(VISIBLE);
        allShopsRequest.setId(UserPreferenceHelper.getServiceId()); //service_id

        if (!UserPreferenceHelper.isLogged()) {
            disposable = locationProvider.getLastKnownLocation()
                    .subscribe(location -> {
                        Timber.e("location: %s", location);
                        allShopsRequest.setLat(location.getLatitude());
                        allShopsRequest.setLng(location.getLongitude());
                        getData();
                        disposable.dispose();
                    });
        } else {
            getData();
        }

    }

    private void getData() {
        Timber.e("getData");
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                allShopsResponse = ((AllShopsResponse) response);
                switch (allShopsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getNearestShopAdapter().updateDataList(allShopsResponse.getData().getNearestShops(),
                                allShopsResponse.getData());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(allShopsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.GET_ALL_SHOPS, allShopsRequest, AllShopsResponse.class);
    }
}
