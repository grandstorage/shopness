package grand.shopness.viewmodel.fragment.login;

import android.view.View;

import com.android.volley.Request;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.forgotpassword.ForgotPasswordRequest;
import grand.shopness.model.forgotpassword.ForgotPasswordResponse;

public class ForgotPasswordViewModel extends BaseViewModel {
    private ForgotPasswordRequest forgotPasswordRequest;

    public ForgotPasswordViewModel() {
        forgotPasswordRequest = new ForgotPasswordRequest();
    }

    public ForgotPasswordRequest getForgotPasswordRequest() {
        return forgotPasswordRequest;
    }

    public void onBackClick(){
        setValue(Codes.LOGIN_SCREEN);
    }

    public void onSendClick(){
        notifyChange();
        submit();
    }

    private void submit() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ForgotPasswordResponse forgotPassResponse = (ForgotPasswordResponse) response;
                switch (forgotPassResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setValue(Codes.ENTER_CODE_SCREEN);
                        break;
                    case WebServices.FAILED:
                        setMessage(forgotPassResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHECK_PHONE, forgotPasswordRequest, ForgotPasswordResponse.class);
    }
}
