package com.routh.model.transition;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TransitionResponse{

	@SerializedName("data")
	private List<TransitionItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<TransitionItem> data){
		this.data = data;
	}

	public List<TransitionItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"TransitionResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}