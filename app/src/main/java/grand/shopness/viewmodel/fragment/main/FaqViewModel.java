package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.lang.reflect.Field;
import java.util.ArrayList;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.aboutus.AboutUsResponse;
import grand.shopness.model.faq.FaqData;
import grand.shopness.model.faq.FaqItem;
import grand.shopness.model.faq.FaqResponse;
import grand.shopness.view.adapter.parent.AllShopsAdapter;
import grand.shopness.view.adapter.parent.FaqAdapter;
import timber.log.Timber;

public class FaqViewModel extends BaseViewModel {

    private FaqAdapter faqAdapter;
    private FaqResponse faqResponse;
    public ObservableBoolean isLoading = new ObservableBoolean();

    public FaqViewModel() {
        faqAdapter = new FaqAdapter();
        startPopulateData();
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }


    @BindingAdapter({"android:adapter"})
    public static void getFaqBinding(RecyclerView recyclerView, FaqAdapter allShopsAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(allShopsAdapter);
    }

    @Bindable
    public FaqAdapter getFaqAdapter() {
        return this.faqAdapter == null ? this.faqAdapter= new FaqAdapter() : this.faqAdapter;
    }

    private void startPopulateData() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                faqResponse = ((FaqResponse) response);
                switch (faqResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getFaqAdapter().updateDataList(faqResponse.getData());
//                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(faqResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.FAQ, new Object(), FaqResponse.class);


    }


}
