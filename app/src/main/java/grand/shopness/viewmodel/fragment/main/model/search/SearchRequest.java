package com.routh.model.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 8/24/2019.
 **/
public class SearchRequest {
    @SerializedName("search")
    private String text;
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
