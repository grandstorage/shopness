package grand.shopness.viewmodel.fragment.main;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.RecyclerView;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.model.payment.PaymentItem;
import grand.shopness.view.adapter.parent.PaymentAdapter;

public class PaymentViewModel extends BaseViewModel {

    private PaymentAdapter paymentAdapter;
    public ObservableBoolean isLoading = new ObservableBoolean();
    private PaymentItem paymentItem;

    public PaymentViewModel() {
        paymentItem = new PaymentItem();
        paymentAdapter = new PaymentAdapter();
        startPopulateData();
    }

    @BindingAdapter({"adapter"})
    public static void getPaymentBinding(RecyclerView recyclerView, PaymentAdapter paymentAdapter) {
        //you can set layout manager here
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(paymentAdapter);
    }

    @Bindable
    public PaymentAdapter getPaymentAdapter() {
        return this.paymentAdapter == null ? this.paymentAdapter = new PaymentAdapter() : this.paymentAdapter;
    }

    private void startPopulateData() {
        getPaymentAdapter().updateDataList(paymentItem.getPaymentItems());
    }

    public void onConfirmClick(){
        setValue(Codes.CONFIRMATION_SCREEN);
    }


}
