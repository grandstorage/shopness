package com.routh.model.product;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductItem{

	@SerializedName("product_Added")
	private List<ProductAdds> productAdded;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("price")
	private int price;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("product_name")
	private String productName;

	public void setProductAdded(List<ProductAdds> productAdded){
		this.productAdded = productAdded;
	}

	public List<ProductAdds> getProductAdded(){
		return productAdded;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@Override
 	public String toString(){
		return 
			"{DataItem{" +
			"product_Added = '" + productAdded + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",price = '" + price + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}}";
		}
}