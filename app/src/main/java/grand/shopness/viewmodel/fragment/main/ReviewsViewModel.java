package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.reviews.ReviewsRequest;
import grand.shopness.model.reviews.ReviewsResponse;
import grand.shopness.model.reviews.SendCommentResponse;
import grand.shopness.view.adapter.parent.ReviewsAdapter;

import static android.view.View.VISIBLE;

public class ReviewsViewModel extends BaseViewModel {

    private ReviewsAdapter reviewsAdapter;
    private ReviewsResponse reviewsResponse;
    private ReviewsRequest reviewsRequest;
    public ObservableBoolean isLoading = new ObservableBoolean();

    public ReviewsViewModel(int shopId) {
        reviewsRequest = new ReviewsRequest();
        reviewsAdapter = new ReviewsAdapter();
        reviewsRequest.setId(shopId);
        startPopulateData();
    }

    @BindingAdapter({"android:adapter"})
    public static void getReviewsBinding(RecyclerView recyclerView, ReviewsAdapter reviewsAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(reviewsAdapter);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }

    @Bindable
    public ReviewsRequest getReviewsRequest() {
        return reviewsRequest;
    }

    @Bindable
    public ReviewsAdapter getReviewsAdapter() {
        return this.reviewsAdapter == null ? this.reviewsAdapter = new ReviewsAdapter() : this.reviewsAdapter;
    }

    private void startPopulateData() {
        accessLoadingBar(VISIBLE);
        setValue(Codes.PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                reviewsResponse = ((ReviewsResponse) response);
                switch (reviewsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getReviewsAdapter().updateDataList(reviewsResponse.getData());
//                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(reviewsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                setValue(Codes.PROGRESS);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.GET_REVIEWS, reviewsRequest, ReviewsResponse.class);


    }


    public void onSendClick(){
        accessLoadingBar(VISIBLE);
        setValue(Codes.ANIM);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
               SendCommentResponse sendCommentResponse = ((SendCommentResponse) response);
                switch (sendCommentResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        reviewsRequest.setComment("");
                        notifyChange();
                        startPopulateData();
                        break;
                    case WebServices.FAILED:
                        setMessage(sendCommentResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                setValue(Codes.ANIM);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.SEND_COMMENT, reviewsRequest, SendCommentResponse.class);
    }


}
