package com.routh.model.cart.delete;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 8/21/2019.
 **/
public class CartDeleteRequest {
    @SerializedName("cart_item_id")
    private int cartItemId;

    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }
}
