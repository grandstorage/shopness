package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.promocode.PromoCodeResponse;

public class PromoCodeViewModel extends BaseViewModel {

    PromoCodeResponse promoCodeResponse;
    public PromoCodeViewModel() {
        accessLoadingBar(View.VISIBLE);
        promoCodeResponse = new PromoCodeResponse();
        startPopulateData();
    }

    @Bindable
    public PromoCodeResponse getPromoCodeResponse() {
        return promoCodeResponse;
    }

    public void onCopyClick(){
        UserPreferenceHelper.copyPromoCode(promoCodeResponse.getData().getCode());
        setValue(Codes.SAVE_TO_SHARED_PREFERENCE);
    }


    private void startPopulateData() {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                promoCodeResponse = ((PromoCodeResponse) response);
                switch (promoCodeResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setValue(WebServices.SUCCESS);
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(promoCodeResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.PROMO_CODE, new Object(), PromoCodeResponse.class);


    }




}
