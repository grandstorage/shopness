package com.routh.model.orders.view;

import com.google.gson.annotations.SerializedName;

public class OrderItem{

	@SerializedName("order_status")
	private OrderStatus orderStatus;

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("id")
	private int id;

	public void setOrderStatus(OrderStatus orderStatus){
		this.orderStatus = orderStatus;
	}

	public OrderStatus getOrderStatus(){
		return orderStatus;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"order_status = '" + orderStatus + '\'' + 
			",order_date = '" + orderDate + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}