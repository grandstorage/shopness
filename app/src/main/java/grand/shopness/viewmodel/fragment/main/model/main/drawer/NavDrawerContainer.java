package com.routh.model.main.drawer;

import java.util.ArrayList;

import com.routh.R;
import com.routh.application.BaseApplication;
import com.routh.base.constantsutils.Codes;

public class NavDrawerContainer {

    private NavHeaderItem navHeaderItem;
    private NavDrawerItem navDrawerItem;

    public NavHeaderItem getNavHeaderItem() {
        return navHeaderItem == null ? navHeaderItem = new NavHeaderItem() : navHeaderItem;
    }

    public void setNavHeaderItem(NavHeaderItem navHeaderItem) {
        this.navHeaderItem = navHeaderItem;
    }

    public NavDrawerItem getNavDrawerItem() {
        return navDrawerItem;
    }

    public void setNavDrawerItem(NavDrawerItem navDrawerItem) {
        this.navDrawerItem = navDrawerItem;
    }

    private String getString(int resId) {
        return BaseApplication.getInstance().getApplicationContext().getString(resId);
    }

    public ArrayList<NavDrawerItem> getDrawerItemsData() {
        ArrayList<NavDrawerItem> items = new ArrayList<>();
        items.add(new NavDrawerItem(Codes.NOTIFICATION_SCREEN, getString(R.string.notifications), R.drawable.ic_notifications));
        items.add(new NavDrawerItem(Codes.TRANSITION_SCREEN, getString(R.string.transitions), R.drawable.ic_transitions));
        items.add(new NavDrawerItem(Codes.TERMS_SCREEN, getString(R.string.privacy_terms), R.drawable.ic_danger));
        items.add(new NavDrawerItem(Codes.RATE_SCREEN, getString(R.string.rate), R.drawable.ic_star));
        items.add(new NavDrawerItem(Codes.SHARE_SCREEN, getString(R.string.share_app), R.drawable.ic_share));
        items.add(new NavDrawerItem(Codes.ABOUT_SCREEN, getString(R.string.about_us), R.drawable.ic_search_info));
        items.add(new NavDrawerItem(Codes.LOGOUT_SCREEN, getString(R.string.log_out), R.drawable.ic_exit));
        return items;
    }

}
