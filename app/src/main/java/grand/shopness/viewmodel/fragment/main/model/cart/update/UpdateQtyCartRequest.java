package com.routh.model.cart.update;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 8/21/2019.
 **/
public class UpdateQtyCartRequest {
    @SerializedName("cart_item_id")
    private int cartId;
    @SerializedName("plus")
    private int plus;
    @SerializedName("product_id")
    private int productId;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getPlus() {
        return plus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }
}
