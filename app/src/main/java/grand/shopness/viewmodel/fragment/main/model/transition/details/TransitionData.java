package com.routh.model.transition.details;

import com.google.gson.annotations.SerializedName;

public class TransitionData{

	@SerializedName("order_transfer_status")
	private OrderTransferStatus orderTransferStatus;

	@SerializedName("order")
	private Order order;

	public void setOrderTransferStatus(OrderTransferStatus orderTransferStatus){
		this.orderTransferStatus = orderTransferStatus;
	}

	public OrderTransferStatus getOrderTransferStatus(){
		return orderTransferStatus;
	}

	public void setOrder(Order order){
		this.order = order;
	}

	public Order getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"order_transfer_status = '" + orderTransferStatus + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}