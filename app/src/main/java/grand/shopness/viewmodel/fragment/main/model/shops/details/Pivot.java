package com.routh.model.shops.details;

import com.google.gson.annotations.SerializedName;

public class Pivot{

	@SerializedName("shop_id")
	private int shopId;

	@SerializedName("category_id")
	private int categoryId;

	public void setShopId(int shopId){
		this.shopId = shopId;
	}

	public int getShopId(){
		return shopId;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	@Override
 	public String toString(){
		return 
			"Pivot{" + 
			"shop_id = '" + shopId + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			"}";
		}
}