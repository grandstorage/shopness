package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.cart.view.CartResponse;
import grand.shopness.model.confirmation.ConfirmationReponse;
import grand.shopness.view.adapter.parent.OrdersItemsAdapter;

/**
 * Created by MahmoudAyman on 7/27/2019.
 **/
public class ConfirmationViewModel extends BaseViewModel {

    private OrdersItemsAdapter ordersItemsAdapter;
    private CartResponse cartResponse;
    public ObservableBoolean isLoading = new ObservableBoolean();
    private ConfirmationReponse confirmationReponse;

    public ConfirmationViewModel() {
        confirmationReponse = new ConfirmationReponse();
        startPopulateData();
    }


    @Bindable
    public ConfirmationReponse getConfirmationReponse() {
        return confirmationReponse;
    }

    @BindingAdapter({"android:adapter"})
    public static void getOrderItemsBinding(RecyclerView recyclerView, OrdersItemsAdapter ordersItemsAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(ordersItemsAdapter);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }

    @Bindable
    public OrdersItemsAdapter getOrdersItemsAdapter() {
        return this.ordersItemsAdapter == null ? this.ordersItemsAdapter = new OrdersItemsAdapter() : this.ordersItemsAdapter;
    }

    private void startPopulateData() {
        cartResponse = new CartResponse();
        notifyChange();
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                cartResponse = ((CartResponse) response);
                switch (cartResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getOrdersItemsAdapter().updateDataList(cartResponse.getData().getCart());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(cartResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_CART, new Object(), CartResponse.class);
    }

    public void onShippingInfoEditClick(){
        setValue(Codes.SHIPPING_SCREEN);
    }


    public void onOrdersEditClick(){
        setValue(Codes.CART_SCREEN);
    }


    public void onPaymentEditClick(){
        setValue(Codes.PAYMENT_SCREEN);
    }

    public void onOrderNowClick(){

    }

}
