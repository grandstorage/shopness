package grand.shopness.viewmodel.fragment.main;

import android.view.View;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import java.util.List;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.cart.add.AddCartRequest;
import grand.shopness.model.cart.add.AddCartResponse;
import grand.shopness.model.favourite.change.ChangeFavResponse;
import grand.shopness.model.favourite.change.ChangeFavouriteRequest;
import grand.shopness.model.product.detail.ProductDetailItem;
import grand.shopness.model.product.detail.ProductDetailRequest;
import grand.shopness.model.product.detail.ProductDetailResponse;
import grand.shopness.model.product.detail.ProductSizeItem;
import grand.shopness.model.product.size.SizeItem;
import grand.shopness.model.product.size.SizesResponse;
import grand.shopness.view.adapter.parent.ColorsAdapter;
import grand.shopness.view.adapter.parent.SizesAdapter;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductDetailViewModel extends BaseViewModel {

    private SizesAdapter sizesAdapter;
    private ColorsAdapter colorsAdapter;
    private SizesResponse sizesResponse;
    private ProductDetailRequest productDetailrequest;
    private ProductDetailItem productDetailItem;
    private ProductDetailResponse productDetailResponse;
    private int price, sizeItemId, colorId;
    private ChangeFavResponse favResponse;
    private ChangeFavouriteRequest favRequest;
    private AddCartRequest addCartRequest;

    public ProductDetailViewModel(int productId, int isClothes) {
        productDetailItem = new ProductDetailItem();
        productDetailrequest = new ProductDetailRequest(productId, isClothes);
        favRequest = new ChangeFavouriteRequest();
        addCartRequest = new AddCartRequest();
        getProductData();
    }

    @Bindable
    public int getPrice() {
        return price;
    }

    public AddCartRequest getAddCartRequest() {
        return addCartRequest;
    }

    @Bindable
    public ProductDetailItem getProductDetailItem() {
        return productDetailItem;
    }

    @BindingAdapter({"sizeAdapter"})
    public static void getSizesBinding(RecyclerView recyclerView, SizesAdapter sizesAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(sizesAdapter);
    }

    @BindingAdapter({"colorAdapter"})
    public static void getColorsBinding(RecyclerView recyclerView, ColorsAdapter colorsAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(colorsAdapter);
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageViewResource(ImageView imageView, String imagePath) {
        ConnectionHelper.loadImage(imageView, imagePath);
    }

    @Bindable
    public SizesAdapter getSizesAdapter() {
        return this.sizesAdapter == null ? this.sizesAdapter = new SizesAdapter() : this.sizesAdapter;
    }

    @Bindable
    public ColorsAdapter getColorsAdapter() {
        return this.colorsAdapter == null ? this.colorsAdapter = new ColorsAdapter() : this.colorsAdapter;
    }

    private void getProductData() {
        accessLoadingBar(VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                productDetailResponse = ((ProductDetailResponse) response);
                switch (productDetailResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        productDetailItem = productDetailResponse.getData().get(0);
                        price = productDetailItem.getPrice();
                        if (productDetailrequest.getIsClothes() == 1) {
                            getSizes(productDetailResponse.getData().get(0).getProductSize());
                        }
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(productDetailResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.GET_PRODUCT_DETAIL, productDetailrequest, ProductDetailResponse.class);

    }

    private void getSizes(List<ProductSizeItem> productSizeItems) {
        accessLoadingBar(VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                sizesResponse = ((SizesResponse) response);
                switch (sizesResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        List<SizeItem> sizeItems = sizesResponse.getData();
                        for (int i = 0; i < productSizeItems.size(); i++) {
                            for (int j = 0; j < sizeItems.size(); j++) {
                                if (productSizeItems.get(i).getSizeId() == sizeItems.get(j).getId())
                                    sizeItems.get(j).setFound(true);
                            }
                        }
                        Timber.e("array%s", sizeItems.toString());
                        getSizesAdapter().setColorsAdapter(getColorsAdapter());
                        getSizesAdapter().updateDataList(sizeItems, productDetailItem.getId());
                        break;
                    case WebServices.FAILED:
                        setMessage(sizesResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_SIZES, new Object(), SizesResponse.class);
    }

    public void setSizeItemId(int sizeItemId) {
        this.sizeItemId = sizeItemId;
        for (int i = 0; i < productDetailResponse.getData().get(0).getProductSize().size(); i++) {
            if (sizeItemId == productDetailResponse.getData().get(0).getProductSize().get(i).getSizeId()) {
                //equals
                price = productDetailResponse.getData().get(0).getProductSize().get(i).getPrice();
                notifyChange();
                return;
            }
        }
    }

    private void favouriteRequest() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                favResponse = ((ChangeFavResponse) response);
                switch (favResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        productDetailItem.setUserFavorite(favResponse.getIsFavorite());
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(favResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHANGE_FAVOURITE, favRequest, ChangeFavResponse.class);

    }

    public void onFavClick(int id) {
        favRequest.setProductId(id);
        favouriteRequest();
    }

    public void onBackClick() {
        setValue(Codes.PRESS_BACK);
    }

    public void onAddToCartClick() {
        accessLoadingBar(VISIBLE);
        addCartRequest.setProductId(productDetailResponse.getData().get(0).getId());
        if (productDetailrequest.getIsClothes() == 1) {
            addCartRequest.setIsClothes(1);
            addCartRequest.setSize(sizeItemId);
            addCartRequest.setProductColor(colorId);
            addCartRequest.setPrice(price);
        }
        addCartNetwork();
    }

    private void addCartNetwork() {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AddCartResponse addCartResponse = ((AddCartResponse) response);
                switch (addCartResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setMessage(addCartResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                    case WebServices.FAILED:
                        setMessage(addCartResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.ADD_UPDATE_CART, addCartRequest, AddCartResponse.class);
    }

    public void setColorId(int id) {
        this.colorId = id;
    }
}
