package com.routh.model.sendrequest;

import com.google.gson.annotations.SerializedName;

public class SendRequestResponse{

	@SerializedName("messsage")
	private String messsage;

	@SerializedName("status")
	private int status;

	public void setMesssage(String messsage){
		this.messsage = messsage;
	}

	public String getMesssage(){
		return messsage;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SendRequestResponse{" + 
			"messsage = '" + messsage + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}