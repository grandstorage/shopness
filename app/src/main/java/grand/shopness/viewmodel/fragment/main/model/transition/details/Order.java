package com.routh.model.transition.details;

import com.google.gson.annotations.SerializedName;

public class Order{

	@SerializedName("receive_location")
	private String receiveLocation;

	@SerializedName("sender_lng")
	private String senderLng;

	@SerializedName("receive_user_name")
	private String receiveUserName;

	@SerializedName("sender_phone")
	private String senderPhone;

	@SerializedName("receive_lat")
	private String receiveLat;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("receive_lng")
	private String receiveLng;

	@SerializedName("order_details")
	private String orderDetails;

	@SerializedName("sender_user_name")
	private String senderUserName;

	@SerializedName("sender_lat")
	private String senderLat;

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("receive_phone")
	private String receivePhone;

	@SerializedName("sender_location")
	private String senderLocation;

	public void setReceiveLocation(String receiveLocation){
		this.receiveLocation = receiveLocation;
	}

	public String getReceiveLocation(){
		return receiveLocation;
	}

	public void setSenderLng(String senderLng){
		this.senderLng = senderLng;
	}

	public String getSenderLng(){
		return senderLng;
	}

	public void setReceiveUserName(String receiveUserName){
		this.receiveUserName = receiveUserName;
	}

	public String getReceiveUserName(){
		return receiveUserName;
	}

	public void setSenderPhone(String senderPhone){
		this.senderPhone = senderPhone;
	}

	public String getSenderPhone(){
		return senderPhone;
	}

	public void setReceiveLat(String receiveLat){
		this.receiveLat = receiveLat;
	}

	public String getReceiveLat(){
		return receiveLat;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setReceiveLng(String receiveLng){
		this.receiveLng = receiveLng;
	}

	public String getReceiveLng(){
		return receiveLng;
	}

	public void setOrderDetails(String orderDetails){
		this.orderDetails = orderDetails;
	}

	public String getOrderDetails(){
		return orderDetails;
	}

	public void setSenderUserName(String senderUserName){
		this.senderUserName = senderUserName;
	}

	public String getSenderUserName(){
		return senderUserName;
	}

	public void setSenderLat(String senderLat){
		this.senderLat = senderLat;
	}

	public String getSenderLat(){
		return senderLat;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setReceivePhone(String receivePhone){
		this.receivePhone = receivePhone;
	}

	public String getReceivePhone(){
		return receivePhone;
	}

	public void setSenderLocation(String senderLocation){
		this.senderLocation = senderLocation;
	}

	public String getSenderLocation(){
		return senderLocation;
	}

	@Override
 	public String toString(){
		return 
			"Order{" + 
			"receive_location = '" + receiveLocation + '\'' + 
			",sender_lng = '" + senderLng + '\'' + 
			",receive_user_name = '" + receiveUserName + '\'' + 
			",sender_phone = '" + senderPhone + '\'' + 
			",receive_lat = '" + receiveLat + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",receive_lng = '" + receiveLng + '\'' + 
			",order_details = '" + orderDetails + '\'' + 
			",sender_user_name = '" + senderUserName + '\'' + 
			",sender_lat = '" + senderLat + '\'' + 
			",order_date = '" + orderDate + '\'' + 
			",receive_phone = '" + receivePhone + '\'' + 
			",sender_location = '" + senderLocation + '\'' + 
			"}";
		}
}