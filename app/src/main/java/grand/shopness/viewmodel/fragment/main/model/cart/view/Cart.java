package com.routh.model.cart.view;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Cart{

	@SerializedName("shipping")
	private String shipping;

	@SerializedName("Order_value")
	private int orderValue;

	@SerializedName("items")
	private List<CartItem> items;

	@SerializedName("subtotal_fees")
	private int subtotalFees;

	public void setShipping(String shipping){
		this.shipping = shipping;
	}

	public String getShipping(){
		return shipping;
	}

	public void setOrderValue(int orderValue){
		this.orderValue = orderValue;
	}

	public int getOrderValue(){
		return orderValue;
	}

	public void setItems(List<CartItem> items){
		this.items = items;
	}

	public List<CartItem> getItems(){
		return items;
	}

	public void setSubtotalFees(int subtotalFees){
		this.subtotalFees = subtotalFees;
	}

	public int getSubtotalFees(){
		return subtotalFees;
	}

	@Override
 	public String toString(){
		return 
			"Cart{" + 
			"shipping = '" + shipping + '\'' + 
			",order_value = '" + orderValue + '\'' + 
			",items = '" + items + '\'' + 
			",subtotal_fees = '" + subtotalFees + '\'' + 
			"}";
		}
}