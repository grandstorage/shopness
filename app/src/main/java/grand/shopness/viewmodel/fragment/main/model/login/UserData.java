package com.routh.model.login;

import com.google.gson.annotations.SerializedName;

public class UserData{

	@SerializedName("user_status")
	private int userStatus;

	@SerializedName("lng")
	private String lng;

	@SerializedName("phone")
	private String phone;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("jwt_token")
	private String jwtToken;

	@SerializedName("details_location")
	private String detailsLocation;

	@SerializedName("location")
	private String location;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("lat")
	private String lat;

	public void setUserStatus(int userStatus){
		this.userStatus = userStatus;
	}

	public int getUserStatus(){
		return userStatus;
	}

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng == null ? "0" : lng;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setJwtToken(String jwtToken){
		this.jwtToken = jwtToken;
	}

	public String getJwtToken(){
		return jwtToken;
	}

	public void setDetailsLocation(String detailsLocation){
		this.detailsLocation = detailsLocation;
	}

	public String getDetailsLocation(){
		return detailsLocation;
	}

	public void setLocation(String location){
		this.location = location;
	}

	public String getLocation(){
		return location;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat == null ? "0" : lat;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"user_status = '" + userStatus + '\'' + 
			",lng = '" + lng + '\'' + 
			",phone = '" + phone + '\'' + 
			",user_image = '" + userImage + '\'' + 
			",user_name = '" + userName + '\'' + 
			",jwt_token = '" + jwtToken + '\'' + 
			",details_location = '" + detailsLocation + '\'' + 
			",location = '" + location + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}