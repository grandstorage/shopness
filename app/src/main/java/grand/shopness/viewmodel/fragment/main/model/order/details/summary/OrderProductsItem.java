package com.routh.model.order.details.summary;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.routh.R;
import com.routh.application.BaseApplication;

public class OrderProductsItem{

	@SerializedName("product_desc")
	private String productDesc;

	@SerializedName("without_extra")
	private String withoutExtra;

	@SerializedName("additions")
	private List<String> additions;

	@SerializedName("total_price")
	private int totalPrice;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("price")
	private String price;

	@SerializedName("product_id")
	private int productId;

	@SerializedName("id")
	private int id;

	@SerializedName("order_qty")
	private int orderQty;

	@SerializedName("product_name")
	private String productName;

	@Expose
	private String adds;

	public void setAdds(String adds) {
		this.adds = adds;
	}

	public String getAdds() {
		StringBuilder builder = new StringBuilder();
		if (additions != null && additions.size() > 0) {
			builder.append(getString() + " ");
			for (String name :
					additions) {
				builder.append(name+" ");
			}
		}
		adds = builder.toString();
		return adds;
	}

	public void setProductDesc(String productDesc){
		this.productDesc = productDesc;
	}

	public String getProductDesc(){
		return productDesc;
	}

	public void setWithoutExtra(String withoutExtra){
		this.withoutExtra = withoutExtra;
	}

	public String getWithoutExtra(){
		return withoutExtra == null? " ": withoutExtra;
	}

	public void setAdditions(List<String> additions){
		this.additions = additions;
	}

	public List<String> getAdditions(){
		return additions;
	}

	public void setTotalPrice(int totalPrice){
		this.totalPrice = totalPrice;
	}

	public int getTotalPrice(){
		return totalPrice;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setOrderQty(int orderQty){
		this.orderQty = orderQty;
	}

	public int getOrderQty(){
		return orderQty;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	private String getString() {
		return BaseApplication.getInstance().getString(R.string.add);
	}



	@Override
 	public String toString(){
		return 
			"OrderProductsItem{" + 
			"product_desc = '" + productDesc + '\'' + 
			",without_extra = '" + withoutExtra + '\'' + 
			",additions = '" + additions + '\'' + 
			",total_price = '" + totalPrice + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",price = '" + price + '\'' + 
			",product_id = '" + productId + '\'' + 
			",id = '" + id + '\'' + 
			",order_qty = '" + orderQty + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}";
		}
}