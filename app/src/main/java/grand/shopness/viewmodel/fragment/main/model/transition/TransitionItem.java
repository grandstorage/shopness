package com.routh.model.transition;

import com.google.gson.annotations.SerializedName;

public class TransitionItem{

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("order_status")
	private OrderStatus orderStatus;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("id")
	private int id;

	@SerializedName("transfer_status")
	private int transferStatus;

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setOrderStatus(OrderStatus orderStatus){
		this.orderStatus = orderStatus;
	}

	public OrderStatus getOrderStatus(){
		return orderStatus;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTransferStatus(int transferStatus){
		this.transferStatus = transferStatus;
	}

	public int getTransferStatus(){
		return transferStatus;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"order_date = '" + orderDate + '\'' + 
			",order_status = '" + orderStatus + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",id = '" + id + '\'' + 
			",transfer_status = '" + transferStatus + '\'' + 
			"}";
		}
}