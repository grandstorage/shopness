package com.routh.model.offers.details;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OfferDetailsResponse{

	@SerializedName("data")
	private List<OfferDetailsItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<OfferDetailsItem> data){
		this.data = data;
	}

	public List<OfferDetailsItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OfferDetailsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}