package com.routh.model.transition;

import com.google.gson.annotations.SerializedName;

public class OrderStatus{

	@SerializedName("status_name")
	private String statusName;

	@SerializedName("id")
	private int id;

	public void setStatusName(String statusName){
		this.statusName = statusName;
	}

	public String getStatusName(){
		return statusName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"OrderStatus{" + 
			"status_name = '" + statusName + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}