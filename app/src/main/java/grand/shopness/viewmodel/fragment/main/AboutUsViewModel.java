package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.aboutus.AboutUsResponse;

public class AboutUsViewModel extends BaseViewModel {
    AboutUsResponse aboutUsResponse;

    public AboutUsViewModel() {
        aboutUsResponse = new AboutUsResponse();
        getAbout();
    }

    @Bindable
    public AboutUsResponse getAboutUsResponse() {
        return aboutUsResponse;
    }

    private void getAbout() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                aboutUsResponse = ((AboutUsResponse) response);
                switch (aboutUsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        notifyChange();
                        break;
                    case WebServices.FAILED:
                        setMessage(aboutUsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.ABOUT_US, new Object(), AboutUsResponse.class);
    }


}
