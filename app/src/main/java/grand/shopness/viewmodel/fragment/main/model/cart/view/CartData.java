package com.routh.model.cart.view;

import com.google.gson.annotations.SerializedName;

public class CartData{

	@SerializedName("cart")
	private Cart cart;

	public void setCart(Cart cart){
		this.cart = cart;
	}

	public Cart getCart(){
		return cart;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"cart = '" + cart + '\'' + 
			"}";
		}
}