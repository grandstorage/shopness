package com.routh.model.orders.view;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MyOrdersResponse{

	@SerializedName("data")
	private List<OrderItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<OrderItem> data){
		this.data = data;
	}

	public List<OrderItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MyOrdersResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}