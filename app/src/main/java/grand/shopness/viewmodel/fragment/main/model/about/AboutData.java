package com.routh.model.about;

import com.google.gson.annotations.SerializedName;

public class AboutData{

	@SerializedName("whats")
	private String whats;

	@SerializedName("twitter")
	private String twitter;

	@SerializedName("about_us")
	private String aboutUs;

	@SerializedName("facebook")
	private String facebook;

	@SerializedName("email")
	private String email;

	public void setWhats(String whats){
		this.whats = whats;
	}

	public String getWhats(){
		return whats;
	}

	public void setTwitter(String twitter){
		this.twitter = twitter;
	}

	public String getTwitter(){
		return twitter;
	}

	public void setAboutUs(String aboutUs){
		this.aboutUs = aboutUs;
	}

	public String getAboutUs(){
		return aboutUs;
	}

	public void setFacebook(String facebook){
		this.facebook = facebook;
	}

	public String getFacebook(){
		return facebook;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"whats = '" + whats + '\'' + 
			",twitter = '" + twitter + '\'' + 
			",about_us = '" + aboutUs + '\'' + 
			",facebook = '" + facebook + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}