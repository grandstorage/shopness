package com.routh.model.register;

import com.google.gson.annotations.SerializedName;

public class RegisterRequest {

    @SerializedName("phone")
    private String phone;
    @SerializedName("firebase_token")
    private String fireBaseToken;

    public String getFireBaseToken() {
        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        this.fireBaseToken = fireBaseToken;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
