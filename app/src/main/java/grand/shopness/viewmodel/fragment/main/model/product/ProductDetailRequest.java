package com.routh.model.product;

import com.google.gson.annotations.SerializedName;

public class ProductDetailRequest {

    @SerializedName("product_id")
    private int productId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
