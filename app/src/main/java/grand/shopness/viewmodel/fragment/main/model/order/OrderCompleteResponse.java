package com.routh.model.order;

import com.google.gson.annotations.SerializedName;

public class OrderCompleteResponse{

	@SerializedName("data")
	private OrderData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setOrderData(OrderData data){
		this.data = data;
	}

	public OrderData getOrderData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OrderCompleteResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}