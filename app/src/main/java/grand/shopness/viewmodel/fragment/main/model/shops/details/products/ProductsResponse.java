package com.routh.model.shops.details.products;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import com.routh.model.shops.details.ProductsItem;

public class ProductsResponse{

	@SerializedName("data")
	private List<ProductsItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<ProductsItem> data){
		this.data = data;
	}

	public List<ProductsItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}