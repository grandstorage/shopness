package com.routh.model.transition.details;

import com.google.gson.annotations.SerializedName;

public class OrderTransferStatus{

	@SerializedName("status_name")
	private String statusName;

	public void setStatusName(String statusName){
		this.statusName = statusName;
	}

	public String getStatusName(){
		return statusName;
	}

	@Override
 	public String toString(){
		return 
			"OrderTransferStatus{" + 
			"status_name = '" + statusName + '\'' + 
			"}";
		}
}