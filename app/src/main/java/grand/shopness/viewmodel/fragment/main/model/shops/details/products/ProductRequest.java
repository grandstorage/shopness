package com.routh.model.shops.details.products;

import com.google.gson.annotations.SerializedName;

public class ProductRequest {
    @SerializedName("category_id")
    private int categoryId;

    public ProductRequest(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
