package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import java.util.ArrayList;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.VolleyFileObject;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.bedelegate.BeDelegateRequest;
import grand.shopness.model.bedelegate.BeDelegateResponse;

public class BeDelegateViewModel extends BaseViewModel {
    private BeDelegateRequest beDelegateRequest;
    private String filePath;
    private ArrayList<VolleyFileObject> volleyFileObjects;

    public BeDelegateViewModel() {
        beDelegateRequest = new BeDelegateRequest();
        volleyFileObjects = new ArrayList<>();
    }

    public void uploadPhotoClick() {
        setValue(Codes.SELECT_PROFILE_IMAGE);
    }

    public void onLicenseClick() {
        setValue(Codes.ON_LICENSE_CLICK);
    }

    public void onNationalIdClick() {
        setValue(Codes.ON_NATIONAL_ID_CLICK);
    }

    public BeDelegateRequest getBeDelegateRequest() {
        return beDelegateRequest;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setBeDelegateRequest(BeDelegateRequest beDelegateRequest) {
        this.beDelegateRequest = beDelegateRequest;
    }

    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }


    public void registerBtnClick() {
        notifyChange();
        goRegister();
    }

    private void goRegister() {

        accessLoadingBar(View.VISIBLE);
        beDelegateRequest.setTypeId("2"); //2 for delegate
        beDelegateRequest.setLat(30); // static
        beDelegateRequest.setLng(31);//static
        beDelegateRequest.setLocation("location");// static
        beDelegateRequest.setPassword("123456"); // static
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                BeDelegateResponse beDelegateResponse = (BeDelegateResponse) response;
                if (beDelegateResponse.getStatus() == WebServices.SUCCESS) {
                    setMessage(beDelegateResponse.getMessage());
                    setValue(Codes.SHOW_MESSAGE);
                    setValue(Codes.HOME_SCREEN);
                } else if (beDelegateResponse.getStatus() == WebServices.FAILED) {
                    setMessage(beDelegateResponse.getMessage());
                    setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).multiPartConnect(WebServices.REGISTER, beDelegateRequest, volleyFileObjects, BeDelegateResponse.class);
    }


}
