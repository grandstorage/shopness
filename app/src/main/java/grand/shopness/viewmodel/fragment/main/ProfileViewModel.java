package grand.shopness.viewmodel.fragment.main;

import android.view.View;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import java.util.ArrayList;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.VolleyFileObject;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.profile.ProfileItem;
import grand.shopness.model.profile.ProfileRequest;
import grand.shopness.model.profile.ProfileResponse;
import timber.log.Timber;

public class ProfileViewModel extends BaseViewModel {

    private ProfileRequest profileRequest;
    private ArrayList<VolleyFileObject> volleyFileObjects;
    private ProfileResponse profileResponse;

    public ProfileViewModel() {
        profileRequest = new ProfileRequest();
        volleyFileObjects = new ArrayList<>();
        getProfile();
        notifyChange();
    }

    public void uploadPhotoClick() {
        setValue(Codes.SELECT_PROFILE_IMAGE);
    }

    @Bindable
    public ProfileRequest getProfileRequest() {
        return profileRequest;
    }

    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }
    
    @BindingAdapter({"imageUrl"})
    public static void setImageViewResource(ImageView imageView, String imagePath) {
        ConnectionHelper.loadImage(imageView, imagePath);
    }

    public void onChangePasswordClick() {
        setValue(Codes.CHANGE_PASSWORD_SCREEN);
    }

    public void updateBtnClick() {
        update();
    }

    private void update() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                profileResponse = (ProfileResponse) response;
                if (profileResponse.getStatus() == WebServices.SUCCESS) {
                    setMessage(profileResponse.getMessage());
                    setValue(Codes.SHOW_MESSAGE);
                    saveUserData();
                    notifyChange();
                } else if (profileResponse.getStatus() == WebServices.FAILED) {
                    setMessage(profileResponse.getMessage());
                    setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).multiPartConnect(WebServices.EDIT_PROFILE, profileRequest, volleyFileObjects, ProfileResponse.class);
    }

    private void saveUserData() {
        if (profileResponse.getData()!= null) {
            ProfileItem item = profileResponse.getData();
            UserPreferenceHelper.getUserLoginDetails().setName(item.getName());
            Timber.e(UserPreferenceHelper.getUserLoginDetails().getName());
            UserPreferenceHelper.getUserLoginDetails().setEmail(item.getEmail());
            UserPreferenceHelper.getUserLoginDetails().setPhone(item.getPhone());
            UserPreferenceHelper.getUserLoginDetails().setLocation(item.getLocation());
            UserPreferenceHelper.getUserLoginDetails().setLat(item.getLat());
            UserPreferenceHelper.getUserLoginDetails().setLng(item.getLng());
            UserPreferenceHelper.getUserLoginDetails().setUserImage(item.getUserImage());
        }
    }

    private void getProfile() {
        profileRequest.setImage(UserPreferenceHelper.getUserLoginDetails().getUserImage());
        profileRequest.setName(UserPreferenceHelper.getUserLoginDetails().getName());
        profileRequest.setEmail(UserPreferenceHelper.getUserLoginDetails().getEmail());
        profileRequest.setAddress(UserPreferenceHelper.getUserLoginDetails().getLocation());
        profileRequest.setPhone(UserPreferenceHelper.getUserLoginDetails().getPhone());
        profileRequest.setLat(Double.valueOf(UserPreferenceHelper.getUserLoginDetails().getLat()));
        profileRequest.setLng(Double.valueOf(UserPreferenceHelper.getUserLoginDetails().getLng()));
    }

}
