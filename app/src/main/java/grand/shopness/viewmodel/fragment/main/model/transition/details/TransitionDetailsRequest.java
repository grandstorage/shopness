package com.routh.model.transition.details;

import com.google.gson.annotations.SerializedName;

public class TransitionDetailsRequest {

    @SerializedName("order_id")
    private int orderId;


    public TransitionDetailsRequest(int orderId) {
        this.orderId = orderId;
    }


}
