package com.routh.model.shops.details;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ShopDetailItem{

	@SerializedName("address")
	private String address;

	@SerializedName("arrival_time")
	private String arrivalTime;

	@SerializedName("shipping")
	private String shipping;

	@SerializedName("mini_charge")
	private String miniCharge;

	@SerializedName("id")
	private int id;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("shop_image")
	private String shopImage;

	@SerializedName("category")
	private List<CategoryItem> category;

	@SerializedName("products")
	private List<ProductsItem> products;

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setArrivalTime(String arrivalTime){
		this.arrivalTime = arrivalTime;
	}

	public String getArrivalTime(){
		return arrivalTime;
	}

	public void setShipping(String shipping){
		this.shipping = shipping;
	}

	public String getShipping(){
		return shipping;
	}

	public void setMiniCharge(String miniCharge){
		this.miniCharge = miniCharge;
	}

	public String getMiniCharge(){
		return miniCharge;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setShopName(String shopName){
		this.shopName = shopName;
	}

	public String getShopName(){
		return shopName;
	}

	public void setShopImage(String shopImage){
		this.shopImage = shopImage;
	}

	public String getShopImage(){
		return shopImage;
	}

	public void setCategory(List<CategoryItem> category){
		this.category = category;
	}

	public List<CategoryItem> getCategory(){
		return category;
	}

	public void setProducts(List<ProductsItem> products){
		this.products = products;
	}

	public List<ProductsItem> getProducts(){
		return products;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"address = '" + address + '\'' + 
			",arrival_time = '" + arrivalTime + '\'' + 
			",shipping = '" + shipping + '\'' + 
			",mini_charge = '" + miniCharge + '\'' + 
			",id = '" + id + '\'' + 
			",shop_name = '" + shopName + '\'' + 
			",shop_image = '" + shopImage + '\'' + 
			",category = '" + category + '\'' + 
			",products = '" + products + '\'' + 
			"}";
		}
}