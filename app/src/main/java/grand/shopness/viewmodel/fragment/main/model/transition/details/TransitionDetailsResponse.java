package com.routh.model.transition.details;

import com.google.gson.annotations.SerializedName;

public class TransitionDetailsResponse{

	@SerializedName("data")
	private TransitionData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(TransitionData data){
		this.data = data;
	}

	public TransitionData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"TransitionDetailsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}