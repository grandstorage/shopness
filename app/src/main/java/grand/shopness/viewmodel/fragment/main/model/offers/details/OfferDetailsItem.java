package com.routh.model.offers.details;

import com.google.gson.annotations.SerializedName;

public class OfferDetailsItem{

	@SerializedName("discount_offer")
	private String discountOffer;

	@SerializedName("user_favorite")
	private int userFavorite;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("price")
	private int price;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("priceAfterDiscount")
	private int priceAfterDiscount;

	public void setDiscountOffer(String discountOffer){
		this.discountOffer = discountOffer;
	}

	public String getDiscountOffer(){
		return discountOffer;
	}

	public void setUserFavorite(int userFavorite){
		this.userFavorite = userFavorite;
	}

	public int getUserFavorite(){
		return userFavorite;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setPriceAfterDiscount(int priceAfterDiscount){
		this.priceAfterDiscount = priceAfterDiscount;
	}

	public int getPriceAfterDiscount(){
		return priceAfterDiscount;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"discount_offer = '" + discountOffer + '\'' + 
			",user_favorite = '" + userFavorite + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",price = '" + price + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",product_name = '" + productName + '\'' + 
			",priceAfterDiscount = '" + priceAfterDiscount + '\'' + 
			"}";
		}
}