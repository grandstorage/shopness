package com.routh.model.shops.details;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 8/4/2019.
 **/
public class ShopDetailsRequest {

    @SerializedName("shop_id")
    private int shopId;


    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }
}
