package com.routh.model.payment;

import java.util.ArrayList;
import java.util.List;

import com.routh.R;
import com.routh.application.BaseApplication;

public class PaymentItem {

    private int id;

    private String name;

    private int icon;

    public PaymentItem() {
    }

    private PaymentItem(int id, String name, int icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    private String getString(int resId){
        return BaseApplication.getInstance().getApplicationContext().getString(resId);
    }

    public List<PaymentItem> getPaymentItems() {
        List<PaymentItem> items = new ArrayList<>();
        items.add(new PaymentItem(1,getString(R.string.pay_on_delivery), R.drawable.ic_shopping_bag2));
//        items.add(new PaymentItem(2,getString(R.string.pay_online), R.drawable.ic_credit_card));
        return items;
    }
}
