package grand.shopness.viewmodel.fragment.login;

import android.view.View;

import java.util.ArrayList;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.VolleyFileObject;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.register.request.RegisterRequest;
import grand.shopness.model.register.response.RegisterResponse;

public class RegisterViewModel extends BaseViewModel {
    private RegisterRequest registerRequest;
    private ArrayList<VolleyFileObject> volleyFileObjects;

    public RegisterViewModel() {
        registerRequest = new RegisterRequest();
        volleyFileObjects = new ArrayList<>();
    }

    public void uploadPhotoClick() {
        setValue(Codes.SELECT_PROFILE_IMAGE);
    }

    public void onBackClick() {
        setValue(Codes.PRESS_BACK);
    }

    public RegisterRequest getRegisterRequest() {
        return registerRequest;
    }

    public ArrayList<VolleyFileObject> getVolleyFileObjects() {
        return volleyFileObjects;
    }

    public void onTermsTxtClick() {
        setValue(Codes.TERMS_TEXT_CLICK);
    }

    public void registerBtnClick() {
        notifyChange();
        goRegister();
    }

    private void goRegister() {

        accessLoadingBar(View.VISIBLE);
        registerRequest.setTypeId("1");


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                RegisterResponse registerResponse = (RegisterResponse) response;
                if (registerResponse.getStatus() == WebServices.SUCCESS) {
                    setValue(Codes.ENTER_CODE_SCREEN);
                } else if (registerResponse.getStatus() == WebServices.FAILED) {
                    setMessage(registerResponse.getMessage());
                    setValue(Codes.SHOW_MESSAGE);
                }
                accessLoadingBar(View.GONE);
            }
        }).multiPartConnect(WebServices.REGISTER, registerRequest, volleyFileObjects, RegisterResponse.class);
    }


}
