package com.routh.model.updatelocation;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 8/26/2019.
 **/
public class UpdateLocationRequest {

    @SerializedName("location")
    private String location;
    @SerializedName("details_location")
    private String details_location;
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDetails_location() {
        return details_location;
    }

    public void setDetails_location(String details_location) {
        this.details_location = details_location;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
