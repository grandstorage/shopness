package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.newpassword.NewPasswordRequest;
import grand.shopness.model.newpassword.NewPasswordResponse;

public class ChangePasswordViewModel extends BaseViewModel {

    private NewPasswordRequest newPasswordRequest;

    public ChangePasswordViewModel() {
        newPasswordRequest = new NewPasswordRequest();
    }

    public NewPasswordRequest getNewPasswordRequest() {
        return newPasswordRequest;
    }

    public void onBackClick(){
        setValue(Codes.ENTER_CODE_SCREEN);
    }

    public void onSaveClick(){
        notifyChange();
        if (newPasswordRequest.isEqual())
            submit();
        else {
            setValue(Codes.NOT_EQUAL);
        }
    }

    private void submit() {
        accessLoadingBar(View.VISIBLE);
        setValue(Codes.BLOCK);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                NewPasswordResponse passwordResponse = (NewPasswordResponse) response;
                switch (passwordResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setMessage(passwordResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                    case WebServices.FAILED:
                        setMessage(passwordResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                setValue(Codes.DONE);
            }
        }).requestJsonObject(Request.Method.POST,
                WebServices.PROFILE + WebServices.CHANGE_PASSWORD,
                newPasswordRequest,
                NewPasswordResponse.class);
    }
}
