package com.routh.model.main;

import com.google.gson.annotations.SerializedName;

public class MainResponse{

	@SerializedName("data")
	private MainData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(MainData data){
		this.data = data;
	}

	public MainData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MainResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}