package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.myorders.MyOrdersResponse;
import grand.shopness.view.adapter.parent.MyOrdersAdapter;

public class MyOrdersViewModel extends BaseViewModel {

    public ObservableBoolean isLoading = new ObservableBoolean();
    private MyOrdersAdapter myOrdersAdapter;

    public MyOrdersViewModel() {
        startPopulateData();
    }

    @BindingAdapter({"android:adapter"})
    public static void getOrdersBinding(RecyclerView recyclerView, MyOrdersAdapter allShopsAdapter) {
        //you can set layout manager here
        recyclerView.setAdapter(allShopsAdapter);
    }

    public void onRefresh() {
        isLoading.set(true);
        startPopulateData();
    }

    @Bindable
    public MyOrdersAdapter getMyOrdersAdapter() {
        return this.myOrdersAdapter == null ? this.myOrdersAdapter = new MyOrdersAdapter() : this.myOrdersAdapter;
    }

    private void startPopulateData() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
               MyOrdersResponse ordersResponse = ((MyOrdersResponse) response);
                switch (ordersResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        getMyOrdersAdapter().updateDataList(ordersResponse.getData());
                        break;
                    case WebServices.FAILED:
                        setMessage(ordersResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
                isLoading.set(false);
            }
        }).requestJsonObject(Request.Method.GET, WebServices.GET_MY_ORDERS, new Object(), MyOrdersResponse.class);


    }



}
