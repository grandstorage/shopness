package com.routh.model.cart.view;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.routh.R;
import com.routh.application.BaseApplication;

import java.util.List;

public class CartItem {


    @SerializedName("additions")
    private List<String> additions;

    @SerializedName("product_image")
    private String productImage;

    @SerializedName("qty")
    private int qty;

    @SerializedName("product_id")
    private int productId;

    @SerializedName("Order_value")
    private int orderValue;

    @SerializedName("description")
    private String description;

    @SerializedName("cart_item_id")
    private int cartItemId;

    @SerializedName("product_price")
    private int productPrice;

    @SerializedName("product_name")
    private String productName;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return qty;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    public void setOrderValue(int orderValue) {
        this.orderValue = orderValue;
    }

    public int getOrderValue() {
        return orderValue;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    public int getCartItemId() {
        return cartItemId;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }


    private String getString() {
        return BaseApplication.getInstance().getString(R.string.add);
    }

    public String getAdds() {
        StringBuilder builder = new StringBuilder();
        if (additions != null && additions.size() > 0) {
            builder.append(getString() + " ");
            for (String name :
                    additions) {
                builder.append(name+" ");
            }
        }
        return builder.toString();
    }

    @Override
    public String toString() {
        return
                "ItemsItem{" +
                        "additions = '" + additions + '\'' +
                        ",product_image = '" + productImage + '\'' +
                        ",qty = '" + qty + '\'' +
                        ",product_id = '" + productId + '\'' +
                        ",order_value = '" + orderValue + '\'' +
                        ",description = '" + description + '\'' +
                        ",cart_item_id = '" + cartItemId + '\'' +
                        ",product_price = '" + productPrice + '\'' +
                        ",product_name = '" + productName + '\'' +
                        "}";
    }
}