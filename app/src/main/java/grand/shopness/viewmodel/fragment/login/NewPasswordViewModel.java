package grand.shopness.viewmodel.fragment.login;

import android.view.View;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.forgotpassword.ForgotPasswordResponse;
import grand.shopness.model.newpassword.NewPasswordRequest;
import grand.shopness.model.newpassword.NewPasswordResponse;

public class NewPasswordViewModel extends BaseViewModel {

    private NewPasswordRequest newPasswordRequest;

    public NewPasswordViewModel() {
        newPasswordRequest = new NewPasswordRequest();
    }

    public NewPasswordRequest getNewPasswordRequest() {
        return newPasswordRequest;
    }

    public void onBackClick(){
        setValue(Codes.ENTER_CODE_SCREEN);
    }

    public void onSaveClick(){
        notifyChange();
        if (newPasswordRequest.isEqual())
        submit();
        else {
            setValue(Codes.NOT_EQUAL);
        }
    }

    private void submit() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                NewPasswordResponse forgotPassResponse = (NewPasswordResponse) response;
                switch (forgotPassResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setValue(Codes.HOME_SCREEN);
                        break;
                    case WebServices.FAILED:
                        setMessage(forgotPassResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST,WebServices.PROFILE + WebServices.CHANGE_PASSWORD, newPasswordRequest, NewPasswordResponse.class);
    }
}
