package grand.shopness.viewmodel.fragment.main;

import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.suggestion.SuggestionRequest;
import grand.shopness.model.suggestion.SuggetionResponse;

public class SuggestionViewModel extends BaseViewModel {

    SuggestionRequest suggestionRequest;

    public SuggestionViewModel() {
        suggestionRequest = new SuggestionRequest();
    }

    @Bindable
    public SuggestionRequest getSuggestionRequest() {
        return suggestionRequest;
    }

    public void onSubmitClick() {
        accessLoadingBar(View.VISIBLE);
        notifyChange(); // to get data from xml
        goSubmit();
    }

    private void goSubmit() {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                SuggetionResponse suggetionResponse = (SuggetionResponse) response;
                switch (suggetionResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setMessage(suggetionResponse.getMessage());
                        setValue(Codes.HOME_SCREEN);
                        break;
                    case WebServices.FAILED:
                        setMessage(suggetionResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.SUGGESTION, suggestionRequest, SuggetionResponse.class);
    }


}
