package com.routh.model.order.details.summary;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 8/23/2019.
 **/
public class OrdersSummaryRequest {

    @SerializedName("order_id")
    private int orderId;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
