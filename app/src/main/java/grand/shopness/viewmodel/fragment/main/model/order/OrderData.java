package com.routh.model.order;

import com.google.gson.annotations.SerializedName;

public class OrderData{

	@SerializedName("order_id")
	private int orderId;

	public void setOrderId(int orderId){
		this.orderId = orderId;
	}

	public int getOrderId(){
		return orderId;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"order_id = '" + orderId + '\'' + 
			"}";
		}
}