package com.routh.model.shipping;

import com.google.gson.annotations.SerializedName;

public class ShippingResponse{

	@SerializedName("address_id")
	private int addressId;

	@SerializedName("messsage")
	private String messsage;

	@SerializedName("status")
	private int status;

	public void setAddressId(int addressId){
		this.addressId = addressId;
	}

	public int getAddressId(){
		return addressId;
	}

	public void setMesssage(String messsage){
		this.messsage = messsage;
	}

	public String getMesssage(){
		return messsage;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ShippingResponse{" + 
			"address_id = '" + addressId + '\'' + 
			",messsage = '" + messsage + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}