package com.routh.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductAdds {

	@SerializedName("id")
	private int id;

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String extraProduct;

	@Expose
	private boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setExtraProduct(String extraProduct){
		this.extraProduct = extraProduct;
	}

	public String getExtraProduct(){
		return extraProduct;
	}

	@Override
 	public String toString(){
		return 
			"ProductAddedItem{" +
			"id = '" + id + '\'' +
			"price = '" + price + '\'' + 
			",extra_product = '" + extraProduct + '\'' + 
			"}";
		}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}