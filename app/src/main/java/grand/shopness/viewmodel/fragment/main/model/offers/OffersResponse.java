package com.routh.model.offers;

import com.google.gson.annotations.SerializedName;

public class OffersResponse{

	@SerializedName("data")
	private OfferData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(OfferData data){
		this.data = data;
	}

	public OfferData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"OffersResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}