package grand.shopness.viewmodel;

import android.content.Context;

import java.util.ArrayList;

import grand.shopness.R;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.model.startfrag.StartScreenModel;

public class StartScreenViewModel extends BaseViewModel {
    StartScreenModel startScreenModel;
    public StartScreenViewModel() {
        startScreenModel = new StartScreenModel();
        setValue(Codes.SET_DATA);
    }

    public StartScreenModel getStartScreenModel() {
        return startScreenModel;
    }


    public void onLoginClick(){
        setValue(Codes.LOGIN_SCREEN);
    }

    public void onShopsClick(){
        setValue(Codes.HOME_SCREEN);
    }

}
