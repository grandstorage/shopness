package grand.shopness.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import grand.shopness.base.constantsutils.Codes;
import grand.shopness.view.ui.activity.MainActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import pl.charmas.android.reactivelocation2.ReactiveLocationProviderConfiguration;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.os.Build.VERSION_CODES.M;

public class LocationHelper {
    private Context context;
    private ReactiveLocationProvider locationProvider;
    private Observable<Location> lastKnownLocationObservable;

    String[] locationList = new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION};

    @SuppressLint("MissingPermission")
    private LocationHelper(Context context) {
        this.context = context;
        locationProvider = new ReactiveLocationProvider(context.getApplicationContext(), ReactiveLocationProviderConfiguration
                .builder()
                .setRetryOnConnectionSuspended(true)
                .build()
        );

        final LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(100);

    }

    @SuppressLint("MissingPermission")
    @RequiresApi(api = M)
    public Observable<Location> getLastKnownLocationObservable(Fragment fragment) {
//        if (PermissionUtily.getInstance(context).requestPermission(fragment)) {
//            lastKnownLocationObservable = locationProvider
//                    .getLastKnownLocation()
//                    .observeOn(AndroidSchedulers.mainThread());
//        }
        return lastKnownLocationObservable;
    }

    public static LocationHelper getInstance(Context context) {
        return new LocationHelper(context);
    }


}
