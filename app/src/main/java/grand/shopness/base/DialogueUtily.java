package grand.shopness.base;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

/**
 * Created by MahmoudAyman on 6/13/2019.
 **/
public class DialogueUtily {
    private final MyAlertDialogue dialog;

    private DialogueUtily() {
        dialog = new MyAlertDialogue();

    }

    public static DialogueUtily getInstance() {
        return new DialogueUtily();
    }

    public DialogFragment createCustomDialouge(Fragment fragment, String title, String msg, String buttonYesTxt, String buttonNoTxt, int requestCode){

        Bundle args = new Bundle();
        args.putString(MyAlertDialogue.ARG_TITLE, title);
        args.putString(MyAlertDialogue.ARG_MESSAGE, msg);
        args.putString(MyAlertDialogue.ARG_BTN_YES, buttonYesTxt);
        args.putString(MyAlertDialogue.ARG_BTN_NO, buttonNoTxt);
        dialog.setArguments(args);
        dialog.setTargetFragment(fragment, requestCode);
        return dialog;
    }

    public DialogFragment createYesNoDialouge(Fragment fragment, String title, String msg,
                                         int requestCode){

        Bundle args = new Bundle();
        args.putString(MyAlertDialogue.ARG_TITLE, title);
        args.putString(MyAlertDialogue.ARG_MESSAGE, msg);
        args.putString(MyAlertDialogue.ARG_BTN_YES, null);
        args.putString(MyAlertDialogue.ARG_BTN_NO, null);
        dialog.setArguments(args);
        dialog.setTargetFragment(fragment, requestCode);
        return dialog;
    }



}
