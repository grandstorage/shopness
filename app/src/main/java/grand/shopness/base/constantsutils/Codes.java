package grand.shopness.base.constantsutils;

public class Codes {
    /*
    never set 0 : View.Visible or 8 : View.Gone     @MahmoudAyman
     */

    public static final int SET_DATA = 1;
    public static final int DONE = 2000;
    public static final int BLOCK = 2001;
    public static final int PROGRESS = 2002;
    public static final int ANIM = 2003;

    //RequestsCodes
    public static final int TYPE_IMAGE_REQUEST_CODE = 10;



    public static int TYPE_VIDEO_REQUEST_CODE = 11;
    public static int TYPE_PDF_REQUEST_CODE = 12;
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 13;
    public static final int ALERT_DIALOGUE_LOCATION_REQ = 14;
    public static final int PERMISSION_IMAGE_REQUEST_CODE = 15;
    public static final int ALERT_DIALOGUE_GPS_REQ = 16;
    public static final int GPS_SETTINGS_REQ_CODE = 17;
    public static final int BUNDLE_RESULT_CODE = 18;
    public static final int ON_LICENSE_CLICK = 19;
    public static final int ON_NATIONAL_ID_CLICK = 20;

    //ACTIONS from 21
    public static int HIDE_LOADING_BAR = 21;
    public static final int SHOW_MESSAGE = 22;
    public static final int SAVE_TO_SHARED_PREFERENCE = 23;
    public static int SHOW_COUNTRY_MENU = 24;
    public static final int PRESS_BACK = 33;
    public static final int PRESS_SKIP = 35;
    public static final int TOOLBAR_SEARCH_CLICK = 36;
    public static final int ACCESS_TOOLBAR = 37;

    //Screens (Fragment) code from 100
    public static final int SHOPS_FRAGMENT = 100;
    public static final int REGISTER_SCREEN = 101;
    public static final int FORGOT_PASSWORD_SCREEN = 102;
    public static final int ENTER_CODE_SCREEN = 103;
    public static final int CHANGE_PASSWORD_SCREEN = 104;
    public static final int HOME_SCREEN = 105;
    public static final int LOGIN_SCREEN = 106;
    public static final int START_SCREEN = 107;
    public static final int SHOP_DETAILS_SCREEN = 106;
    public static final int REVIEWS_SCREEN = 107;
    public static final int PRODUCT_DETAILS = 108;
    public static final int DIALOG = 109;
    public static final int CHANGE_AREA = 110;
    public static final int QUANTITY = 111;
    public static final int CHECKOUT_SCREEN = 112;
    public static final int SHIPPING_SCREEN = 113;
    public static final int PAYMENT_SCREEN = 114;
    public static final int CONFIRMATION_SCREEN = 115;
    public static final int CART_SCREEN = 116;
    public static final int ORDER_DETAILS = 117;
    public static final int CHAT_SCREEN = 118;
    public static final int FILTER_SCREEN = 119;

    public static int SELECT_COMMERICAL_IMAGE = 229;
    public static final int SELECT_PROFILE_IMAGE = 210;
    public static int SELECT_ACCOUNT_TYPE = 211;
    public static int SELECT_ORDER_IMAGE = 212;


    //mutable codes
    public static final int MAP_SCREEN = 300;
    public static final int TERMS_TEXT_CLICK = 301;
    public static final int ON_ALL_CLICK = 302;
    public static final int MOVE_SLIDER = 303;

    public static final int NOT_EQUAL = 1000;




}
