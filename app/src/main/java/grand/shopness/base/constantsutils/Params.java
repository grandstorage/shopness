package grand.shopness.base.constantsutils;

public class Params {
    //SharedPreferenceParams
    public static final String PREF_USER_NAME="user_name";

    //BundleParams
    public static final String BUNDLE_PAGE="user_details";

    //IntentParams
    public static final String INTENT_SETTINGS="settings";
    public static final String INTENT_PAGE="page";


    //data in bundle
    public static final String SERVICE_ID = "service_id";

    public static final String TOOLBAR_TITLE = "toolbar_title";
    public static final String SHOP_ID = "shop_id";
    public static final String PRODUCT_ID = "product_id";
    public static final String IS_CLOTHES = "is_clothes";
    public static final String ORDER_ID = "order_id";
}
