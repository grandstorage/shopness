package grand.shopness.base.constantsutils;

/**
 * Created by MAyman on 4/2/18.
 */

public class WebServices {
    //Status
    public static final int SUCCESS = 200;
    public static final int FAILED = 401;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;
    public static final int NOT_ACTIVE = 405;


    public static final String TYPE_SCHEDULED = "scheduled";
    public static final String TYPE_URGENT = "urgent";
    public static final String TYPE_USER = "user";
    public static final String TYPE_WORKER = "worker";


    public static final String PROFILE = "user/profile";

    //URLS
    public static final String BASE_URL = "http://shopnes.my-staff.net/api/";
    public static final String LOGIN = "login";
    public static final String SEND_CODE = "activateAccount";
    public static final String REGISTER = "register";
    public static final String ABOUT_US = "about_us";
    public static final String CHECK_PHONE = "CheckPhone";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String FAQ = "FAQ";
    public static final String PRIVACY_TERMS = "terms_and_conditions";
    public static final String SUGGESTION = "suggestion/add";
    public static final String PROMO_CODE = TYPE_USER + "/PromoCode";
    public static final String SERVICES = TYPE_USER + "/services";
    public static final String GET_ALL_SHOPS = TYPE_USER + "/FilterShopbylatlng";
    public static final String GET_SHOPS = "shop?";
    public static final String FOLLOW = TYPE_USER + "/createFollowShop";
    public static final String GET_PRODUCTS = "productsitems?";
    public static final String GET_REVIEWS = TYPE_USER + "/view_comments";
    public static final String SEND_COMMENT = TYPE_USER + "/user_review";
    public static final String CHANGE_FAVOURITE = TYPE_USER + "/favorite/add_or_remove";
    public static final String GET_OFFERS = "Shops/offers";
    public static final String GET_SIZES = "Product/ShowSizes";
    public static final String GET_PRODUCT_DETAIL = "ShowProduct";
    public static final String GET_COLORS = "Product/ShowColors";
    public static final String EDIT_PROFILE = TYPE_USER + "/profile/edit";
    public static final String GET_WISHLIST = TYPE_USER + "/favorite";
    public static final String ADD_UPDATE_CART = TYPE_USER + "/cart/add_or_update";
    public static final String GET_CART = TYPE_USER + "/cart";
    public static final String UPDATE_ADDRESS = "order/" + TYPE_USER + "/UpdateAddress";
    public static final String GET_MY_ORDERS = " order/" + TYPE_USER + "/list";

    //params
    public static String USER_NAME = "user_name";
    public static final String PHONE = "phone";
    public static final String GOOGLE_TOKEN = "google_token";
    public static final String USER_PROFILE = "profile_image";

    public static final String IMAGE = "image";
    public static final String USER_IMAGE = "user_image";
    public static final String License = "license";
    public static final String DELEGATE_ID = "delegate_id";


}
