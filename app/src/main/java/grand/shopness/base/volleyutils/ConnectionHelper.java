package grand.shopness.base.volleyutils;

import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.VolleyFileObject;
import timber.log.Timber;


public class ConnectionHelper {

    private static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.color.white)
            .showImageOnLoading(R.color.white)
            .showImageOnFail(R.color.white)
            .cacheInMemory(true)
            .cacheOnDisk(true).build();

    private static ImageLoader imageLoader = ImageLoader.getInstance();
    private ConnectionListener connectionListener;
    private RequestQueue queue;
    private static final int TIME_OUT = 10000;
    private Gson gson;

    public ConnectionHelper(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
        queue = MyApplication.getInstance().getRequestQueue();
        gson = new Gson();
    }


    public void requestJsonObject(int method, String url, Object requestData, final Class<?> responseType) {
        final Gson gson = new Gson();
        String link = WebServices.BASE_URL + url;

        link = link.replaceAll(" ", "%20");
        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(gson.toJson(requestData));
        } catch (Exception e) {
            e.getStackTrace();
        }

        Timber.e(url);
        if (jsonObject != null) {
            Timber.e(jsonObject.toString());
        } else {
            Timber.e("Make sure that you added request correctly");
        }


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(method, link, jsonObject,

                response -> {
                    Timber.e(response.toString());
                    parseData(response, responseType);
                }
                , volleyError -> {
            showErrorDetails(volleyError);
            connectionListener.onRequestError(volleyError);
        }) {
            @Override
            public Map getHeaders() {

                return getCustomHeaders();
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonObjReq);

    }


    public void multiPartConnect(String url, final Object requestData, final List<VolleyFileObject> volleyFileObjects, final Class<?> responseType) {

        String link = WebServices.BASE_URL + url;
        link = link.replaceAll(" ", "%20");
        Timber.e("theerror%s", link);


        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, link, networkResponse -> {
            String responseString = new String(networkResponse.data);
            JSONObject response = null;
            try {
                Timber.e("theerror%s", responseString);
                response = new JSONObject(responseString);
            } catch (Exception e) {
                e.getStackTrace();
            }
            parseData(response, responseType);
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                showErrorDetails(volleyError);
                connectionListener.onRequestError(volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return getParameters(requestData);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getCustomHeaders();
            }

            @Override
            protected Map<String, DataPart> getByteData() {

                return getFileParameters(volleyFileObjects);
            }

        };

        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(multipartRequest);
    }

    public HashMap<String, String> getCustomHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        //header
        String token = UserPreferenceHelper.getUserLoginDetails().getJwtToken();
        headers.put("jwt", token);
        headers.put("lang", UserPreferenceHelper.getCurrentLanguage());
        return headers;
    }


    public static void loadImage(final ImageView image, String imageUrl) {
        Timber.d(imageUrl);
        imageLoader.displayImage(imageUrl, image, options);
    }


    private void showErrorDetails(VolleyError volleyError) {
        String body;

        Timber.e("theerror");

        try {
            final String statusCode = String.valueOf(volleyError.networkResponse.statusCode);
            body = new String(volleyError.networkResponse.data, StandardCharsets.UTF_8);
            Timber.e("Error Body " + body + " StatusCode " + statusCode);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    private void parseData(JSONObject response, final Class<?> responseType) {

        try {
            System.out.println("Response " + response.toString());
            if (response.toString().equals("")) {
                connectionListener.onRequestError(null);
            } else {
                connectionListener.onRequestSuccess(gson.fromJson(response.toString(), responseType));
            }
        } catch (Exception e) {
            connectionListener.onRequestError(null);
        }

    }

    private Map<String, String> getParameters(final Object requestData) {
        Map<String, String> params = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject(gson.toJson(requestData));
            for (int i = 0; i < Objects.requireNonNull(jsonObject.names()).length(); i++) {
                params.put(jsonObject.names().getString(i), jsonObject.get(jsonObject.names().getString(i)) + "");
                Timber.e("%s", jsonObject.get(Objects.requireNonNull(jsonObject.names()).getString(i)));
            }
            Timber.e("%s", params.size());
        } catch (Exception e) {
            Timber.e(e);
        }
        return params;
    }

    private Map<String, VolleyMultipartRequest.DataPart> getFileParameters(List<VolleyFileObject> volleyFileObjects) {
        Map<String, VolleyMultipartRequest.DataPart> filesParams = new HashMap<>();
        if (volleyFileObjects == null) {
            return filesParams;
        }

        for (int i = 0; i < volleyFileObjects.size(); i++) {
            final File filePath = new File(volleyFileObjects.get(i).getFilePath());
            filesParams.put(volleyFileObjects.get(i).getParamName(), new VolleyMultipartRequest.DataPart(filePath.getName(), volleyFileObjects.get(i).getCompressObject().getBytes()));
        }
        Timber.e("%s", filesParams.size());
        volleyFileObjects.clear();
        return filesParams;
    }


}