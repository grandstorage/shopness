package grand.shopness.base;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.os.Build.VERSION_CODES.M;

/**
 * Created by MahmoudAyman on 6/12/2019.
 **/
public class PermissionUtily {

    private Context context;

    private PermissionUtily(Context context) {
        this.context = context;
    }

    public static PermissionUtily getInstance(Context context) {
        return new PermissionUtily(context);
    }

    @RequiresApi(api = M) // or higher
    public boolean isGranted(String permission) {
        return context.checkSelfPermission(permission) == PERMISSION_GRANTED;
    }

    @RequiresApi(api = M)
    public void requestPermission(Fragment forFragment, String[] permissionList, int requestCode) {
        //override onRequest in fragment
        forFragment.requestPermissions(permissionList,
                requestCode);
    }//end method

    @RequiresApi(api = M)
    public void requestPermission(String[] permissionList, int requestCode) {

        //override onRequest in activity
        ActivityCompat.requestPermissions((Activity) context, permissionList,
                requestCode);
    }//end method

//    public void setContext(Fragment contextFragment) {
//        this.contextFragment = contextFragment;
//    }
//    public void setContext(AppCompatActivity contextAppCompatActivity) {
//        this.contextAppCompatActivity = contextAppCompatActivity;
//    }
}
