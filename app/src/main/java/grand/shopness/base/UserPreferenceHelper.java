package grand.shopness.base;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import grand.shopness.application.MyApplication;
import grand.shopness.model.login.response.LoginData;
import grand.shopness.model.login.response.LoginResponse;
import grand.shopness.model.profile.ProfileItem;
import grand.shopness.model.profile.ProfileResponse;

public class UserPreferenceHelper {

    private static SharedPreferences sharedPreferences = null;
    private static Context context;
    //here you can find shared preference operations like get saved data for user


    private UserPreferenceHelper() {
        context = MyApplication.getInstance().getApplicationContext();
    }

    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = context.getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }

    public static int getUserId(Context context) {
        return getSharedPreferenceInstance(context).getInt("userId", -1);
    }

    public static void setUserId(Context context, int userId) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putInt("userId", userId);
        prefsEditor.apply();
    }


    public static void setRememberMe(Context context, boolean rememberMe) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putBoolean("rememberMe", rememberMe);
        prefsEditor.apply();
    }

    public static boolean isRemember(Context context) {
        return getSharedPreferenceInstance(context).getBoolean("rememberMe", false);
    }

    public static void saveUserDetails(Object userModel) {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.putString("userDetails", json);
        if (userModel instanceof LoginResponse) {
            prefsEditor.putInt("userId", ((LoginResponse) userModel).getData().getId());
        }
        prefsEditor.apply();
    }

    public static void clearUserDetails(Context context) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.clear();
        prefsEditor.apply();
    }

    public static LoginData getUserLoginDetails() {
        Context context = MyApplication.getInstance().getApplicationContext();
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance(context).getString("userDetails", "");
        if (json.equals("")) return new LoginData();
        return gson.fromJson(json, LoginData.class);
    }


    public static boolean isLogged() {
        return getUserLoginDetails() != null && getUserLoginDetails().getId() != 0;
    }


    public static void setLanguage(Context context, String language) {
        SharedPreferences userDetails = context.getSharedPreferences("languageData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString("language", language);
        editor.putBoolean("haveLanguage", true);
        editor.apply();
    }

    public static String getCurrentLanguage() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences sharedPreferences = context.getSharedPreferences("languageData", Context.MODE_PRIVATE);
        if (!sharedPreferences.getBoolean("haveLanguage", false)) return "en";
        return sharedPreferences.getString("language", "en");
    }

    public static boolean isFirstTime(Context context) {
        boolean isFirstTime = getSharedPreferenceInstance(context).getBoolean("firstTime", true);
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putBoolean("firstTime", false);
        prefsEditor.apply();

        return isFirstTime;
    }


    public static void saveGoogleToken(Context context, String token) {
        if (token == null || token.equals("")) {
            return;
        } else {
            SharedPreferences.Editor editor = getSharedPreferenceInstance(context).edit();
            editor.putString("googleToken", token);
            editor.apply();
        }
    }

    public static String getGoogleToken(Context context) {
        String token = getSharedPreferenceInstance(context).getString("googleToken", "") + "";
        return (token.length() < 1 || token == null) ? "EmptyToken" : token;
    }


    public static boolean getForgotPassScreen() {
        Context context = MyApplication.getInstance().getApplicationContext();
        return getSharedPreferenceInstance(context).getBoolean("forgot", false);
    }

    public static void setForgotPass(boolean isForgot) {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putBoolean("forgot", isForgot);
        prefsEditor.apply();
    }

    public static void copyPromoCode(String code) {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putString("promocode", code);
        prefsEditor.apply();
    }

    public static String getPromoCode() {
        return getSharedPreferenceInstance(context).getString("promocode", "");
    }

    public static void setServiceId(int id) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putInt("serviceId", id);
        prefsEditor.apply();
    }

    public static int getServiceId() {
        return getSharedPreferenceInstance(context).getInt("serviceId", 0);
    }

    public static void setAddressId(int addressId) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putInt("addressId", addressId);
        prefsEditor.apply();
    }

    public static int getAddressId() {
        return getSharedPreferenceInstance(context).getInt("addressId", 0);
    }

    public static void setPayIt(int id) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putInt("paymentId", id);
        prefsEditor.apply();
    }

    public static int getPayId() {
        return getSharedPreferenceInstance(context).getInt("paymentId", 0);
    }


    public static void setTotalAmount(float price) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putFloat("price", price);
        prefsEditor.apply();
    }

    public static float getTotalAmount() {
        return getSharedPreferenceInstance(context).getFloat("price", 0f);
    }

}
