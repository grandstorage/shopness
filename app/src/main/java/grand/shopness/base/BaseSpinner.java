package grand.shopness.base;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.fragment.app.FragmentActivity;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.lang.reflect.Field;
import java.net.ContentHandler;
import java.util.ArrayList;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.view.adapter.parent.ColorSpinnerAdapter;
import grand.shopness.view.adapter.parent.DefaultSpinnerAdapter;
import timber.log.Timber;

/**
 * Created by MahmoudAyman on 14/01/2019.
 */
public class BaseSpinner {

    private static void setHeight(Spinner spinner, int height){
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);

            // Set popupWindow height to 500px
            Objects.requireNonNull(popupWindow).setHeight(height);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
            Timber.e(e);
        }
    }

    public static Spinner setSpinner(Spinner spinner, ArrayList<String> stringArrayList, int resLayout) {
        setHeight(spinner, 500);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(MyApplication.getInstance().getApplicationContext(), stringArrayList, resLayout);
        spinner.setAdapter(adapter);
        return spinner;
    }

    public static MaterialSpinner setMaterialSpinner(MaterialSpinner spinner, ArrayList<String> stringArrayList) {
        spinner.setItems(stringArrayList);
        return spinner;
    }

    public static Spinner setColorSpinner(Spinner spinner, ArrayList<String> stringArrayList) {
        ColorSpinnerAdapter adapter = new ColorSpinnerAdapter(MyApplication.getInstance().getApplicationContext(), stringArrayList,
                R.layout.single_item_spinner_colors);
        spinner.setAdapter(adapter);
        return spinner;
    }

}
