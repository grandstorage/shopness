package grand.shopness.base;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;

public class BaseViewModel extends BaseObservable {
    private MutableLiveData<Object> mutableLiveData;
    private String message;

    public BaseViewModel() {
        mutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Object> getMutableLiveData() {
//        if (mutableLiveData == null)
//            mutableLiveData = new MutableLiveData<>();
        return mutableLiveData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void accessLoadingBar(int visible) {
        setValue(visible);
    }

    public void setValue(Object item) {
//        if (mutableLiveData == null)
//            mutableLiveData = new MutableLiveData<>();
        this.mutableLiveData.setValue(item);
    }


}
