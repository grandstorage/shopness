package grand.shopness.base.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.button.MaterialButton;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.DialogueUtily;
import grand.shopness.base.PermissionUtily;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.filesutils.FileOperations;
import grand.shopness.util.SettingsManager;
import grand.shopness.view.ui.activity.DetailsActivity;
import grand.shopness.view.ui.activity.MainActivity;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class BaseFragment extends Fragment {
    private FragmentActivity mContext;

    protected void showProgressBar(int show) {
        ProgressBar progressBar;
        if (mContext instanceof BaseActivity) {
            try {
                progressBar = ((BaseActivity) mContext).getProgressBar();
                progressBar.setVisibility(show);
            } catch (ClassCastException e) {
                e.getStackTrace();
            }
        } else if (mContext instanceof MainActivity) {
            try {
                progressBar = ((MainActivity) mContext).getProgressBar();
                progressBar.setVisibility(show);
            } catch (ClassCastException e) {
                e.getStackTrace();
            }
        } else if (mContext instanceof DetailsActivity) {
            try {
                progressBar = ((DetailsActivity) mContext).getProgressBar();
                progressBar.setVisibility(show);
            } catch (ClassCastException e) {
                e.getStackTrace();
            }
        }
    }

    protected void accessLoadAnimation(int resRaw, int show) {
        LottieAnimationView animationView;
        if (mContext instanceof MainActivity) {
            animationView = ((MainActivity) mContext).getAnimationView();
        } else {
            animationView = ((DetailsActivity) mContext).getAnimationView();
        }
        animationView.setAnimation(resRaw);
        animationView.setVisibility(show);
    }

    protected void showProgressAnimation(int show) {
        LottieAnimationView animationView;
        if (mContext instanceof MainActivity) {
            animationView = ((MainActivity) mContext).getAnimationView();
        } else {
            animationView = ((DetailsActivity) mContext).getAnimationView();
        }
        animationView.setAnimation(R.raw.loading_progress_anim);
        animationView.setVisibility(show);
    }

    public void showMessage(Object message) {
        if (mContext instanceof BaseActivity) {
            ((BaseActivity) mContext).showMessage(message);
        } else if (mContext instanceof MainActivity) {
            ((MainActivity) mContext).showMessage(message);
        } else {
            ((DetailsActivity) mContext).showMessage(message);
        }
    }

    private void showGPSAlertDialogue() {
        assert getFragmentManager() != null;
        DialogueUtily.getInstance().createCustomDialouge(this,
                getString(R.string.gps_disabled),
                "",
                getString(R.string.open_settings),
                getString(R.string.cancel),
                Codes.ALERT_DIALOGUE_GPS_REQ)
                .show(getFragmentManager(), "GPSAlertDialogue");
    }

    protected void showLocationPermissionAlertDialogue() {
        assert getFragmentManager() != null;
        DialogueUtily.getInstance().createCustomDialouge(this,
                getString(R.string.warning),
                getString(R.string.missing_perm_msg),
                getString(R.string.ok),
                getString(R.string.no),
                Codes.ALERT_DIALOGUE_LOCATION_REQ)
                .show(getFragmentManager(), "LocationAlertDialogue");
    }

    protected boolean isGPSEnabled() {
        if (SettingsManager.isGPSEnabled(Objects.requireNonNull(getContext()))) {
            return true;
        } else {
            showGPSAlertDialogue();
            return false;
        }
    }

    protected MaterialButton getFilterIcon(){
        return ((DetailsActivity) mContext).getFilterToolbarIcon();
    }

    protected MaterialButton getSearchIcon() {
        if (mContext instanceof MainActivity) {
            return ((MainActivity) mContext).getMainSearchBtn();
        } else {
            return ((DetailsActivity) mContext).getSearchToolbarIcon();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected boolean hasPermission() {
        if (!PermissionUtily.getInstance(getContext()).isGranted(ACCESS_FINE_LOCATION) &&
                !PermissionUtily.getInstance(getContext()).isGranted(ACCESS_COARSE_LOCATION)) {
            PermissionUtily.getInstance(getContext()).requestPermission(this,
                    new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION},
                    Codes.PERMISSION_LOCATION_REQUEST_CODE);
            return false;
        } else
            return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void pickImage(int reqCode) {
        if (!PermissionUtily.getInstance(getContext()).isGranted(READ_EXTERNAL_STORAGE) && !PermissionUtily.getInstance(getContext()).isGranted(READ_EXTERNAL_STORAGE)) {
            PermissionUtily.getInstance(getContext()).requestPermission(this,
                    new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE},
                    Codes.PERMISSION_IMAGE_REQUEST_CODE);
        } else {
            FileOperations.pickImage(getActivity(), reqCode);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mContext = null;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        try {
            mContext = (FragmentActivity) context;
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    @Nullable
    @Override
    public Context getContext() {
        return mContext;
    }
}
