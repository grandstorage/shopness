package grand.shopness.base.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import com.google.android.material.navigation.NavigationView;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.util.SettingsManager;
import grand.shopness.view.ui.activity.ChangeLanguageActivity;
import grand.shopness.view.ui.activity.MainActivity;
import grand.shopness.view.ui.fragment.main.sidemenu.AboutUsFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.BeAShopFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.BeDelegateFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.ChangeAreaFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.FaqFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.PrivacyTermsFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.PromoCodeFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.SuggestionFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.accounts.AccountsFragment;

public class BaseNavigationView extends NavigationView implements NavigationView.OnNavigationItemSelectedListener {
    private MainActivity mainActivity;
    private Context context = MyApplication.getInstance().getApplicationContext();

    public BaseNavigationView(Context context) {
        super(context);
        mainActivity = (MainActivity) context;
        setNavigationItemSelectedListener(this);
    }

    public BaseNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mainActivity = (MainActivity) context;
        setNavigationItemSelectedListener(this);
    }

    public BaseNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mainActivity = (MainActivity) context;
        setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setItemClicks(item.getItemId());
        mainActivity.getDrawerLayout().closeDrawer(GravityCompat.START);
        return false;
    }

    private void setItemClicks(int itemId) {
        switch (itemId) {
            case R.id.nav_accounts:
                if (UserPreferenceHelper.isLogged())
                    MovementManager.replaceFragment(getContext(), new AccountsFragment(), "AccountsFragment");
                else
                    Toast.makeText(mainActivity, "login first", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_change_area:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.change_area));
                MovementManager.replaceFragment(getContext(), new ChangeAreaFragment(), "ChangeAreaFragment");
                break;
            case R.id.nav_promotional_code:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.promotional_code));
                MovementManager.replaceFragment(getContext(), new PromoCodeFragment(), "PromoCodeFragment");
                break;
            case R.id.nav_be_delegate:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.be_a_delegate));
                MovementManager.replaceFragment(getContext(), new BeDelegateFragment(), "BeDelegateFragment");
                break;
            case R.id.nav_be_shop:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.be_a_shop));
                MovementManager.replaceFragment(getContext(), new BeAShopFragment(), "BeAShopFragment");
                break;
            case R.id.nav_language:
                MovementManager.startActivity(getContext(), ChangeLanguageActivity.class);
                break;
            case R.id.nav_privacy:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.privacy));
                MovementManager.replaceFragment(getContext(), new PrivacyTermsFragment(), "PrivacyTermsFragment");
                break;
            case R.id.nav_suggestions:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.suggestions));
                MovementManager.replaceFragment(getContext(), new SuggestionFragment(), "SuggestionFragment");
                break;
            case R.id.nav_faq:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.faq));
                MovementManager.replaceFragment(getContext(), new FaqFragment(), "");
                break;
            case R.id.nav_rate_app:
                SettingsManager.rateApp();
                break;
            case R.id.nav_share_app:
                SettingsManager.shareApp();
                break;
            case R.id.nav_about_us:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.about_us));
                MovementManager.replaceFragment(getContext(), new AboutUsFragment(), "");
                break;
            case R.id.nav_logout:
                UserPreferenceHelper.clearUserDetails(MyApplication.getInstance().getApplicationContext());
                MovementManager.startActivity(mainActivity, Codes.LOGIN_SCREEN);
                mainActivity.finish();
                break;
        }
//        mDrawer.closeDrawer(GravityCompat.START);
    }
}
