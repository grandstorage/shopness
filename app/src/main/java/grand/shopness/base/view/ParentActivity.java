package grand.shopness.base.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MyContextWrapper;
import grand.shopness.base.UserPreferenceHelper;

public class ParentActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayoutLanguage(UserPreferenceHelper.getCurrentLanguage());
    }

    public void showMessage(Object message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
    }

    public void setLayoutLanguage(String language) {
        if (language.equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, UserPreferenceHelper.getCurrentLanguage()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
            Objects.requireNonNull(fragment).onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.getStackTrace();
        }

    }


}
