package grand.shopness.base.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.base.MovementManager;
import grand.shopness.view.ui.activity.MainActivity;
import grand.shopness.view.ui.fragment.main.bottombar.CartFragment;
import grand.shopness.view.ui.fragment.main.bottombar.MainShopsFragment;
import grand.shopness.view.ui.fragment.main.bottombar.NotificationFragment;
import grand.shopness.view.ui.fragment.main.OrdersFragment;
import grand.shopness.view.ui.fragment.main.bottombar.OffersFragment;

public class BaseBottomNavigationView extends BottomNavigationView implements  BottomNavigationView.OnNavigationItemSelectedListener{
    private MainActivity mainActivity;
    private Context context = MyApplication.getInstance().getApplicationContext();
    public BaseBottomNavigationView(Context context) {
        super(context);
        mainActivity = (MainActivity) context;
        setOnNavigationItemSelectedListener(this);

    }

    public BaseBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mainActivity = (MainActivity) context;

        setOnNavigationItemSelectedListener(this);
    }

    public BaseBottomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mainActivity = (MainActivity) context;

        setOnNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        onItemListener(item.getItemId());
        item.setChecked(true);
        return false;
    }

    private void onItemListener(int id){
        switch (id) {
            //Bottom Navigation
            case R.id.bv_shops:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.app_name));
                MovementManager.replaceFragment(getContext(), new MainShopsFragment(), "MainShopsFragment");
                break;
            case R.id.bv_notification:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.notifications));
                MovementManager.replaceFragment(getContext(), new NotificationFragment(), "NotificationFragment");
                break;
            case R.id.bv_offers:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.offers));
                MovementManager.replaceFragment(getContext(), new OffersFragment(), "OffersFragment");
                break;
            case R.id.bv_cart:
                mainActivity.getToolBarTitle().setText(context.getString(R.string.cart));
                MovementManager.replaceFragment(getContext(), new CartFragment(), "CartFragment");
                break;
        }

    }

}
