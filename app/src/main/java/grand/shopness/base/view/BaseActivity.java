package grand.shopness.base.view;


import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.databinding.ActivityBaseBinding;
import grand.shopness.view.ui.fragment.login.ForgotPasswordFragment;
import grand.shopness.view.ui.fragment.login.LoginFragment;
import grand.shopness.view.ui.fragment.login.MapsFragment;
import grand.shopness.view.ui.fragment.login.NewPasswordFragment;
import grand.shopness.view.ui.fragment.login.RegisterFragment;
import grand.shopness.view.ui.fragment.login.SendCodeFragment;
import grand.shopness.view.ui.fragment.login.StartFragment;
import timber.log.Timber;


public class BaseActivity extends ParentActivity {
    public ActivityBaseBinding activityBaseBinding;
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        progressBar = activityBaseBinding.pbBaseLoadingBar;

        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE, 0));
        } else
            Timber.e("no fragment registered");
    }

    private void addFragment(int page) {
        Fragment fragment;
        if (page == Codes.REGISTER_SCREEN) {
            fragment = new RegisterFragment();
        } else if (page == Codes.ENTER_CODE_SCREEN) {
            fragment = new SendCodeFragment();
        } else if (page == Codes.START_SCREEN) {
            fragment = new StartFragment();
        } else if (page == Codes.MAP_SCREEN) {
            fragment = new MapsFragment();
        }else if (page == Codes.FORGOT_PASSWORD_SCREEN) {
            fragment = new ForgotPasswordFragment();
        }else if (page == Codes.CHANGE_PASSWORD_SCREEN) {
            fragment = new NewPasswordFragment();
        } else {
            fragment = new LoginFragment();
        }

        fragment.setArguments(getIntent().getBundleExtra(Params.BUNDLE_PAGE));
        MovementManager.replaceFragment(this, fragment, "BackStack");
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
            Objects.requireNonNull(fragment).onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.getStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
