package grand.shopness.base;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import java.util.Objects;

import timber.log.Timber;

public class MyClipBoardManager {

    private final Context context;

    private MyClipBoardManager(Context context) {
        this.context = context;
    }

    public static MyClipBoardManager getInstance(Context context) {
        return new MyClipBoardManager(context);
    }

    public void copyToClipboard(String text) {
        try {
            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("promo code", text);
            Objects.requireNonNull(clipboard).setPrimaryClip(clip);
        } catch (Exception e) {
            Timber.e(e);
        }
    }
}
