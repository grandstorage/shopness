package grand.shopness.model.sendcode;

import com.google.gson.annotations.SerializedName;

public class SendCodeRequest {
    @SerializedName("activate_code")
    private
    String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
