package grand.shopness.model.sendcode;

public class SendCodeResponse{
	private SendCodeResponseData data;
	private String message;
	private int status;

	public void setData(SendCodeResponseData data){
		this.data = data;
	}

	public SendCodeResponseData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SendCodeResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
