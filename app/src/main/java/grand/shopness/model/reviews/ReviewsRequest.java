package grand.shopness.model.reviews;

import com.google.gson.annotations.SerializedName;

public class ReviewsRequest {

    @SerializedName("shop_id")
    private int id;

    @SerializedName("comment")
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}