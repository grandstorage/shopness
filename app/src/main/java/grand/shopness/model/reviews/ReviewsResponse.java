package grand.shopness.model.reviews;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ReviewsResponse{

	@SerializedName("data")
	private List<ReviewItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<ReviewItem> data){
		this.data = data;
	}

	public List<ReviewItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ReviewsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}