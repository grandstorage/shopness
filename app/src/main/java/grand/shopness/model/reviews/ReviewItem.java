package grand.shopness.model.reviews;
import com.google.gson.annotations.SerializedName;

public class ReviewItem{

	@SerializedName("user_id")
	private int userId;

	@SerializedName("user_image")
	private String userImage;

	@SerializedName("name")
	private String name;

	@SerializedName("comment")
	private String comment;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"user_id = '" + userId + '\'' + 
			",user_image = '" + userImage + '\'' + 
			",name = '" + name + '\'' + 
			",comment = '" + comment + '\'' + 
			"}";
		}
}