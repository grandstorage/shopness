package grand.shopness.model.forgotpassword;

public class ForgotPasswordRequest {
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
