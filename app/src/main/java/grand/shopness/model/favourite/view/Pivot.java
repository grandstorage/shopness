package grand.shopness.model.favourite.view;

import com.google.gson.annotations.SerializedName;

public class Pivot{

	@SerializedName("product_id")
	private int productId;

	@SerializedName("size_id")
	private int sizeId;

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setSizeId(int sizeId){
		this.sizeId = sizeId;
	}

	public int getSizeId(){
		return sizeId;
	}

	@Override
 	public String toString(){
		return 
			"Pivot{" + 
			"product_id = '" + productId + '\'' + 
			",size_id = '" + sizeId + '\'' + 
			"}";
		}
}