package grand.shopness.model.favourite.change;

import com.google.gson.annotations.SerializedName;

public class ChangeFavResponse{

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	@SerializedName("isFavorite")
	private int isFavorite;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setIsFavorite(int isFavorite){
		this.isFavorite = isFavorite;
	}

	public int getIsFavorite(){
		return isFavorite;
	}

	@Override
 	public String toString(){
		return 
			"ChangeFavResponse{" + 
			"message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			",isFavorite = '" + isFavorite + '\'' + 
			"}";
		}
}