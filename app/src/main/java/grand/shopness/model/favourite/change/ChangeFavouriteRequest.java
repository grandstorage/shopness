package grand.shopness.model.favourite.change;

import com.google.gson.annotations.SerializedName;

public class ChangeFavouriteRequest {
    @SerializedName("product_id")
    private int productId;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
