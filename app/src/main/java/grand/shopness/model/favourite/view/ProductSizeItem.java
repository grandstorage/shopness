package grand.shopness.model.favourite.view;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductSizeItem{

	@SerializedName("is_clothes")
	private int isClothes;

	@SerializedName("product_name_ar")
	private String productNameAr;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("product_desc_en")
	private String productDescEn;

	@SerializedName("product_color")
	private Object productColor;

	@SerializedName("product_code")
	private Object productCode;

	@SerializedName("product_desc_ar")
	private String productDescAr;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("sizes")
	private List<SizesItem> sizes;

	@SerializedName("price")
	private int price;

	@SerializedName("product_name_en")
	private String productNameEn;

	@SerializedName("id")
	private int id;

	@SerializedName("product_stock")
	private int productStock;

	@SerializedName("status")
	private int status;

	public void setIsClothes(int isClothes){
		this.isClothes = isClothes;
	}

	public int getIsClothes(){
		return isClothes;
	}

	public void setProductNameAr(String productNameAr){
		this.productNameAr = productNameAr;
	}

	public String getProductNameAr(){
		return productNameAr;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setProductDescEn(String productDescEn){
		this.productDescEn = productDescEn;
	}

	public String getProductDescEn(){
		return productDescEn;
	}

	public void setProductColor(Object productColor){
		this.productColor = productColor;
	}

	public Object getProductColor(){
		return productColor;
	}

	public void setProductCode(Object productCode){
		this.productCode = productCode;
	}

	public Object getProductCode(){
		return productCode;
	}

	public void setProductDescAr(String productDescAr){
		this.productDescAr = productDescAr;
	}

	public String getProductDescAr(){
		return productDescAr;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setSizes(List<SizesItem> sizes){
		this.sizes = sizes;
	}

	public List<SizesItem> getSizes(){
		return sizes;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setProductNameEn(String productNameEn){
		this.productNameEn = productNameEn;
	}

	public String getProductNameEn(){
		return productNameEn;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductStock(int productStock){
		this.productStock = productStock;
	}

	public int getProductStock(){
		return productStock;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductSizeItem{" + 
			"is_clothes = '" + isClothes + '\'' + 
			",product_name_ar = '" + productNameAr + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",product_desc_en = '" + productDescEn + '\'' + 
			",product_color = '" + productColor + '\'' + 
			",product_code = '" + productCode + '\'' + 
			",product_desc_ar = '" + productDescAr + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",sizes = '" + sizes + '\'' + 
			",price = '" + price + '\'' + 
			",product_name_en = '" + productNameEn + '\'' + 
			",id = '" + id + '\'' + 
			",product_stock = '" + productStock + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}