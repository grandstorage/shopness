package grand.shopness.model.favourite.view;

import com.google.gson.annotations.SerializedName;

public class SizesItem{

	@SerializedName("size_name")
	private String sizeName;

	@SerializedName("pivot")
	private Pivot pivot;

	@SerializedName("id")
	private int id;

	public void setSizeName(String sizeName){
		this.sizeName = sizeName;
	}

	public String getSizeName(){
		return sizeName;
	}

	public void setPivot(Pivot pivot){
		this.pivot = pivot;
	}

	public Pivot getPivot(){
		return pivot;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SizesItem{" + 
			"size_name = '" + sizeName + '\'' + 
			",pivot = '" + pivot + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}