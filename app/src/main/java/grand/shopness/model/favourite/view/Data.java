package grand.shopness.model.favourite.view;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("data")
	private List<WhishlistItem> data;

	public void setData(List<WhishlistItem> data){
		this.data = data;
	}

	public List<WhishlistItem> getWhishlistItems(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"data = '" + data + '\'' + 
			"}";
		}
}