package grand.shopness.model.favourite.view;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WhishlistItem{

	@SerializedName("is_clothes")
	private int isClothes;

	@SerializedName("price")
	private int price;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("product_desc_en")
	private String productDescEn;

	@SerializedName("product_name_en")
	private String productNameEn;

	@SerializedName("id")
	private int id;

	@SerializedName("product_stock")
	private String productStock;

	@SerializedName("discounted_price")
	private int discountedPrice;

	@SerializedName("product_size")
	private List<ProductSizeItem> productSize;

	public void setIsClothes(int isClothes){
		this.isClothes = isClothes;
	}

	public int getIsClothes(){
		return isClothes;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setProductDescEn(String productDescEn){
		this.productDescEn = productDescEn;
	}

	public String getProductDescEn(){
		return productDescEn;
	}

	public void setProductNameEn(String productNameEn){
		this.productNameEn = productNameEn;
	}

	public String getProductNameEn(){
		return productNameEn;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductStock(String productStock){
		this.productStock = productStock;
	}

	public String getProductStock(){
		return productStock;
	}

	public void setDiscountedPrice(int discountedPrice){
		this.discountedPrice = discountedPrice;
	}

	public int getDiscountedPrice(){
		return discountedPrice;
	}

	public void setProductSize(List<ProductSizeItem> productSize){
		this.productSize = productSize;
	}

	public List<ProductSizeItem> getProductSize(){
		return productSize;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"is_clothes = '" + isClothes + '\'' + 
			",price = '" + price + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",product_desc_en = '" + productDescEn + '\'' + 
			",product_name_en = '" + productNameEn + '\'' + 
			",id = '" + id + '\'' + 
			",product_stock = '" + productStock + '\'' + 
			",discounted_price = '" + discountedPrice + '\'' + 
			",product_size = '" + productSize + '\'' + 
			"}";
		}
}