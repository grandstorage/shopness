package grand.shopness.model.myorders;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MyOrdersResponse{

	@SerializedName("data")
	private List<MyOrderItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public List<MyOrderItem> getData(){
		return data;
	}

	public void setData(List<MyOrderItem> data){
		this.data = data;
	}

	public String getMessage(){
		return message;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public int getStatus(){
		return status;
	}

	public void setStatus(int status){
		this.status = status;
	}

	@Override
 	public String toString(){
		return 
			"MyOrdersResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}