package grand.shopness.model.myorders;

import com.google.gson.annotations.SerializedName;

public class MyOrderItem{

	@SerializedName("order_status")
	private OrderStatus orderStatus;

	@SerializedName("order_date")
	private String orderDate;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("id")
	private int id;

	public OrderStatus getOrderStatus(){
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus){
		this.orderStatus = orderStatus;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"order_status = '" + orderStatus + '\'' + 
			",order_date = '" + orderDate + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}