package grand.shopness.model.profile;

import com.google.gson.annotations.SerializedName;

public class ProfileResponse{

	@SerializedName("data")
	private ProfileItem data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(ProfileItem data){
		this.data = data;
	}

	public ProfileItem getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProfileResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}