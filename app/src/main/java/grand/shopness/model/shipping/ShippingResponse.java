package grand.shopness.model.shipping;

import com.google.gson.annotations.SerializedName;

public class ShippingResponse{

	@SerializedName("address_id")
	private int addressId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setAddressId(int addressId){
		this.addressId = addressId;
	}

	public int getAddressId(){
		return addressId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ShippingResponse{" + 
			"address_id = '" + addressId + '\'' + 
			",message = '" + message + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}
}