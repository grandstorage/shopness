package grand.shopness.model.faq;

import java.util.List;


public class FaqResponse{
	private List<FaqData> data;
	private String message;
	private int status;

	public void setData(List<FaqData> data){
		this.data = data;
	}

	public List<FaqData> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"FaqResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}