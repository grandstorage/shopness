package grand.shopness.model.startfrag;

import java.util.ArrayList;
import grand.shopness.R;

public class StartScreenModel {
    private String welcomeText, shopness, bio;

    public StartScreenModel() {
    }


    public ArrayList<Integer> getImagesData() {
        ArrayList<Integer> imagesData = new ArrayList<>();
        imagesData.add(R.drawable.app_logo);
        imagesData.add(R.drawable.app_logo);
        imagesData.add(R.drawable.app_logo);
        return imagesData;
    }

    public String getWelcomeText() {
        return welcomeText;
    }

    public void setWelcomeText(String welcomeText) {
        this.welcomeText = welcomeText;
    }

    public String getShopness() {
        return shopness;
    }

    public void setShopness(String shopness) {
        this.shopness = shopness;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
