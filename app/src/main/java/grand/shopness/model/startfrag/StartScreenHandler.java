package grand.shopness.model.startfrag;


import android.content.Context;
import android.view.View;

import grand.shopness.base.MovementManager;
import grand.shopness.view.ui.fragment.login.LoginFragment;

public class StartScreenHandler {
    private Context context;
    public StartScreenHandler(Context context) {
        this.context = context;
    }

    public void onLoginClick(View button){
        MovementManager.replaceFragment(context, new LoginFragment(), "LoginFragment");
    }

    public void onShopsClick(View button){

    }
}
