package grand.shopness.model.cart.view;

import com.google.gson.annotations.SerializedName;

public class Size{

	@SerializedName("size_name")
	private String sizeName;

	public void setSizeName(String sizeName){
		this.sizeName = sizeName;
	}

	public String getSizeName(){
		return sizeName;
	}

	@Override
 	public String toString(){
		return 
			"Size{" + 
			"size_name = '" + sizeName + '\'' + 
			"}";
		}
}