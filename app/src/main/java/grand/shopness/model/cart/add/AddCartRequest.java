package grand.shopness.model.cart.add;

import com.google.gson.annotations.SerializedName;

public class AddCartRequest {
    @SerializedName("product_id")
    private int productId;

    @SerializedName("qty")
    private int qty;

    @SerializedName("is_clothes")
    private int isClothes;

    @SerializedName("product_color")
    private int productColor;

    @SerializedName("size")
    private int size;

    @SerializedName("price")
    private float price;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getIsClothes() {
        return isClothes;
    }

    public void setIsClothes(int isClothes) {
        this.isClothes = isClothes;
    }

    public int getProductColor() {
        return productColor;
    }

    public void setProductColor(int productColor) {
        this.productColor = productColor;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
