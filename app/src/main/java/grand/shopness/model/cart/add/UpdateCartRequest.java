package grand.shopness.model.cart.add;

import com.google.gson.annotations.SerializedName;

public class UpdateCartRequest {

    @SerializedName("cart_item_id")
    private int cartItemId;

    @SerializedName("plus")
    private int plus;

    public int getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(int cartItemId) {
        this.cartItemId = cartItemId;
    }

    public int getPlus() {
        return plus;
    }

    public void setPlus(int plus) {
        this.plus = plus;
    }
}
