package grand.shopness.model.cart.view;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("total_price")
	private int price;

	@SerializedName("cart")
	private List<CartItem> cart;

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setCart(List<CartItem> cart){
		this.cart = cart;
	}

	public List<CartItem> getCart(){
		return cart;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"price = '" + price + '\'' + 
			",cart = '" + cart + '\'' + 
			"}";
		}
}