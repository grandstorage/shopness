package grand.shopness.model.cart.view;

import com.google.gson.annotations.SerializedName;

public class CartItem{

	@SerializedName("size")
	private Size size;

	@SerializedName("total_price")
	private int totalPrice;

	@SerializedName("price")
	private int price;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("qty")
	private int qty;

	@SerializedName("product_color")
	private String productColor;

	@SerializedName("cart_item_id")
	private int cartItemId;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("subtotal_fees")
	private int subtotalFees;

	public void setSize(Size size){
		this.size = size;
	}

	public Size getSize(){
		return size;
	}

	public void setTotalPrice(int totalPrice){
		this.totalPrice = totalPrice;
	}

	public int getTotalPrice(){
		return totalPrice;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setQty(int qty){
		this.qty = qty;
	}

	public int getQty(){
		return qty;
	}

	public void setProductColor(String productColor){
		this.productColor = productColor;
	}

	public String getProductColor(){
		return productColor;
	}

	public void setCartItemId(int cartItemId){
		this.cartItemId = cartItemId;
	}

	public int getCartItemId(){
		return cartItemId;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setSubtotalFees(int subtotalFees){
		this.subtotalFees = subtotalFees;
	}

	public int getSubtotalFees(){
		return subtotalFees;
	}

	@Override
 	public String toString(){
		return 
			"CartItem{" + 
			"size = '" + size + '\'' + 
			",total_price = '" + totalPrice + '\'' + 
			",price = '" + price + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",qty = '" + qty + '\'' + 
			",product_color = '" + productColor + '\'' + 
			",cart_item_id = '" + cartItemId + '\'' + 
			",product_name = '" + productName + '\'' + 
			",subtotal_fees = '" + subtotalFees + '\'' + 
			"}";
		}
}