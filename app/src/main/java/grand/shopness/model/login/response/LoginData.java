
package grand.shopness.model.login.response;

import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private int mId;
    @SerializedName("jwt_token")
    private String mJwtToken;
    @SerializedName("lat")
    private String mLat;
    @SerializedName("lng")
    private String mLng;
    @SerializedName("location")
    private String mLocation;
    @SerializedName("name")
    private String mName;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("user_image")
    private String mUserImage;
    @SerializedName("user_status")
    private int mUserStatus;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getJwtToken() {
        return mJwtToken;
    }

    public void setJwtToken(String jwtToken) {
        mJwtToken = jwtToken;
    }

    public String getLat() {
        return mLat;
    }

    public void setLat(String lat) {
        mLat = lat;
    }

    public String getLng() {
        return mLng;
    }

    public void setLng(String lng) {
        mLng = lng;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getUserImage() {
        return mUserImage;
    }

    public void setUserImage(String userImage) {
        mUserImage = userImage;
    }

    public int getUserStatus() {
        return mUserStatus;
    }

    public void setUserStatus(int userStatus) {
        mUserStatus = userStatus;
    }

}
