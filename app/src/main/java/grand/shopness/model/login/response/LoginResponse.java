
package grand.shopness.model.login.response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("data")
    private LoginData mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private int mStatus;

    public LoginData getData() {
        return mData;
    }

    public void setData(LoginData data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

}
