package grand.shopness.model.suggestion;

import com.google.gson.annotations.SerializedName;

public class SuggestionRequest {
    @SerializedName("comment")
    String title;
    @SerializedName("title")
    String subject;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
