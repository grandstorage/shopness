package grand.shopness.model.offers;

import com.google.gson.annotations.SerializedName;

public class OfferItem{

	@SerializedName("shop_id")
	private int shopId;

	@SerializedName("distance")
	private int distance;

	@SerializedName("id")
	private int id;

	@SerializedName("offer_image")
	private String offerImage;

	@SerializedName("shop_name_ar")
	private String shopNameAr;

	public void setShopId(int shopId){
		this.shopId = shopId;
	}

	public int getShopId(){
		return shopId;
	}

	public void setDistance(int distance){
		this.distance = distance;
	}

	public int getDistance(){
		return distance;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setOfferImage(String offerImage){
		this.offerImage = offerImage;
	}

	public String getOfferImage(){
		return offerImage;
	}

	public void setShopNameAr(String shopNameAr){
		this.shopNameAr = shopNameAr;
	}

	public String getShopNameAr(){
		return shopNameAr;
	}

	@Override
 	public String toString(){
		return 
			"AllOfferShopsItem{" + 
			"shop_id = '" + shopId + '\'' + 
			",distance = '" + distance + '\'' + 
			",id = '" + id + '\'' + 
			",offer_image = '" + offerImage + '\'' + 
			",shop_name_ar = '" + shopNameAr + '\'' + 
			"}";
		}
}