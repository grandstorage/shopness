package grand.shopness.model.offers;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OfferData{

	@SerializedName("AllOfferShops")
	private List<OfferItem> allOfferShops;

	public void setAllOfferShops(List<OfferItem> allOfferShops){
		this.allOfferShops = allOfferShops;
	}

	public List<OfferItem> getAllOfferShops(){
		return allOfferShops;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"allOfferShops = '" + allOfferShops + '\'' + 
			"}";
		}
}