package grand.shopness.model.newpassword;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class NewPasswordRequest {
    @SerializedName("new_password")
    private String newPassword;
    private String confirmPassword;
    @SerializedName("old_password")
    private String oldPassword;

    public boolean isEqual(){
        return TextUtils.equals(newPassword,confirmPassword);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }


    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
