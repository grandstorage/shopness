package grand.shopness.model.payment;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

import grand.shopness.R;
import grand.shopness.application.MyApplication;

public class PaymentItem {
    private int id;
    private Drawable ImageRes;
    private String name;
    private boolean isChecked;
    private Context context = MyApplication.getInstance().getApplicationContext();

    public PaymentItem() {
    }

    public PaymentItem(int id, Drawable imageRes, String name, boolean isChecked) {
        this.id = id;
        ImageRes = imageRes;
        this.name = name;
        this.isChecked = isChecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Drawable getImageRes() {
        return ImageRes;
    }

    public void setImageRes(Drawable imageRes) {
        ImageRes = imageRes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public ArrayList<PaymentItem> getPaymentItems() {
        ArrayList<PaymentItem> paymentItems = new ArrayList<>();
        paymentItems.add(new PaymentItem(1,
                context.getResources().getDrawable(R.drawable.ic_cash),
                context.getResources().getString(R.string.cash_on_delivery),
                false)
        );

        paymentItems.add(new PaymentItem(2,
                context.getResources().getDrawable(R.drawable.ic_e_wallet),
                context.getResources().getString(R.string.e_wallet),
                false)
        );

        paymentItems.add(new PaymentItem(3,
                context.getResources().getDrawable(R.drawable.ic_credit_card),
                context.getResources().getString(R.string.credit_card),
                false)
        );
        return paymentItems;
    }

}
