package grand.shopness.model.product.size;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SizeItem {

    @SerializedName("size_name")
    private String sizeName;

    @SerializedName("id")
    private int id;

    @Expose
    private boolean isFound;

    public boolean isFound() {
        return isFound;
    }

    public void setFound(boolean found) {
        isFound = found;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "size_name = '" + sizeName + '\'' +
                        ",id = '" + id + '\'' +
                        ",isFound = '" + isFound + '\'' +
                        "}";
    }
}