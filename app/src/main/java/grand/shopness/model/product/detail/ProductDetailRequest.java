package grand.shopness.model.product.detail;

import com.google.gson.annotations.SerializedName;

public class ProductDetailRequest {
    @SerializedName("product_id")
    private int productId;

    @SerializedName("status")
    private int isClothes;

    public ProductDetailRequest(int productId, int isClothes) {
        this.productId = productId;
        this.isClothes = isClothes;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getIsClothes() {
        return isClothes;
    }

    public void setIsClothes(int isClothes) {
        this.isClothes = isClothes;
    }
}
