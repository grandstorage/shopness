package grand.shopness.model.product.detail;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductDetailResponse{

	@SerializedName("data")
	private List<ProductDetailItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<ProductDetailItem> data){
		this.data = data;
	}

	public List<ProductDetailItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductDetailResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}