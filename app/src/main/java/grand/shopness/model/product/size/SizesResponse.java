package grand.shopness.model.product.size;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.shopness.model.product.size.SizeItem;

public class SizesResponse {

	@SerializedName("data")
	private List<SizeItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<SizeItem> data){
		this.data = data;
	}

	public List<SizeItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductDetailResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}