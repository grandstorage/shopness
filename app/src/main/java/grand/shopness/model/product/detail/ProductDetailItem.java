package grand.shopness.model.product.detail;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductDetailItem{

	@SerializedName("user_favorite")
	private int userFavorite;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("description")
	private String description;

	@SerializedName("product_size")
	private List<ProductSizeItem> productSize;

	@SerializedName("id")
	private int id;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("price")
	private int price;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setUserFavorite(int userFavorite){
		this.userFavorite = userFavorite;
	}

	public int getUserFavorite(){
		return userFavorite;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setProductSize(List<ProductSizeItem> productSize){
		this.productSize = productSize;
	}

	public List<ProductSizeItem> getProductSize(){
		return productSize;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@Override
	public String toString(){
		return
				"ProductDetailItem{" +
						"user_favorite = '" + userFavorite + '\'' +
						",product_image = '" + productImage + '\'' +
						",description = '" + description + '\'' +
						",product_size = '" + productSize + '\'' +
						",id = '" + id + '\'' +
						",product_name = '" + productName + '\'' +
						"}";
	}
}