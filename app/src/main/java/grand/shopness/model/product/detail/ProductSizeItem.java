package grand.shopness.model.product.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductSizeItem{


	@SerializedName("price")
	private int price;

	@SerializedName("product_id")
	private int productId;

	@SerializedName("size_id")
	private int sizeId;

	@SerializedName("id")
	private int id;

	@SerializedName("stock")
	private int stock;

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setSizeId(int sizeId){
		this.sizeId = sizeId;
	}

	public int getSizeId(){
		return sizeId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStock(int stock){
		this.stock = stock;
	}

	public int getStock(){
		return stock;
	}

	@Override
 	public String toString(){
		return 
			"ProductSizeItem{" + 
			"price = '" + price + '\'' + 
			",product_id = '" + productId + '\'' + 
			",size_id = '" + sizeId + '\'' + 
			",id = '" + id + '\'' + 
			",stock = '" + stock + '\'' + 
			"}";
		}
}