package grand.shopness.model.product.color;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColorsRequest {
    @SerializedName("size_id")
    private int sizeId;
    @SerializedName("product_id")
    private int productId;

    @Expose
    private boolean isLoading;

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
