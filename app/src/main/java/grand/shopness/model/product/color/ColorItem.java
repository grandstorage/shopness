package grand.shopness.model.product.color;

import com.google.gson.annotations.SerializedName;

public class ColorItem {

    @SerializedName("id")
    private int id;

    @SerializedName("color_name")
    private String colorName;

    @SerializedName("color")
    private String color;


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorName() {
        return colorName;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "id = '" + id + '\'' +
                        ",color_name = '" + colorName + '\'' +
                        ",color = '" + color + '\'' +
                        "}";
    }
}