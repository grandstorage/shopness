
package grand.shopness.model.register.response;

 import com.google.gson.annotations.SerializedName;

 public class RegisterResponse {

    @SerializedName("data")
    private RegisterData mRegisterData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private int mStatus;

    public RegisterData getData() {
        return mRegisterData;
    }

    public void setData(RegisterData registerData) {
        mRegisterData = registerData;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

}
