
package grand.shopness.model.register.response;

 import com.google.gson.annotations.SerializedName;

 public class RegisterData {

    @SerializedName("jwt_token")
    private String mJwtToken;
    @SerializedName("user_id")
    private int mUserId;

    public String getJwtToken() {
        return mJwtToken;
    }

    public void setJwtToken(String jwtToken) {
        mJwtToken = jwtToken;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }


 }
