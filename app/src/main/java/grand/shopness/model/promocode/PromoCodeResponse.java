package grand.shopness.model.promocode;

public class PromoCodeResponse{
	private PromoCodeData data;
	private String message;
	private int status;

	public void setData(PromoCodeData data){
		this.data = data;
	}

	public PromoCodeData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PromoCodeResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
