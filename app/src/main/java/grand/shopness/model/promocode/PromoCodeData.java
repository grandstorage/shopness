package grand.shopness.model.promocode;

public class PromoCodeData{
	private String code;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"code = '" + code + '\'' + 
			"}";
		}
}
