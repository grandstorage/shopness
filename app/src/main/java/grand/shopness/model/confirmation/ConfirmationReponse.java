package grand.shopness.model.confirmation;

import com.google.gson.annotations.Expose;

/**
 * Created by MahmoudAyman on 7/28/2019.
 **/
public class ConfirmationReponse {

    @Expose
    private String name;
    @Expose
    private String address;
    @Expose
    private float orderTotal;
    @Expose
    private String delivery;
    @Expose
    private float totalAmount;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(float orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }
}
