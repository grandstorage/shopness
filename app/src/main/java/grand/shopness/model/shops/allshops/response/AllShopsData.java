package grand.shopness.model.shops.allshops.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AllShopsData {

	@SerializedName("AllShops")
	private List<AllShopsItem> allShops;

	@SerializedName("Shipping")
	private String shipping;

	@SerializedName("NearestShops")
	private List<NearestShopsItem> nearestShops;

	public void setAllShops(List<AllShopsItem> allShops){
		this.allShops = allShops;
	}

	public List<AllShopsItem> getAllShops(){
		return allShops;
	}

	public void setShipping(String shipping){
		this.shipping = shipping;
	}

	public String getShipping(){
		return shipping;
	}

	public void setNearestShops(List<NearestShopsItem> nearestShops){
		this.nearestShops = nearestShops;
	}

	public List<NearestShopsItem> getNearestShops(){
		return nearestShops;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"allShops = '" + allShops + '\'' + 
			",shipping = '" + shipping + '\'' + 
			",nearestShops = '" + nearestShops + '\'' + 
			"}";
		}
}