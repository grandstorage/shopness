package grand.shopness.model.shops.mainshops.response;

import com.google.gson.annotations.SerializedName;

public class ServiceItem{

	@SerializedName("service_name")
	private String serviceName;

	@SerializedName("service_image")
	private String serviceImage;

	@SerializedName("id")
	private int id;

	public void setServiceName(String serviceName){
		this.serviceName = serviceName;
	}

	public String getServiceName(){
		return serviceName;
	}

	public void setServiceImage(String serviceImage){
		this.serviceImage = serviceImage;
	}

	public String getServiceImage(){
		return serviceImage;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ServiceItem{" + 
			"service_name = '" + serviceName + '\'' + 
			",service_image = '" + serviceImage + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}