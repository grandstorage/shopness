package grand.shopness.model.shops.details;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ShopDetailsResponse{

	@SerializedName("data")
	private List<ShopDetailsItem> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<ShopDetailsItem> data){
		this.data = data;
	}

	public List<ShopDetailsItem> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ShopDetailsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}