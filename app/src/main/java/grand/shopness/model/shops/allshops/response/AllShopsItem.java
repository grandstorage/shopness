package grand.shopness.model.shops.allshops.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class AllShopsItem implements Parcelable {

	@SerializedName("distance")
	private int distance;

	@SerializedName("mini_charge")
	private String miniCharge;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("shop_image")
	private String shopImage;

	@SerializedName("rate_shop")
	private float rateShop;

	private AllShopsItem(Parcel in) {
		distance = in.readInt();
		miniCharge = in.readString();
		description = in.readString();
		id = in.readInt();
		shopName = in.readString();
		shopImage = in.readString();
		rateShop = in.readInt();
	}

	public static final Creator<AllShopsItem> CREATOR = new Creator<AllShopsItem>() {
		@Override
		public AllShopsItem createFromParcel(Parcel in) {
			return new AllShopsItem(in);
		}

		@Override
		public AllShopsItem[] newArray(int size) {
			return new AllShopsItem[size];
		}
	};

	public void setDistance(int distance){
		this.distance = distance;
	}

	public int getDistance(){
		return distance;
	}

	public void setMiniCharge(String miniCharge){
		this.miniCharge = miniCharge;
	}

	public String getMiniCharge(){
		return miniCharge;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setShopName(String shopName){
		this.shopName = shopName;
	}

	public String getShopName(){
		return shopName;
	}

	public void setShopImage(String shopImage){
		this.shopImage = shopImage;
	}

	public String getShopImage(){
		return shopImage;
	}

	public float getRateShop(){
		return rateShop;
	}

	public void setRateShop(float rateShop){
		this.rateShop = rateShop;
	}

	@Override
 	public String toString(){
		return 
			"AllShopsItem{" + 
			"distance = '" + distance + '\'' + 
			",mini_charge = '" + miniCharge + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",shop_name = '" + shopName + '\'' + 
			",shop_image = '" + shopImage + '\'' + 
			",rate_shop = '" + rateShop + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(distance);
		parcel.writeString(miniCharge);
		parcel.writeString(description);
		parcel.writeInt(id);
		parcel.writeString(shopName);
		parcel.writeString(shopImage);
		parcel.writeFloat(rateShop);
	}
}