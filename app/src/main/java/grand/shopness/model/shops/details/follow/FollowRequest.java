package grand.shopness.model.shops.details.follow;

import com.google.gson.annotations.SerializedName;

public class FollowRequest {
    @SerializedName("shop_id")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
