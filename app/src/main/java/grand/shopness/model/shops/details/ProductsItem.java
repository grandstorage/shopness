package grand.shopness.model.shops.details;

import com.google.gson.annotations.SerializedName;

public class ProductsItem{

	@SerializedName("product_desc")
	private String productDesc;

	@SerializedName("user_favorite")
	private int userFavorite;

	@SerializedName("product_image")
	private String productImage;

	@SerializedName("price")
	private int price;

	@SerializedName("id")
	private int id;

	@SerializedName("product_name")
	private String productName;

	@SerializedName("is_clothes")
	private int isClothes;

	public int getIsClothes() {
		return isClothes;
	}

	public void setIsClothes(int isClothes) {
		this.isClothes = isClothes;
	}

	public void setProductDesc(String productDesc){
		this.productDesc = productDesc;
	}

	public String getProductDesc(){
		return productDesc;
	}

	public void setUserFavorite(int userFavorite){
		this.userFavorite = userFavorite;
	}

	public int getUserFavorite(){
		return userFavorite;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@Override
 	public String toString(){
		return 
			"ProductsItem{" + 
			"product_desc = '" + productDesc + '\'' + 
			",user_favorite = '" + userFavorite + '\'' + 
			",product_image = '" + productImage + '\'' + 
			",price = '" + price + '\'' + 
			",id = '" + id + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}";
		}
}