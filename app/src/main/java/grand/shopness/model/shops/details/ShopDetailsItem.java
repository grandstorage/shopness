package grand.shopness.model.shops.details;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ShopDetailsItem{

	@SerializedName("isFollow")
	private int isFollow;

	@SerializedName("rate")
	private float rate;

	@SerializedName("OfFollowing")
	private int ofFollowing;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("shop_name")
	private String shopName;

	@SerializedName("shop_image")
	private String shopImage;

	@SerializedName("category")
	private List<CategoryItem> category;

	@SerializedName("products")
	private List<ProductsItem> products;

	public void setIsFollow(int isFollow){
		this.isFollow = isFollow;
	}

	public int getIsFollow(){
		return isFollow;
	}

	public void setRate(int rate){
		this.rate = rate;
	}

	public float getRate(){
		return rate;
	}

	public void setOfFollowing(int ofFollowing){
		this.ofFollowing = ofFollowing;
	}

	public int getOfFollowing(){
		return ofFollowing;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setShopName(String shopName){
		this.shopName = shopName;
	}

	public String getShopName(){
		return shopName;
	}

	public void setShopImage(String shopImage){
		this.shopImage = shopImage;
	}

	public String getShopImage(){
		return shopImage;
	}

	public void setCategory(List<CategoryItem> category){
		this.category = category;
	}

	public List<CategoryItem> getCategory(){
		return category;
	}

	public void setProducts(List<ProductsItem> products){
		this.products = products;
	}

	public List<ProductsItem> getProducts(){
		return products;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"isFollow = '" + isFollow + '\'' + 
			",rate = '" + rate + '\'' + 
			",ofFollowing = '" + ofFollowing + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			",shop_name = '" + shopName + '\'' + 
			",shop_image = '" + shopImage + '\'' + 
			",category = '" + category + '\'' + 
			",products = '" + products + '\'' + 
			"}";
		}
}