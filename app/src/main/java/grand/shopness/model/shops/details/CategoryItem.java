package grand.shopness.model.shops.details;

import com.google.gson.annotations.SerializedName;

public class CategoryItem{

	@SerializedName("Category_name")
	private String categoryName;

	@SerializedName("pivot")
	private Pivot pivot;

	@SerializedName("id")
	private int id;

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setPivot(Pivot pivot){
		this.pivot = pivot;
	}

	public Pivot getPivot(){
		return pivot;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"CategoryItem{" + 
			"category_name = '" + categoryName + '\'' + 
			",pivot = '" + pivot + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}