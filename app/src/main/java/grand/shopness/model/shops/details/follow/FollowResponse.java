package grand.shopness.model.shops.details.follow;

import com.google.gson.annotations.SerializedName;

public class FollowResponse{

	@SerializedName("isFollow")
	private int isFollow;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setIsFollow(int isFollow){
		this.isFollow = isFollow;
	}

	public int getIsFollow(){
		return isFollow;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"FollowResponse{" + 
			"isFollow = '" + isFollow + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}