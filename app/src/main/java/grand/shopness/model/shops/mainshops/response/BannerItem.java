package grand.shopness.model.shops.mainshops.response;

import com.google.gson.annotations.SerializedName;

public class BannerItem{

	@SerializedName("banner_image")
	private String bannerImage;

	public void setBannerImage(String bannerImage){
		this.bannerImage = bannerImage;
	}

	public String getBannerImage(){
		return bannerImage;
	}

	@Override
 	public String toString(){
		return 
			"BannerItem{" + 
			"banner_image = '" + bannerImage + '\'' + 
			"}";
		}
}