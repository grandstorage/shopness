package grand.shopness.model.shops.allshops.response;

import com.google.gson.annotations.SerializedName;

public class AllShopsResponse{

	@SerializedName("data")
	private AllShopsData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(AllShopsData data){
		this.data = data;
	}

	public AllShopsData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AllShopsResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}