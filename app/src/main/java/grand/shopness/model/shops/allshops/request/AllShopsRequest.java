package grand.shopness.model.shops.allshops.request;

import com.google.gson.annotations.SerializedName;

public class AllShopsRequest {
    @SerializedName("service_id")
    private int id;

    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
