package grand.shopness.model.shops.mainshops.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("service")
	private List<ServiceItem> service;

	@SerializedName("banner")
	private List<BannerItem> banner;

	public void setService(List<ServiceItem> service){
		this.service = service;
	}

	public List<ServiceItem> getService(){
		return service;
	}

	public void setBanner(List<BannerItem> banner){
		this.banner = banner;
	}

	public List<BannerItem> getBanner(){
		return banner;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"service = '" + service + '\'' + 
			",banner = '" + banner + '\'' + 
			"}";
		}
}