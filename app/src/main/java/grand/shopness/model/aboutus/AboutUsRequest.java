package grand.shopness.model.aboutus;

public class AboutUsRequest {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
