package grand.shopness.model.bedelegate;

public class BeDelegateResponse{
	private BeDelegate data;
	private String message;
	private int status;

	public void setData(BeDelegate data){
		this.data = data;
	}

	public BeDelegate getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"BeDelegateResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
