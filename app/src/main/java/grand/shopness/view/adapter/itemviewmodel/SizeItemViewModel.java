package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.product.color.ColorsRequest;
import grand.shopness.model.product.color.ColorsResponse;
import grand.shopness.model.product.size.SizeItem;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SizeItemViewModel extends BaseViewModel {
    private SizeItem sizeItem;
    private ColorsRequest colorsRequest;

    public SizeItemViewModel(SizeItem sizeItem, int productId) {
        this.sizeItem = sizeItem;
        colorsRequest = new ColorsRequest();
        colorsRequest.setProductId(productId);
        colorsRequest.setSizeId(sizeItem.getId());

    }

    @Bindable
    public SizeItem getSizeItem() {
        return sizeItem;
    }

    public void onSizeClick() {
        setValue(null);
        getColors();
    }

    private void getColors() {
        accessLoadingBar(VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                ColorsResponse colorsResponse = ((ColorsResponse) response);
                switch (colorsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        setValue(colorsResponse.getData());
                        break;
                    case WebServices.FAILED:
                        setMessage(colorsResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.GET_COLORS, colorsRequest, ColorsResponse.class);
    }


}
