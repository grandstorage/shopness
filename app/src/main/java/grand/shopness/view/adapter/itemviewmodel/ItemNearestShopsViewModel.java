package grand.shopness.view.adapter.itemviewmodel;

import android.widget.ImageView;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.model.shops.allshops.response.AllShopsData;
import grand.shopness.model.shops.allshops.response.NearestShopsItem;

public class ItemNearestShopsViewModel extends BaseViewModel {
    private NearestShopsItem nearestShopsItem;
    private AllShopsData allShopsData;
    public ItemNearestShopsViewModel(NearestShopsItem nearestShopsItem, AllShopsData allShopsData) {
        this.nearestShopsItem = nearestShopsItem;
        this.allShopsData = allShopsData;
    }


    @Bindable
    public NearestShopsItem getNearestShopsItem() {
        return nearestShopsItem;
    }

    @Bindable
    public AllShopsData getAllShopsdata() {
        return allShopsData;
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imagePath){
        ConnectionHelper.loadImage(view,  imagePath);
    }

    public void setActionListener(){
        setValue(null);
    }

}
