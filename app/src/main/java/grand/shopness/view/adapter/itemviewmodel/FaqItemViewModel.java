package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;
import grand.shopness.base.BaseViewModel;
import grand.shopness.model.faq.FaqData;

public class FaqItemViewModel extends BaseViewModel {

    private FaqData faqItem;

    public FaqItemViewModel(FaqData faqItem) {
        this.faqItem = faqItem;
    }

    @Bindable
    public FaqData getFaqData() {
        return faqItem;
    }

    public void setActionListener(){
        /**
         - here to turn on {@link MutableLiveData} and pass a value (maybe null)
         - setItem is an Override method.
         */
        setValue(null);
    }
    
}
