package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.model.shops.details.ProductsItem;
import grand.shopness.view.adapter.itemviewmodel.ItemProductShopDetailViewModel;
import grand.shopness.view.adapter.viewholder.shopdetails.ShopDetailsProductViewHolder;
import timber.log.Timber;

public class ShopDetailsProductAdapter extends RecyclerView.Adapter<ShopDetailsProductViewHolder> {
    private List<ProductsItem> dataList;
    private MutableLiveData<Object> mutableLiveData;
    private String message;

    public ShopDetailsProductAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public MutableLiveData<Object> getMutableLiveData() {
        return mutableLiveData;
    }

    public String getMessage() {
        return message;
    }

    public void updateDataList(List<ProductsItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ShopDetailsProductViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ShopDetailsProductViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public ShopDetailsProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_shop_details_product_item,
                new FrameLayout(parent.getContext()), false);
        return new ShopDetailsProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopDetailsProductViewHolder holder, int position) {
        ItemProductShopDetailViewModel viewModel = new ItemProductShopDetailViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            if (o instanceof Integer) {
                int res = ((Integer) o);
                if (res == Codes.PRODUCT_DETAILS) {
                    mutableLiveData.setValue(getCurrentItem(position));
                } else if (res == Codes.SHOW_MESSAGE) {
                    message = viewModel.getMessage();
                    mutableLiveData.setValue(res);
                }else {
                    mutableLiveData.setValue(res);
                }
            }
        });
    }

    private ProductsItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }
}
