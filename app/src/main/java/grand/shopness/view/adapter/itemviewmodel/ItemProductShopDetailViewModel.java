package grand.shopness.view.adapter.itemviewmodel;

import android.view.View;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.favourite.change.ChangeFavResponse;
import grand.shopness.model.favourite.change.ChangeFavouriteRequest;
import grand.shopness.model.shops.details.ProductsItem;

public class ItemProductShopDetailViewModel extends BaseViewModel {
    private ProductsItem productItem;
    private ChangeFavResponse favResponse;
    private ChangeFavouriteRequest favRequest;
    public ItemProductShopDetailViewModel(ProductsItem productItem) {
        this.productItem = productItem;
        favRequest = new ChangeFavouriteRequest();
    }

    @Bindable
    public ChangeFavResponse getFavResponse() {
        return favResponse;
    }

    @Bindable
    public ProductsItem getProductItem() {
        return productItem;
    }

    public void setProductItem(ProductsItem productItem) {
        this.productItem = productItem;
    }

    public void setItemClickListener() {
        setValue(Codes.PRODUCT_DETAILS);
    }

    public void onFavClick(int code) {
        favRequest.setProductId(code);
        changeChangeFavouriteRequest();
    }

    private void changeChangeFavouriteRequest() {
        accessLoadingBar(View.VISIBLE);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                favResponse = ((ChangeFavResponse) response);
                switch (favResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        productItem.setUserFavorite(favResponse.getIsFavorite());
                        notifyChange();
                        setMessage(favResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                    case WebServices.FAILED:
                        setMessage(favResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.CHANGE_FAVOURITE , favRequest, ChangeFavResponse.class);
    }

}

