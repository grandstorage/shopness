package grand.shopness.view.adapter.viewholder.shopdetails;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.databinding.SingleItemShopDetailsCategoryItemBinding;
import grand.shopness.view.adapter.itemviewmodel.ItemCategoryShopDetailsViewModel;

public class ShopDetailCategoryViewHolder extends RecyclerView.ViewHolder {
    private SingleItemShopDetailsCategoryItemBinding binding;

    public ShopDetailCategoryViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemCategoryShopDetailsViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }


    public void changeTextColorOnClick(int position, int selectedCell) {
        binding.btnName.setTextColor(position == selectedCell ?
                MyApplication.getInstance().getApplicationContext().getResources().getColor(R.color.white) :
                MyApplication.getInstance().getApplicationContext().getResources().getColor(R.color.colorAccent));
    }
}
