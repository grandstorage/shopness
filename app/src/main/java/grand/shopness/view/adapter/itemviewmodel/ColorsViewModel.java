package grand.shopness.view.adapter.itemviewmodel;

import android.graphics.Color;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import com.google.android.material.card.MaterialCardView;
import grand.shopness.base.BaseViewModel;
import grand.shopness.model.product.color.ColorItem;
import timber.log.Timber;

public class ColorsViewModel extends BaseViewModel {

    private ColorItem colorItem;

    public ColorsViewModel(ColorItem colorItem) {
        this.colorItem = colorItem;
    }

    @Bindable
    public ColorItem getColorItem() {
        return colorItem;
    }

    @BindingAdapter({"bgColor"})
    public static void setImageViewResource(MaterialCardView materialButton, String colorRes) {
        try {
            materialButton.setCardBackgroundColor(Color.parseColor(colorRes));
        }catch (Exception e){
            materialButton.setBackgroundColor(Color.parseColor("#FFFFFF")); //default
            Timber.e(e);
        }
    }


    public void onColorClick(){
        setValue(null);
    }

}
