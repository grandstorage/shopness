package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.model.favourite.view.WhishlistItem;
import grand.shopness.view.adapter.itemviewmodel.ItemWishlistViewModel;
import grand.shopness.view.adapter.viewholder.FavViewHolder;
import timber.log.Timber;

public class FavouriteAdapter extends RecyclerView.Adapter<FavViewHolder> {

    private List<WhishlistItem> dataList;
    public MutableLiveData<WhishlistItem> mutableLiveData;


    public FavouriteAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public void updateDataList(List<WhishlistItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull FavViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull FavViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public FavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_wishlist_view,
                new FrameLayout(parent.getContext()), false);
        return new FavViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavViewHolder holder, int position) {
        ItemWishlistViewModel viewModel = new ItemWishlistViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
//            notifyItemChanged(position);
            //listen inside the adapter for mutable data change to get the item details
            if (o instanceof Integer) {
                if ((Integer) o == WebServices.SUCCESS)
                    holder.setColorsToSpinner();
            } else {
                mutableLiveData.setValue(getCurrentItem(position));
            }
        });
    }



    public WhishlistItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
