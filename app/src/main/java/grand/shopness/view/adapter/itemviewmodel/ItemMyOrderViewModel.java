package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;

import grand.shopness.base.BaseViewModel;
import grand.shopness.model.myorders.MyOrderItem;

public class ItemMyOrderViewModel extends BaseViewModel {
    private MyOrderItem myOrderItem;

    public ItemMyOrderViewModel(MyOrderItem myOrderItem) {
        this.myOrderItem = myOrderItem;
    }

    @Bindable
    public MyOrderItem getMyOrderItem() {
        return myOrderItem;
    }



}
