package grand.shopness.view.adapter.viewholder.mainshops;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.databinding.SingleItemMainShopsHeaderBinding;
import grand.shopness.view.adapter.itemviewmodel.HeaderItemMainShopViewModel;

public class MainShopsHeaderViewHolder extends RecyclerView.ViewHolder {
    private SingleItemMainShopsHeaderBinding binding;
    public MainShopsHeaderViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(HeaderItemMainShopViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }
}
