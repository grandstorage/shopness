package grand.shopness.view.adapter.parent;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import timber.log.Timber;

public class DefaultSpinnerAdapter extends ArrayAdapter<String> {

    private List<String> mDataList;
    private Context mContext;
    private int resLayout;

    public DefaultSpinnerAdapter(@NonNull Context context, List<String> mDataList, int resLayout) {
        super(context, resLayout);
        this.resLayout = resLayout;
        this.mDataList = mDataList;
        this.mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert mInflater != null;
            convertView = mInflater.inflate(resLayout, parent, false);
            mViewHolder.mName = convertView.findViewById(R.id.tv_name);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mName.setText(mDataList.get(position));
        return convertView;
    }

    private static class ViewHolder {
        TextView mName;
    }

}