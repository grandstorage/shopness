package grand.shopness.view.adapter.viewholder;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.os.Build;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.databinding.SingleRadioButtonItemViewBinding;
import grand.shopness.view.adapter.itemviewmodel.ItemPaymentViewModel;

public class PaymentViewHolder extends RecyclerView.ViewHolder {

    private SingleRadioButtonItemViewBinding binding;

    public PaymentViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemPaymentViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

    public void setOnClickChange(int selectedCell) {

        binding.btnRadio.setIconTint(getAdapterPosition() == selectedCell ?
                ColorStateList.valueOf(MyApplication.getInstance().getApplicationContext()
                        .getResources().getColor(R.color.colorPrimary)) :
                ColorStateList.valueOf(MyApplication.getInstance().getApplicationContext()
                        .getResources().getColor(R.color.grey_dark)));

        binding.radioBtn.setChecked(getAdapterPosition() == selectedCell);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.radioBtn.setButtonTintList(getAdapterPosition() == selectedCell ?
                    ColorStateList.valueOf(MyApplication.getInstance().getApplicationContext()
                            .getResources().getColor(R.color.colorPrimary)) :
                    ColorStateList.valueOf(MyApplication.getInstance().getApplicationContext()
                            .getResources().getColor(R.color.grey_dark)));
        }


    }
}
