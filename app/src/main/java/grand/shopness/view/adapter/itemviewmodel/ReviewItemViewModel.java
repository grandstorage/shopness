package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.shopness.base.BaseViewModel;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.model.reviews.ReviewItem;

public class ReviewItemViewModel extends BaseViewModel {

    private ReviewItem reviewItem;

    public ReviewItemViewModel(ReviewItem reviewItem) {
        this.reviewItem = reviewItem;
    }

    @Bindable
    public ReviewItem getReviewItem() {
        return reviewItem;
    }

    @BindingAdapter({"imageUrl"})
    public static void getImageBinding(CircleImageView imageView, String imagePath) {
        //you can set layout manager here
        ConnectionHelper.loadImage(imageView, imagePath);
    }

    public void setActionListener(){
        /**
         - here to turn on {@link MutableLiveData} and pass a value (maybe null)
         - setItem is an Override method.
         */
        setValue(null);
    }
}
