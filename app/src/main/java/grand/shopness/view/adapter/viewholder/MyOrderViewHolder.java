package grand.shopness.view.adapter.viewholder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import grand.shopness.databinding.SingleItemMyOrderViewBinding;
import grand.shopness.view.adapter.itemviewmodel.ItemMyOrderViewModel;

public class MyOrderViewHolder extends RecyclerView.ViewHolder {
    SingleItemMyOrderViewBinding binding;

    public MyOrderViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemMyOrderViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }
}
