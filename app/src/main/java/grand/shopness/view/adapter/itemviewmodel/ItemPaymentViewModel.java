package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;

import grand.shopness.base.BaseViewModel;
import grand.shopness.model.payment.PaymentItem;

public class ItemPaymentViewModel extends BaseViewModel {

    private PaymentItem paymentItem;

    public ItemPaymentViewModel(PaymentItem faqItem) {
        this.paymentItem = faqItem;
    }

    @Bindable
    public PaymentItem getPaymentItem() {
        return paymentItem;
    }

    public void onClickListener(){
        setValue(null);
    }
}
