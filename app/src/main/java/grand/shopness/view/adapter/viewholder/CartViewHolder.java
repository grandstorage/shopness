package grand.shopness.view.adapter.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.databinding.SingleItemCartViewBinding;
import grand.shopness.util.MyTextUtils;
import grand.shopness.view.adapter.itemviewmodel.ItemCartViewModel;

public class CartViewHolder extends RecyclerView.ViewHolder {


    private SingleItemCartViewBinding binding;

    public CartViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }else {
            MyTextUtils.strikeThrough(binding.tvPriceBeforeDiscount);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemCartViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

}
