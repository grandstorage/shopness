package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;
import grand.shopness.base.BaseViewModel;
import grand.shopness.model.shops.details.CategoryItem;

public class ItemCategoryShopDetailsViewModel extends BaseViewModel {
    private CategoryItem categoryItem;

    public ItemCategoryShopDetailsViewModel(CategoryItem categoryItem) {
        this.categoryItem = categoryItem;
    }

    @Bindable
    public CategoryItem getShopDetailsCategoryItem() {
        return categoryItem;
    }

    public void setActionListener(){
        /**
         - here to turn on {@link MutableLiveData} and pass a value (maybe null)
         - setItem is an Override method.
         */
        setValue(null);
    }
}
