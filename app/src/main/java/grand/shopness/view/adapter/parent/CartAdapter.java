package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.cart.view.CartItem;
import grand.shopness.view.adapter.itemviewmodel.ItemCartViewModel;
import grand.shopness.view.adapter.viewholder.CartViewHolder;
import timber.log.Timber;

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {
    private List<CartItem> dataList;
    public MutableLiveData<Integer> mutableLiveData;

    public MutableLiveData<Integer> getMutableLiveData() {
        return mutableLiveData;
    }

    public CartAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public void updateDataList(List<CartItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CartViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull CartViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_cart_view,
                new FrameLayout(parent.getContext()), false);
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        ItemCartViewModel viewModel = new ItemCartViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            mutableLiveData.setValue((Integer) o);
//            notifyItemChanged(position);
        });
    }

    public CartItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void deleteItem(int position) {
//        mRecentlyDeletedItem = mListItems.get(position);
//        mRecentlyDeletedItemPosition = position;
//        mListItems.remove(position);
//        notifyItemRemoved(position);
//        showUndoSnackbar();
        Timber.e("delete");
    }
}