package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.shops.allshops.response.AllShopsData;
import grand.shopness.model.shops.allshops.response.NearestShopsItem;
import grand.shopness.view.adapter.itemviewmodel.ItemNearestShopsViewModel;
import grand.shopness.view.adapter.viewholder.NearestShopsViewHolder;
import timber.log.Timber;

public class NearestShopAdapter extends RecyclerView.Adapter<NearestShopsViewHolder> {
    private List<NearestShopsItem> dataList;
    public MutableLiveData<NearestShopsItem> mutableLiveData;
    private AllShopsData allShopsData;

    public NearestShopAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public void updateDataList(List<NearestShopsItem> dataList, AllShopsData allShopsData) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        this.allShopsData = allShopsData;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull NearestShopsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull NearestShopsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public NearestShopsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_nearest_shops,
                new FrameLayout(parent.getContext()), false);
        return new NearestShopsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NearestShopsViewHolder holder, int position) {
        ItemNearestShopsViewModel shopsViewModel = new ItemNearestShopsViewModel(getCurrentItem(position),
                allShopsData);
        holder.setViewModel(shopsViewModel);
        shopsViewModel.getMutableLiveData().observeForever(o -> {
            notifyItemChanged(position);
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    public NearestShopsItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }
}