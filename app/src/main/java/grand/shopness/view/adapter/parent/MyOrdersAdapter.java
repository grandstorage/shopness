package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.shopness.R;
import grand.shopness.model.myorders.MyOrderItem;
import grand.shopness.view.adapter.itemviewmodel.ItemMyOrderViewModel;
import grand.shopness.view.adapter.viewholder.MyOrderViewHolder;
import grand.shopness.viewmodel.fragment.main.MyOrdersViewModel;
import timber.log.Timber;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrderViewHolder> {
    public MutableLiveData<MyOrderItem> mutableLiveData;
    private List<MyOrderItem> dataList;

    public MyOrdersAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public MutableLiveData<MyOrderItem> getMutableLiveData() {
        return mutableLiveData;
    }

    public void updateDataList(List<MyOrderItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull MyOrderViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull MyOrderViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public MyOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_my_order_view,
                new FrameLayout(parent.getContext()), false);
        return new MyOrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrderViewHolder holder, int position) {
        ItemMyOrderViewModel viewModel = new ItemMyOrderViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            notifyItemChanged(position);
            //listen inside the adapter for mutable data change to get the item details
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    public MyOrderItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return  dataList.size() ;
    }
}
