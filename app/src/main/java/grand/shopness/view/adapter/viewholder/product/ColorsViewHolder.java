package grand.shopness.view.adapter.viewholder.product;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.databinding.SingleItemProductDetailsColorViewBinding;
import grand.shopness.model.product.color.ColorItem;
import grand.shopness.view.adapter.itemviewmodel.ColorsViewModel;
import timber.log.Timber;

public class ColorsViewHolder extends RecyclerView.ViewHolder {
    private SingleItemProductDetailsColorViewBinding binding;
    private Context context = MyApplication.getInstance().getApplicationContext();

    public ColorsViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ColorsViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

    public void setChipBackgroundColor(int selectedCell, ColorItem currentItem) {
        if (getAdapterPosition() == selectedCell) {
            binding.btnIconStroke.setIcon(context.getResources().getDrawable(R.drawable.ic_check_mark));
            binding.btnIconStroke.setStrokeColor(ColorStateList.valueOf(Color.parseColor(currentItem.getColor())));
        } else {
            binding.btnIconStroke.setIcon(null);
            binding. btnIconStroke.setStrokeColor(ColorStateList.valueOf(context.getResources()
                    .getColor(android.R.color.transparent)));
        }
    }

}
