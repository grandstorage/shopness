package grand.shopness.view.adapter.itemviewmodel;

import android.widget.ImageView;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.model.shops.allshops.response.AllShopsData;
import grand.shopness.model.shops.allshops.response.AllShopsItem;

public class ItemAllShopsViewModel extends BaseViewModel {
    private AllShopsItem allShopsItem;
    private AllShopsData allShopsData;
    public ItemAllShopsViewModel(AllShopsItem allShopsItem, AllShopsData allShopsData) {
        this.allShopsItem = allShopsItem;
        this.allShopsData = allShopsData;
    }

    @Bindable
    public AllShopsItem getAllShopsItem() {
        return allShopsItem;
    }

    @Bindable
    public AllShopsData getAllShopsdata() {
        return allShopsData;
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imagePath){
        ConnectionHelper.loadImage(view,  imagePath);
    }

    public void setActionListener(){
        setValue(null);
    }


}
