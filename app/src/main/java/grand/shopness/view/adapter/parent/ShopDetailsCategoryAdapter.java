package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.shops.details.CategoryItem;
import grand.shopness.view.adapter.itemviewmodel.ItemCategoryShopDetailsViewModel;
import grand.shopness.view.adapter.viewholder.shopdetails.ShopDetailCategoryViewHolder;
import timber.log.Timber;

public class ShopDetailsCategoryAdapter extends RecyclerView.Adapter<ShopDetailCategoryViewHolder> {
    private List<CategoryItem> dataList;
    private MutableLiveData<CategoryItem> mutableLiveData;
    int selectedCell = 0;

    public ShopDetailsCategoryAdapter() {
        dataList = new ArrayList<>();
        mutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<CategoryItem> getMutableLiveData() {
        return mutableLiveData;
    }

    public void updateDataList(List<CategoryItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ShopDetailCategoryViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ShopDetailCategoryViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public ShopDetailCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_shop_details_category_item,
                new FrameLayout(parent.getContext()), false);
        return new ShopDetailCategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopDetailCategoryViewHolder holder, int position) {
        holder.changeTextColorOnClick(position, selectedCell);
        ItemCategoryShopDetailsViewModel shopsViewModel = new ItemCategoryShopDetailsViewModel(getCurrentItem(position));
        holder.setViewModel(shopsViewModel);
        shopsViewModel.getMutableLiveData().observeForever(o -> {
            mutableLiveData.setValue(getCurrentItem(position));
            selectedCell = position;
            notifyDataSetChanged();
        });
    }

    public CategoryItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }


}
