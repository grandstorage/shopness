package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.cart.view.CartItem;
import grand.shopness.view.adapter.itemviewmodel.ItemCartViewModel;
import grand.shopness.view.adapter.viewholder.OrdersItemsViewHolder;
import timber.log.Timber;

public class OrdersItemsAdapter extends RecyclerView.Adapter<OrdersItemsViewHolder> {
    private List<CartItem> dataList;
    public MutableLiveData<CartItem> mutableLiveData;

    public OrdersItemsAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public void updateDataList(List<CartItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull OrdersItemsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull OrdersItemsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public OrdersItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_orders_view,
                new FrameLayout(parent.getContext()), false);
        return new OrdersItemsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersItemsViewHolder holder, int position) {
        ItemCartViewModel viewModel = new ItemCartViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            notifyItemChanged(position);
            //listen inside the adapter for mutable data change to get the item details
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    public CartItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return  dataList.size() ;
    }
}