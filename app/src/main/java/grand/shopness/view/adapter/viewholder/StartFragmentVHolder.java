package grand.shopness.view.adapter.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.databinding.SingleItemStartFragmentBinding;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

public class StartFragmentVHolder extends RecyclerView.ViewHolder {
    private SingleItemStartFragmentBinding binding;

    public StartFragmentVHolder(@NonNull SingleItemStartFragmentBinding itemView) {
        super(itemView.getRoot());
        binding = itemView;
    }

    public void onBind(Integer currentNode) {
        binding.imageView.setImageResource(currentNode);
    }


}
