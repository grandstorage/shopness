package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.offers.OfferItem;
import grand.shopness.view.adapter.itemviewmodel.OffersItemViewModel;
import grand.shopness.view.adapter.viewholder.offers.OffersHeaderViewHolder;
import grand.shopness.view.adapter.viewholder.offers.OffersItemViewHolder;
import timber.log.Timber;

public class OffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<OfferItem> dataList;
    private MutableLiveData<OfferItem> mutableLiveData;

    public OffersAdapter() {
        dataList = new ArrayList<>();
        mutableLiveData = new MutableLiveData<>();
    }

    public void updateDataList(List<OfferItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    public MutableLiveData<OfferItem> getMutableLiveData() {
        return mutableLiveData;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        if (holder instanceof OffersHeaderViewHolder) {
            ((OffersHeaderViewHolder) holder).bind();
        } else
            ((OffersItemViewHolder) holder).bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        if (holder instanceof OffersHeaderViewHolder) {
            ((OffersHeaderViewHolder) holder).bind();
        } else
            ((OffersItemViewHolder) holder).bind();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_HEADER:
                View itemHeader = layoutInflater.inflate(R.layout.single_item_offer_header_view,
                        new FrameLayout(parent.getContext()), false);
                return getViewHolder(TYPE_HEADER, itemHeader);
            case TYPE_ITEM:
                View itemView = layoutInflater.inflate(R.layout.single_item_offer_view,
                        new FrameLayout(parent.getContext()), false);
                return getViewHolder(TYPE_ITEM, itemView);
            default:
                View itemView2 = layoutInflater.inflate(R.layout.single_item_offer_view,
                        new FrameLayout(parent.getContext()), false);
                return getViewHolder(TYPE_ITEM, itemView2);
        }
    }

    private int counter = 0;

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            counter++;
            return TYPE_HEADER;
        } else {
            if (counter == 0)
                return TYPE_HEADER;
            else {
                if (counter == 2)
                    counter = 0;
                else
                    counter++;
                return TYPE_ITEM;
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OffersItemViewModel itemViewModel = new OffersItemViewModel(getCurrentItem(position));
        if (holder instanceof OffersHeaderViewHolder) {
            ((OffersHeaderViewHolder) holder).setViewModel(itemViewModel);
            itemViewModel.getMutableLiveData().observeForever(o -> {
                notifyItemChanged(position);
                //listen inside the adapter for mutable data change to get the item details
                mutableLiveData.setValue(getCurrentItem(position));
            });
        } else {
            ((OffersItemViewHolder) holder).setViewModel(itemViewModel);
            itemViewModel.getMutableLiveData().observeForever(o -> {
                notifyItemChanged(position);
                //listen inside the adapter for mutable data change to get the item details
                mutableLiveData.setValue(getCurrentItem(position));
            });
        }
    }

    private OfferItem getCurrentItem(int position) {
        return dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }


    private RecyclerView.ViewHolder getViewHolder(int type, View v) {
        switch (type) {
            case TYPE_HEADER:
                return new OffersHeaderViewHolder(v);
            case TYPE_ITEM:
                return new OffersItemViewHolder(v);
            default:
                return new OffersItemViewHolder(v);
        }
    }


}
