package grand.shopness.view.adapter.viewholder;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import grand.shopness.databinding.SingleItemAllShopsBinding;
import grand.shopness.view.adapter.itemviewmodel.ItemAllShopsViewModel;

public class AllShopsViewHolder extends RecyclerView.ViewHolder {
    private SingleItemAllShopsBinding binding;

    public AllShopsViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemAllShopsViewModel viewModel) {
        if (binding != null) {
            binding.setViewModelBinding(viewModel);
        }
    }


}
