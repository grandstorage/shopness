package grand.shopness.view.adapter.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import grand.shopness.databinding.SingleItemOrdersViewBinding;
import grand.shopness.util.MyTextUtils;
import grand.shopness.view.adapter.itemviewmodel.ItemCartViewModel;

/**
 * Created by MahmoudAyman on 7/27/2019.
 **/
public class OrdersItemsViewHolder extends RecyclerView.ViewHolder {

    private SingleItemOrdersViewBinding binding;

    public OrdersItemsViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }else {
            MyTextUtils.strikeThrough(binding.tvPriceBeforeDiscount);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemCartViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

}
