package grand.shopness.view.adapter.itemviewmodel;

import android.view.View;
import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.android.volley.Request;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.cart.add.AddCartResponse;
import grand.shopness.model.cart.add.UpdateCartRequest;
import grand.shopness.model.cart.view.CartItem;
import timber.log.Timber;

public class ItemCartViewModel extends BaseViewModel {
    private CartItem cartItem;
    private UpdateCartRequest updateCartRequest;

    public ItemCartViewModel(CartItem cartItem) {
        this.cartItem = cartItem;
        updateCartRequest = new UpdateCartRequest();
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageViewResource(ImageView imageView, String imagePath) {
        ConnectionHelper.loadImage(imageView, imagePath);
    }


    @Bindable
    public CartItem getCartItem() {
        return cartItem;
    }

    public void onQuantityBtnClick(int plus) {
        accessLoadingBar(View.VISIBLE);
        updateCartRequest.setCartItemId(cartItem.getCartItemId());
        if (plus == 0){ //minus btn
            updateCartRequest.setPlus(0);
        }else { // plus btn
            updateCartRequest.setPlus(1);
        }
        updateNetworkRequest(plus);
    }

    private void updateNetworkRequest(int plus) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                AddCartResponse addCartResponse = ((AddCartResponse) response);
                switch (addCartResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        cartItem.setQty(plus == 0 ? cartItem.getQty() - 1 : cartItem.getQty() + 1);
                        notifyChange();
                        setMessage(addCartResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                    case WebServices.FAILED:
                        setMessage(addCartResponse.getMessage());
                        setValue(Codes.SHOW_MESSAGE);
                        break;
                }
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(Request.Method.POST, WebServices.ADD_UPDATE_CART, updateCartRequest, AddCartResponse.class);
    }

}
