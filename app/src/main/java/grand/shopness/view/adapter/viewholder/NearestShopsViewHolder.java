package grand.shopness.view.adapter.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.databinding.SingleItemNearestShopsBinding;
import grand.shopness.view.adapter.itemviewmodel.ItemNearestShopsViewModel;

public class NearestShopsViewHolder extends RecyclerView.ViewHolder {
    private SingleItemNearestShopsBinding binding;

    public NearestShopsViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemNearestShopsViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }


}
