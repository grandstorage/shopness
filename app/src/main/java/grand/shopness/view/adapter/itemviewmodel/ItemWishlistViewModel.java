package grand.shopness.view.adapter.itemviewmodel;

import androidx.databinding.Bindable;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.base.volleyutils.ConnectionListener;
import grand.shopness.model.cart.add.AddCartRequest;
import grand.shopness.model.favourite.view.SizesItem;
import grand.shopness.model.favourite.view.WhishlistItem;
import grand.shopness.model.product.color.ColorItem;
import grand.shopness.model.product.color.ColorsRequest;
import grand.shopness.model.product.color.ColorsResponse;
import timber.log.Timber;

public class ItemWishlistViewModel extends BaseViewModel {
    private WhishlistItem favItem;
    private AddCartRequest addCartRequest;
    private ColorsRequest colorsRequest;
    private ColorsResponse colorsResponse;

    public ItemWishlistViewModel(WhishlistItem favItem) {
        this.favItem = favItem;
        addCartRequest = new AddCartRequest();
        colorsRequest = new ColorsRequest();
    }

    @Bindable
    public WhishlistItem getFavItem() {
        return favItem;
    }

    @Bindable
    public ColorsRequest getColorsRequest() {
        return colorsRequest;
    }

    public AddCartRequest getAddCartRequest() {
        return addCartRequest;
    }

    public void setOnItemClick() {
        setValue(null);
    }

    public void setOnCartClick() {
        addCartRequest.setProductId(favItem.getId());
        addCartRequest.setQty(1);
        if (favItem.getIsClothes() == 1)
            addToCartRequest();
    }

    private void addToCartRequest() {

    }

    public List<SizesItem> getSizesList() {
        return favItem.getProductSize().get(0).getSizes();
    }

    public ArrayList<ColorItem> getColorsList() {
        return colorItemList == null? colorItemList = new ArrayList<>() : colorItemList;
    }

    private ArrayList<ColorItem> colorItemList;

    public void getColorsFromServer(int sizeId) {
        colorsRequest.setLoading(true);
        notifyChange();
        colorsRequest.setSizeId(sizeId);
        colorsRequest.setProductId(favItem.getId());
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                colorsResponse = ((ColorsResponse) response);
                switch (colorsResponse.getStatus()) {
                    case WebServices.SUCCESS:
                        Timber.e("sss");
                        colorItemList.addAll(colorsResponse.getData());
                        setValue(WebServices.SUCCESS);
                        break;
                    case WebServices.FAILED:
                        break;
                }
                Timber.e("here");
                colorsRequest.setLoading(false);
                notifyChange();
            }
        }).requestJsonObject(Request.Method.POST, WebServices.GET_COLORS, colorsRequest, ColorsResponse.class);
    }

}
