package grand.shopness.view.adapter.viewholder;

import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.base.BaseSpinner;
import grand.shopness.databinding.SingleItemWishlistViewBinding;
import grand.shopness.model.favourite.view.SizesItem;
import grand.shopness.model.product.color.ColorItem;
import grand.shopness.view.adapter.itemviewmodel.ItemWishlistViewModel;
import timber.log.Timber;

public class FavViewHolder extends RecyclerView.ViewHolder {


    SingleItemWishlistViewBinding binding;

    public FavViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemWishlistViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
        if (viewModel.getFavItem().getIsClothes() == 1) {
            setSizesSpinner();
        }

    }

    private void setSizesSpinner() {
        ArrayList<String> sizeNames = new ArrayList<>();
        sizeNames.add(MyApplication.getInstance().getApplicationContext().getString(R.string.size));
        for (SizesItem item :
                binding.getViewModel().getSizesList()) {
            sizeNames.add("" + item.getSizeName());
        }

        BaseSpinner.setSpinner(binding.spinnerSize, sizeNames, R.layout.single_item_spinner_sizes)
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i > 0) {
                            binding.getViewModel().getAddCartRequest().setSize(binding.getViewModel().getSizesList().get(i - 1).getId());
                            binding.getViewModel().getColorsFromServer(binding.getViewModel().getSizesList().get(i - 1).getId());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
    }

    public void setColorsToSpinner() {
        ArrayList<ColorItem> colorItems = binding.getViewModel().getColorsList();
        ArrayList<String> colorsString = new ArrayList<>();

        for (ColorItem item :
                colorItems) {
            colorsString.add(item.getColor());
        }

        BaseSpinner.setColorSpinner(binding.spinnerColors, colorsString)
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (i > 0) {
                            Timber.e("color: %s", colorItems.get(i).getId());
                            binding.getViewModel().getAddCartRequest().setProductColor(colorItems.get(i).getId());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
    }
}
