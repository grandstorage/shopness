package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.payment.PaymentItem;
import grand.shopness.view.adapter.itemviewmodel.ItemPaymentViewModel;
import grand.shopness.view.adapter.viewholder.PaymentViewHolder;
import timber.log.Timber;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentViewHolder> {
    private List<PaymentItem> dataList;
    private MutableLiveData<PaymentItem> mutableLiveData;
    private int selectedCell = 0;

    public PaymentAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public MutableLiveData<PaymentItem> getMutableLiveData() {
        return mutableLiveData;
    }

    public void updateDataList(List<PaymentItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull PaymentViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull PaymentViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_radio_button_item_view,
                new FrameLayout(parent.getContext()), false);
        return new PaymentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentViewHolder holder, int position) {
        holder.setOnClickChange(selectedCell);
        ItemPaymentViewModel viewModel = new ItemPaymentViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            notifyItemChanged(position);
            selectedCell = position;
            //listen inside the adapter for mutable data change to get the item details
            mutableLiveData.setValue(getCurrentItem(position));
            notifyDataSetChanged();
        });
    }

    public PaymentItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return  dataList.size() ;
    }
}
