package grand.shopness.view.adapter.viewholder.shopdetails;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

import grand.shopness.databinding.SingleItemShopDetailsProductItemBinding;
import grand.shopness.model.shops.details.ProductsItem;
import grand.shopness.util.MyTextUtils;
import grand.shopness.view.adapter.itemviewmodel.ItemProductShopDetailViewModel;

public class ShopDetailsProductViewHolder extends RecyclerView.ViewHolder {
    private SingleItemShopDetailsProductItemBinding binding;

    public ShopDetailsProductViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        } else {
            MyTextUtils.strikeThrough(binding.tvPriceBeforeDiscount);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemProductShopDetailViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

}
