package grand.shopness.view.adapter.itemviewmodel;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.model.offers.OfferItem;

public class OffersItemViewModel extends BaseViewModel {

    private OfferItem offerItem;

    public OffersItemViewModel(OfferItem serviceItem) {
        this.offerItem = serviceItem;
    }

    @Bindable
    public OfferItem getOfferItem() {
        return offerItem;
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageViewResource(ImageView imageView, String imagePath) {
        ConnectionHelper.loadImage(imageView,  imagePath);
    }

    public void setActionListener(){
        /**
         - here to turn on {@link MutableLiveData} and pass a value (maybe null)
         - setItem is an Override method.
         */
        setValue(null);
    }
}
