package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import grand.shopness.R;
import grand.shopness.model.faq.FaqData;
import grand.shopness.view.adapter.itemviewmodel.FaqItemViewModel;
import grand.shopness.view.adapter.viewholder.FaqViewHolder;
import timber.log.Timber;

public class FaqAdapter extends RecyclerView.Adapter<FaqViewHolder> {
    private List<FaqData> dataList;
    public MutableLiveData<Object> mutableLiveData;

    public FaqAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }
    

    public void updateDataList(List<FaqData> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull FaqViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull FaqViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public FaqViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_faq_view,
                new FrameLayout(parent.getContext()), false);
        return new FaqViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FaqViewHolder holder, int position) {
        FaqItemViewModel viewModel = new FaqItemViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
                notifyItemChanged(position);
                //listen inside the adapter for mutable data change to get the item details
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    public FaqData getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return  dataList.size() ;
    }
}
