package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import grand.shopness.R;
import grand.shopness.model.product.color.ColorItem;
import grand.shopness.view.adapter.itemviewmodel.ColorsViewModel;
import grand.shopness.view.adapter.viewholder.product.ColorsViewHolder;
import timber.log.Timber;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsViewHolder> {

    private List<ColorItem> dataList;
    private MutableLiveData<ColorItem> mutableLiveData;
    private int selectedCell = - 1;

    public ColorsAdapter() {
        dataList = new ArrayList<>();
        mutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<ColorItem> getMutableLiveData() {
        return mutableLiveData;
    }

    public void updateDataList(List<ColorItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ColorsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ColorsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public ColorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_product_details_color_view,
                new FrameLayout(parent.getContext()), false);
        return new ColorsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorsViewHolder holder, int position) {
        ColorsViewModel shopsViewModel = new ColorsViewModel(getCurrentItem(position));
        holder.setChipBackgroundColor(selectedCell, getCurrentItem(position));

        holder.setViewModel(shopsViewModel);
        shopsViewModel.getMutableLiveData().observeForever(o -> {
            mutableLiveData.setValue(getCurrentItem(position));
            selectedCell = position;
            notifyDataSetChanged();
        });
    }

    public ColorItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }
}
