package grand.shopness.view.adapter.parent;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import grand.shopness.R;
import timber.log.Timber;

public class ColorSpinnerAdapter extends ArrayAdapter<String> {

    private List<String> mDataList;
    private Context mContext;
    private int resLayout;

    public ColorSpinnerAdapter(@NonNull Context context, List<String> mDataList, int resLayout) {
        super(context, resLayout);
        this.resLayout = resLayout;
        this.mDataList = mDataList;
        this.mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ColorSpinnerAdapter.ViewHolder mViewHolder = new ColorSpinnerAdapter.ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert mInflater != null;
            convertView = mInflater.inflate(resLayout, parent, false);
            mViewHolder.imageView = convertView.findViewById(R.id.imageView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ColorSpinnerAdapter.ViewHolder) convertView.getTag();
        }
        mViewHolder.imageView.setBorderColor(Color.parseColor(mDataList.get(position)));
        return convertView;
    }

    private static class ViewHolder {
        CircleImageView imageView;
    }

}