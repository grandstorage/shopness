package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.shops.allshops.response.AllShopsData;
import grand.shopness.model.shops.allshops.response.AllShopsItem;
import grand.shopness.view.adapter.itemviewmodel.ItemAllShopsViewModel;
import grand.shopness.view.adapter.viewholder.AllShopsViewHolder;
import timber.log.Timber;

public class AllShopsAdapter extends RecyclerView.Adapter<AllShopsViewHolder> {
    private List<AllShopsItem> dataList;
    public MutableLiveData<AllShopsItem> mutableLiveData;
    private AllShopsData allShopsData;

    public AllShopsAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public void updateDataList(List<AllShopsItem> dataList, AllShopsData allShopsData) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        this.allShopsData = allShopsData;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull AllShopsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull AllShopsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public AllShopsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_all_shops,
                new FrameLayout(parent.getContext()), false);
        return new AllShopsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AllShopsViewHolder holder, int position) {
        ItemAllShopsViewModel shopsViewModel = new ItemAllShopsViewModel(getCurrentItem(position), allShopsData);
        holder.setViewModel(shopsViewModel);
        shopsViewModel.getMutableLiveData().observeForever(o -> {
                notifyItemChanged(position);
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    public AllShopsItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }
}
