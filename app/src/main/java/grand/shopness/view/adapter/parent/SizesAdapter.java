package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.product.color.ColorItem;
import grand.shopness.model.product.size.SizeItem;
import grand.shopness.view.adapter.itemviewmodel.SizeItemViewModel;
import grand.shopness.view.adapter.viewholder.product.SizesViewHolder;
import timber.log.Timber;

public class SizesAdapter extends RecyclerView.Adapter<SizesViewHolder> {

    private List<SizeItem> dataList;
    private MutableLiveData<Object> mutableLiveData;
    private int selectedCell = -1;
    private int productId;
    private ColorsAdapter colorsAdapter;

    public SizesAdapter() {
        dataList = new ArrayList<>();
        mutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Object> getMutableLiveData() {
        return mutableLiveData;
    }

    public void setColorsAdapter(ColorsAdapter colorsAdapter) {
        this.colorsAdapter = colorsAdapter;
    }

    public void updateDataList(List<SizeItem> dataList, int productId) {
        Timber.e("array%s", dataList.toString());
        this.dataList.clear();
        this.dataList.addAll(dataList);
        Timber.e(dataList.toString());
        this.productId = productId;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull SizesViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull SizesViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public SizesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_product_details_size_view,
                new FrameLayout(parent.getContext()), false);
        return new SizesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SizesViewHolder holder, int position) {
        holder.setChipBackgroundColor(selectedCell);
        SizeItemViewModel shopsViewModel = new SizeItemViewModel(getCurrentItem(position), productId);
        holder.setViewModel(shopsViewModel);
        shopsViewModel.getMutableLiveData().observeForever(o -> {
            if (o instanceof List) {
                    colorsAdapter.updateDataList(getItems(o));
            }else {
                mutableLiveData.setValue(getCurrentItem(position));
                selectedCell = position;
                notifyDataSetChanged();
            }
        });
    }

    private List<ColorItem> getItems(Object var){
        //to check if list is instance of Color item and remove suppres hint error
        List<ColorItem> result = new ArrayList<>();
        if (var instanceof List){
            for(int i = 0; i < ((List<?>)var).size(); i++){
                Object item = ((List<?>) var).get(i);
                if(item instanceof ColorItem){
                    result.add((ColorItem) item);
                }
            }
        }
        return result;
    }

    public SizeItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }
}
