package grand.shopness.view.adapter.viewholder.product;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.databinding.SingleItemProductDetailsSizeViewBinding;
import grand.shopness.view.adapter.itemviewmodel.SizeItemViewModel;
import timber.log.Timber;

public class SizesViewHolder extends RecyclerView.ViewHolder {

    private SingleItemProductDetailsSizeViewBinding binding;

    public SizesViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(SizeItemViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

    public void setChipBackgroundColor(int selectedCell) {
        binding.btnSizeLayout.setBackgroundColor(getAdapterPosition() == selectedCell ?
                MyApplication.getInstance().getApplicationContext().getResources().getColor(R.color.colorPrimary) :
                MyApplication.getInstance().getApplicationContext().getResources().getColor(R.color.transparent));
    }

}
