package grand.shopness.view.adapter.viewholder.mainshops;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.databinding.SingleItemMainShopsBinding;
import grand.shopness.view.adapter.itemviewmodel.ItemMainShopViewModel;

public class MainShopsItemViewHolder extends RecyclerView.ViewHolder {
    private SingleItemMainShopsBinding binding;
    public MainShopsItemViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(ItemMainShopViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }
}
