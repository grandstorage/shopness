package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.databinding.SingleImageSlideViewBinding;
import grand.shopness.model.shops.mainshops.response.BannerItem;

public class MainShopsSliderAdapt extends PagerAdapter {
    private List<BannerItem> bannerItems;
    private SingleImageSlideViewBinding view;

    public MainShopsSliderAdapt() {
        bannerItems = new ArrayList<>();
    }

    public void updateData(List<BannerItem> bannerItems) {
        this.bannerItems.clear();
        this.bannerItems.addAll(bannerItems);
        notifyDataSetChanged();
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (bannerItems != null)
            return bannerItems.size();
        else
            return 0;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup viewGroup, final int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        view = DataBindingUtil.inflate(inflater, R.layout.single_image_slide_view, viewGroup, false);

        ConnectionHelper.loadImage(view.image, bannerItems.get(position).getBannerImage());

//        view.setOnClickListener(view1 -> listAllListeners.onItemViewClick(bannerItems, position));

        viewGroup.addView(view.getRoot(), 0);
        return view.getRoot();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}
