package grand.shopness.view.adapter.itemviewmodel;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import grand.shopness.base.BaseViewModel;
import grand.shopness.base.volleyutils.ConnectionHelper;
import grand.shopness.model.shops.mainshops.response.ServiceItem;

public class ItemMainShopViewModel extends BaseViewModel {

    private ServiceItem serviceItem;

    public ItemMainShopViewModel(ServiceItem serviceItem) {
        this.serviceItem = serviceItem;
    }

    @Bindable
    public ServiceItem getServiceItem() {
        return serviceItem;
    }

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, String imagePath) {
        ConnectionHelper.loadImage(imageView,  imagePath);
    }

    public void setActionListener(){
        /**
         - here to turn on {@link MutableLiveData} and pass a value (maybe null)
         - setItem is an Override method.
         */
        setValue(null);
    }


}
