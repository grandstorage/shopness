package grand.shopness.view.adapter.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import grand.shopness.databinding.SingleItemFaqViewBinding;
import grand.shopness.view.adapter.itemviewmodel.FaqItemViewModel;

public class FaqViewHolder extends RecyclerView.ViewHolder {

    private SingleItemFaqViewBinding binding;

    public FaqViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(FaqItemViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

}
