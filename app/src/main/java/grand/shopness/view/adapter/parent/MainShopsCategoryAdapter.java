package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.shopness.R;
import grand.shopness.model.shops.mainshops.response.ServiceItem;
import grand.shopness.view.adapter.itemviewmodel.HeaderItemMainShopViewModel;
import grand.shopness.view.adapter.itemviewmodel.ItemMainShopViewModel;
import grand.shopness.view.adapter.viewholder.mainshops.MainShopsHeaderViewHolder;
import grand.shopness.view.adapter.viewholder.mainshops.MainShopsItemViewHolder;
import timber.log.Timber;

public class MainShopsCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<ServiceItem> dataList;
    private MutableLiveData<ServiceItem> mutableLiveData;

    public MainShopsCategoryAdapter() {
        dataList = new ArrayList<>();
        mutableLiveData = new MutableLiveData<>();
    }

    public void updateDataList(List<ServiceItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    public MutableLiveData<ServiceItem> getMutableLiveData() {
        return mutableLiveData;
    }


    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        if (holder instanceof MainShopsHeaderViewHolder) {
            ((MainShopsHeaderViewHolder) holder).bind();
        } else
            ((MainShopsItemViewHolder) holder).bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        if (holder instanceof MainShopsHeaderViewHolder) {
            ((MainShopsHeaderViewHolder) holder).bind();
        } else
            ((MainShopsItemViewHolder) holder).bind();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_HEADER:
                View itemHeader = layoutInflater.inflate(R.layout.single_item_main_shops_header,
                        new FrameLayout(parent.getContext()), false);
                return getViewHolder(TYPE_HEADER, itemHeader);
            case TYPE_ITEM:
                View itemView = layoutInflater.inflate(R.layout.single_item_main_shops,
                        new FrameLayout(parent.getContext()), false);
                return getViewHolder(TYPE_ITEM, itemView);
            default:
                View itemView2 = layoutInflater.inflate(R.layout.single_item_main_shops,
                        new FrameLayout(parent.getContext()), false);
                return getViewHolder(TYPE_ITEM, itemView2);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;
        else
            return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemMainShopViewModel itemViewModel = new ItemMainShopViewModel(getCurrentItem(position));
        HeaderItemMainShopViewModel headerViewModel = new HeaderItemMainShopViewModel(getCurrentItem(position));
        if (holder instanceof MainShopsHeaderViewHolder) {
            ((MainShopsHeaderViewHolder) holder).setViewModel(headerViewModel);
            headerViewModel.getMutableLiveData().observeForever(o -> {
                notifyItemChanged(position);
                //listen inside the adapter for mutable data change to get the item details
                mutableLiveData.setValue(getCurrentItem(position));
            });
        } else {
            ((MainShopsItemViewHolder) holder).setViewModel(itemViewModel);
            itemViewModel.getMutableLiveData().observeForever(o -> {
                notifyItemChanged(position);
                //listen inside the adapter for mutable data change to get the item details
                mutableLiveData.setValue(getCurrentItem(position));
            });
        }
    }

    private ServiceItem getCurrentItem(int position) {
        return dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return dataList != null && !dataList.isEmpty() ? dataList.size() : 0;
    }


    private RecyclerView.ViewHolder getViewHolder(int type, View v) {
        switch (type) {
            case TYPE_HEADER:
                return new MainShopsHeaderViewHolder(v);
            case TYPE_ITEM:
                return new MainShopsItemViewHolder(v);
            default:
                return new MainShopsItemViewHolder(v);
        }
    }


}
