package grand.shopness.view.adapter.viewholder.offers;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import grand.shopness.databinding.SingleItemOfferViewBinding;
import grand.shopness.view.adapter.itemviewmodel.OffersItemViewModel;

public class OffersItemViewHolder extends RecyclerView.ViewHolder {

    private SingleItemOfferViewBinding binding;
    public OffersItemViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(OffersItemViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

}
