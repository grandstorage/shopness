package grand.shopness.view.adapter.viewholder.offers;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import grand.shopness.databinding.SingleItemOfferHeaderViewBinding;
import grand.shopness.view.adapter.itemviewmodel.OffersItemViewModel;

public class OffersHeaderViewHolder extends RecyclerView.ViewHolder {
    private SingleItemOfferHeaderViewBinding binding;
    public OffersHeaderViewHolder(@NonNull View itemView) {
        super(itemView);
        bind();
    }

    public void bind() {
        if (binding == null) {
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void unbind() {
        if (binding != null) {
            binding.unbind(); // Don't forget to unbind
        }
    }

    public void setViewModel(OffersItemViewModel viewModel) {
        if (binding != null) {
            binding.setViewModel(viewModel);
        }
    }

}
