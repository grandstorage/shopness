package grand.shopness.view.adapter.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import grand.shopness.R;
import grand.shopness.model.reviews.ReviewItem;
import grand.shopness.view.adapter.itemviewmodel.ReviewItemViewModel;
import grand.shopness.view.adapter.viewholder.ReviewsViewHolder;
import timber.log.Timber;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsViewHolder> {
    private List<ReviewItem> dataList;
    public MutableLiveData<ReviewItem> mutableLiveData;


    public ReviewsAdapter() {
        mutableLiveData = new MutableLiveData<>();
        dataList = new ArrayList<>();
    }

    public MutableLiveData<ReviewItem> getMutableLiveData() {
        return mutableLiveData;
    }

    public void updateDataList(List<ReviewItem> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ReviewsViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Timber.d("onViewAttachedToWindow");
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ReviewsViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Timber.d("onViewDetachedFromWindow");
        holder.unbind();
    }

    @NonNull
    @Override
    public ReviewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_review_view,
                new FrameLayout(parent.getContext()), false);
        return new ReviewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsViewHolder holder, int position) {
        ReviewItemViewModel viewModel = new ReviewItemViewModel(getCurrentItem(position));
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            notifyItemChanged(position);
            //listen inside the adapter for mutable data change to get the item details
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    private ReviewItem getCurrentItem(int pos) {
        return dataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return  dataList.size() ;
    }
}
