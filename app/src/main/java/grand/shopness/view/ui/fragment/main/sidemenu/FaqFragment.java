package grand.shopness.view.ui.fragment.main.sidemenu;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IntegerRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentFaqBinding;
import grand.shopness.interfaces.OnHomeBackBtnCallBack;
import grand.shopness.model.faq.FaqItem;
import grand.shopness.view.ui.fragment.main.ShopsFragment;
import grand.shopness.viewmodel.fragment.main.FaqViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends BaseFragment implements Observer<Object> {

    FragmentFaqBinding binding;

    public FaqFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_faq, container, false);
        binding.swipeRefresh.setColorSchemeResources(R.color.g_blue, R.color.g_red, R.color.g_yellow, R.color.g_green);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        FaqViewModel viewModel = new FaqViewModel();
         binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this,this);
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }
    }

}
