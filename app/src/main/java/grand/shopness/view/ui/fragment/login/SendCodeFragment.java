package grand.shopness.view.ui.fragment.login;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentSendCodeBinding;
import grand.shopness.viewmodel.fragment.login.SendCodeViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SendCodeFragment extends BaseFragment implements Observer<Object> {

    FragmentSendCodeBinding binding;
    private SendCodeViewModel sendCodeViewModel;

    public SendCodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_code, container, false);
        sendCodeViewModel = new SendCodeViewModel();
        binding.setViewModel(sendCodeViewModel);
        sendCodeViewModel.getMutableLiveData().observe(this, this);
        return binding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int res = (Integer) o;
        if (res == View.VISIBLE || res == View.GONE) {
            showProgressBar(res);
        }

        switch (res) {
            case Codes.PRESS_BACK:
                MovementManager.startActivity(getContext(), Codes.REGISTER_SCREEN);
                break;
            case Codes.HOME_SCREEN:
                MovementManager.startMainActivity(getContext(), Codes.HOME_SCREEN);
                break;
            case Codes.CHANGE_PASSWORD_SCREEN:
                MovementManager.startActivity(getContext(), Codes.CHANGE_PASSWORD_SCREEN);
                break;
            case Codes.SHOW_MESSAGE:
                showMessage(sendCodeViewModel.getMessage());
                break;
        }
    }
}
