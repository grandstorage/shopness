package grand.shopness.view.ui.fragment.login;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentNewPasswordBinding;
import grand.shopness.view.ui.fragment.main.bottombar.MainShopsFragment;
import grand.shopness.viewmodel.fragment.login.NewPasswordViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPasswordFragment extends BaseFragment implements Observer<Object> {

    private FragmentNewPasswordBinding newPasswordBinding;
    private NewPasswordViewModel newPasswordViewModel;

    public NewPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        newPasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_password, container, false);
        newPasswordViewModel = new NewPasswordViewModel();
        newPasswordBinding.setViewModel(newPasswordViewModel);
        newPasswordViewModel.getMutableLiveData().observe(this, this);
        return newPasswordBinding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressBar(result);
        }
        switch (result) {
            case Codes.NOT_EQUAL:
                showMessage(getString(R.string.not_equal));
                YoYo.with(Techniques.Shake).playOn(newPasswordBinding.tilConfirm);
                break;
            case Codes.ENTER_CODE_SCREEN:
                MovementManager.startActivity(getContext(), Codes.ENTER_CODE_SCREEN);
                break;
            case Codes.HOME_SCREEN:
                MovementManager.startMainActivity(getContext(),Codes.HOME_SCREEN);
                break;
            case Codes.SHOW_MESSAGE:
                showMessage(newPasswordViewModel.getMessage());
                break;
        }

    }
}