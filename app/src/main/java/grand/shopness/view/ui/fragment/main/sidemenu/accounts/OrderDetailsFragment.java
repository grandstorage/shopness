package grand.shopness.view.ui.fragment.main.sidemenu.accounts;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentOrderDetailsBinding;
import grand.shopness.viewmodel.fragment.main.OrderDetailViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailsFragment extends BaseFragment implements Observer<Object> {

    private OrderDetailViewModel viewModel;
    private FragmentOrderDetailsBinding binding;

    public OrderDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new OrderDetailViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
    }


    @Override
    public void onChanged(Object o) {

    }
}
