package grand.shopness.view.ui.fragment.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentNearestShopsBinding;
import grand.shopness.viewmodel.fragment.main.NearestShopsViewModel;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class NearestShopsFragment extends BaseFragment implements Observer<Object> {


    private FragmentNearestShopsBinding binding;
    private NearestShopsViewModel viewModel;
    private ReactiveLocationProvider locationProvider;

    public NearestShopsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationProvider = new ReactiveLocationProvider(getContext());
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nearest_shops, container, false);
        binding.swipeRefresh.setColorSchemeResources(R.color.g_blue, R.color.g_red, R.color.g_yellow, R.color.g_green);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new NearestShopsViewModel();
        binding.setViewModel(viewModel);
        viewModel.setLocationProvider(locationProvider);
        viewModel.startPopulateData(); // must set here
        viewModel.getMutableLiveData().observe(this, this);
        viewModel.getNearestShopAdapter().mutableLiveData.observe(this, o -> {
            //on item click
            Bundle bundle = new Bundle();
            bundle.putString(Params.TOOLBAR_TITLE, o.getShopName());
            bundle.putInt(Params.SHOP_ID, o.getId());
            MovementManager.startDetailsActivity(getContext(), Codes.SHOP_DETAILS_SCREEN, bundle);

        });
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == VISIBLE || result == GONE) {
            accessLoadAnimation(R.raw.location_find, result);
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        }
    }
}
