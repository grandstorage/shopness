package grand.shopness.view.ui.fragment.main.bottombar;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentCartBinding;
import grand.shopness.viewmodel.fragment.main.CartViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends BaseFragment implements Observer<Object> {

    private FragmentCartBinding binding;

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        binding.swipeRefresh.setColorSchemeResources(R.color.g_blue, R.color.g_red, R.color.g_yellow, R.color.g_green);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        CartViewModel viewModel = new CartViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        viewModel.getCartAdapter().getMutableLiveData().observe(this, o -> {
            showProgressBar(o);
            viewModel.onRefresh();
        });
    }

    @Override
    public void onChanged(Object o) {
        int res = (Integer) o;
        if (res == View.VISIBLE || res == View.GONE){
            showProgressAnimation(res);
        }else if (res == Codes.CHECKOUT_SCREEN){
            Bundle bundle = new Bundle();
            bundle.putString(Params.TOOLBAR_TITLE, getString(R.string.checkout));
            MovementManager.startDetailsActivity(getContext(), Codes.CHECKOUT_SCREEN, bundle);
        }
    }
}
