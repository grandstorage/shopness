package grand.shopness.view.ui.fragment.main.sidemenu;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentSuggestionBinding;
import grand.shopness.view.ui.fragment.main.bottombar.MainShopsFragment;
import grand.shopness.viewmodel.fragment.main.SuggestionViewModel;

/**
 * A simple {@link Fragment} subclass.
 */

public class SuggestionFragment extends BaseFragment implements Observer<Object> {

    FragmentSuggestionBinding suggestionBinding;
    SuggestionViewModel viewModel;

    public SuggestionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        suggestionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_suggestion, container, false);
        viewModel = new SuggestionViewModel();
        suggestionBinding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        return suggestionBinding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }
        else if (result == Codes.SHOW_MESSAGE){
            showMessage(viewModel.getMessage());
        }else if(result == Codes.HOME_SCREEN){
            MovementManager.replaceFragment(getContext(), new MainShopsFragment(),"");
        }

    }
}
