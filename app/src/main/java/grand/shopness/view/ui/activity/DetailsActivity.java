package grand.shopness.view.ui.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.button.MaterialButton;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.ParentActivity;
import grand.shopness.databinding.ActivityDetailsBinding;
import grand.shopness.view.ui.fragment.login.MapsFragment;
import grand.shopness.view.ui.fragment.main.ProductDetailFragment;
import grand.shopness.view.ui.fragment.main.ReviewsFragment;
import grand.shopness.view.ui.fragment.main.ShopDetailsFragment;
import grand.shopness.view.ui.fragment.main.ShopsFragment;
import grand.shopness.view.ui.fragment.main.shipping.CheckoutFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.accounts.ChangePasswordFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.accounts.OrderDetailsFragment;
import grand.shopness.viewmodel.activity.DetailsActivityViewModel;
import timber.log.Timber;

public class DetailsActivity extends ParentActivity implements Observer<Object> {
    private ActivityDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        setSupportActionBar(getToolbar());
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setViewModel();
        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE, 0));
        } else
            Timber.e("no fragment registered");
    }

    private void addFragment(int page) {
        Fragment fragment;
        if (page == Codes.SHOPS_FRAGMENT) {
            getSearchToolbarIcon().setVisibility(View.VISIBLE);
            getFilterToolbarIcon().setVisibility(View.GONE);
            fragment = new ShopsFragment();
        } else if (page == Codes.SHOP_DETAILS_SCREEN) {
            getSearchToolbarIcon().setVisibility(View.GONE);
            getFilterToolbarIcon().setVisibility(View.VISIBLE);
            fragment = new ShopDetailsFragment();
        } else if (page == Codes.MAP_SCREEN) {
            getSearchToolbarIcon().setVisibility(View.INVISIBLE);
            getFilterToolbarIcon().setVisibility(View.GONE);
            fragment = new MapsFragment();
        } else if (page == Codes.CHANGE_PASSWORD_SCREEN) {
            getToolbarTitle().setText(getString(R.string.change_password));
            getSearchToolbarIcon().setVisibility(View.INVISIBLE);
            getFilterToolbarIcon().setVisibility(View.GONE);
            fragment = new ChangePasswordFragment();
        } else if (page == Codes.REVIEWS_SCREEN) {
            getToolbarTitle().setText(getString(R.string.reviews));
            getSearchToolbarIcon().setVisibility(View.INVISIBLE);
            getFilterToolbarIcon().setVisibility(View.GONE);
            fragment = new ReviewsFragment();
        } else if (page == Codes.PRODUCT_DETAILS) {
            getToolbarTitle().setText(getString(R.string.product_details));
            getToolbar().setVisibility(View.GONE);
            fragment = new ProductDetailFragment();
        } else if (page == Codes.CHECKOUT_SCREEN) {
            getToolbarTitle().setText(getString(R.string.checkout));
            getSearchToolbarIcon().setVisibility(View.INVISIBLE);
            getFilterToolbarIcon().setVisibility(View.GONE);
            fragment = new CheckoutFragment();
        } else if (page == Codes.ORDER_DETAILS) {
            getToolbarTitle().setText(getString(R.string.order_details));
            getSearchToolbarIcon().setVisibility(View.INVISIBLE);
            getFilterToolbarIcon().setVisibility(View.GONE);
            fragment = new OrderDetailsFragment();
        } else {
            getSearchToolbarIcon().setVisibility(View.VISIBLE);
            getSearchToolbarIcon().setVisibility(View.GONE);
            fragment = new ShopsFragment();
        }

        fragment.setArguments(getIntent().getBundleExtra(Params.BUNDLE_PAGE));
        try {
            getToolbarTitle().setText(Objects.requireNonNull(getIntent().getBundleExtra(Params.BUNDLE_PAGE)).getString(Params.TOOLBAR_TITLE));
        } catch (Exception e) {
            Timber.e(e);
        }
        MovementManager.replaceFragment(this, fragment, "");
    }

    private void setViewModel() {
        DetailsActivityViewModel viewModel = new DetailsActivityViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
    }

    public ProgressBar getProgressBar() {
        return binding.pbDetails;
    }

    public Toolbar getToolbar() {
        return binding.toolbar;
    }

    public LottieAnimationView getAnimationView() {
        return binding.animView;
    }

    public TextView getToolbarTitle() {
        return binding.tvToolbarTitle;
    }

    public MaterialButton getSearchToolbarIcon(){
        return binding.btnToolbarSearch;
    }

    public MaterialButton getFilterToolbarIcon(){
        return binding.btnToolbarFilter;
    }



    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }


    @Override
    public void onChanged(Object o) {
        //mutable live data changes
    }
}
