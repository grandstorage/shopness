package grand.shopness.view.ui.fragment.main.sidemenu.accounts;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayoutMediator;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.BasePagerAdapter;
import grand.shopness.databinding.FragmentAccountsBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountsFragment extends Fragment {

    private FragmentAccountsBinding binding;
    private ArrayList<Fragment> list = new ArrayList<>();

    public AccountsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list.add(new ProfileFragment());
        list.add(new MyOrdersFragment());
        list.add(new WishlistFragment());
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_accounts, container, false);
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        initViewPager();
    }

    private void initViewPager() {
        binding.viewPager.setAdapter(new BasePagerAdapter(Objects.requireNonNull(getChildFragmentManager()), getLifecycle(), list));
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(binding.tabLayout, binding.viewPager, true,
                (tab, position) -> {
                    // position of the current tab and that tab
                    switch (position) {
                        case 0:
                            tab.setText(getString(R.string.profile));
                            break;
                        case 1:
                            tab.setText(getString(R.string.my_orders));
                            break;
                        case 2:
                            tab.setText(getString(R.string.wishlist));
                            break;
                    }
                });
        tabLayoutMediator.attach();
        binding.viewPager.setUserInputEnabled(false);
    }
}
