package grand.shopness.view.ui.fragment.main;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.tabs.TabLayoutMediator;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentShopsBinding;
import grand.shopness.base.BasePagerAdapter;

import static android.app.Activity.RESULT_OK;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopsFragment extends BaseFragment {

    private FragmentShopsBinding binding;
    private ArrayList<Fragment> list = new ArrayList<>();

    public ShopsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list.add(new AllShopsFragment());
        list.add(new NearestShopsFragment());
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shops, container, false);
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        checkForGPSEnabled();
    }

    private void initViewPager() {
        binding.viewPager.setAdapter(new BasePagerAdapter(Objects.requireNonNull(getChildFragmentManager()), getLifecycle(),
                list));
        // position of the current tab and that tab
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(binding.tabLayout, binding.viewPager, false,
                (tab, position) -> {
                    // position of the current tab and that tab
                    switch (position) {
                        case 0:
                            tab.setText(getString(R.string.all_shops));
                            break;
                        case 1:
                            tab.setText(getString(R.string.nearest_shops));
                            break;
                    }
                });
        tabLayoutMediator.attach();
    }

    private void checkForGPSEnabled() {
        if (isGPSEnabled()) {
            if (hasPermission()) {
                initViewPager();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Codes.ALERT_DIALOGUE_LOCATION_REQ:
                if (resultCode == RESULT_OK) {
                    if (hasPermission()) {
                        initViewPager();
                    }
                }
                break;
            case Codes.ALERT_DIALOGUE_GPS_REQ:
                if (resultCode == RESULT_OK) {
                    MovementManager.openGPSSetting(this);
                }
                break;
            case Codes.GPS_SETTINGS_REQ_CODE:
                //if result is ok or not we also make a check
                if (isGPSEnabled()) {
                    if (hasPermission()) {
                        initViewPager();
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Codes.PERMISSION_LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                initViewPager();
            } else {
                //permission denied
                showLocationPermissionAlertDialogue();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


}
