package grand.shopness.view.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.button.MaterialButton;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseBottomNavigationView;
import grand.shopness.base.view.ParentActivity;
import grand.shopness.databinding.ActivityMainBinding;
import grand.shopness.databinding.NavHeaderMainBinding;
import grand.shopness.view.ui.fragment.login.MapsFragment;
import grand.shopness.view.ui.fragment.main.ReviewsFragment;
import grand.shopness.view.ui.fragment.main.ShopDetailsFragment;
import grand.shopness.view.ui.fragment.main.ShopsFragment;
import grand.shopness.view.ui.fragment.main.bottombar.MainShopsFragment;
import grand.shopness.view.ui.fragment.main.sidemenu.accounts.ChangePasswordFragment;
import grand.shopness.viewmodel.activity.MainActivityViewModel;
import timber.log.Timber;

public class MainActivity extends ParentActivity implements Observer<Object> {

    private ActionBarDrawerToggle mToggle;
    private ActivityMainBinding activityMainBinding;
    private NavHeaderMainBinding headerBinding;
    private DrawerLayout mDrawerLayout;
    private LottieAnimationView animationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initUI();
        settingViewModel();
        settingNavDrawer();

        if (getIntent().hasExtra(Params.INTENT_PAGE)) {
            addFragment(getIntent().getIntExtra(Params.INTENT_PAGE, 0));
        } else
            Timber.e("no fragment registered");
    }

    private void addFragment(int page) {
        Fragment fragment;
        if (page == Codes.MAP_SCREEN) {
            fragment = new MapsFragment();
        } else if (page == Codes.CHANGE_PASSWORD_SCREEN) {
            fragment = new ChangePasswordFragment();
        } else {
            getToolBarTitle().setText(getString(R.string.app_name));
            fragment = new MainShopsFragment();
        }

        fragment.setArguments(getIntent().getBundleExtra(Params.BUNDLE_PAGE));
        MovementManager.replaceFragment(this, fragment, "");
    }

    private void settingViewModel() {
        MainActivityViewModel mainActivityViewModel = new MainActivityViewModel();
        activityMainBinding.setViewModel(mainActivityViewModel);
        mainActivityViewModel.getMutableLiveData().observe(this, this);
        headerBinding = NavHeaderMainBinding.bind(activityMainBinding.navView.getHeaderView(0));
        headerBinding.setViewModel(mainActivityViewModel);
    }

    private void initUI() {
        mDrawerLayout = activityMainBinding.drawerLayout;
        animationView = activityMainBinding.appBarMain.contentMain.animView;
    }

    public ProgressBar getProgressBar() {
        return activityMainBinding.appBarMain.contentMain.pbMain;
    }




    public LottieAnimationView getAnimationView() {
        return animationView;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
            Objects.requireNonNull(fragment).onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.getStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void settingNavDrawer() {
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, getToolbar(), R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mToggle.setDrawerIndicatorEnabled(false);
        mToggle.setHomeAsUpIndicator(R.drawable.ic_burger_menu);
        mToggle.setToolbarNavigationClickListener(view -> mDrawerLayout.openDrawer(GravityCompat.START));
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    public TextView getToolBarTitle() {
        return activityMainBinding.appBarMain.toolbarTitle;
    }

    public MaterialButton getMainSearchBtn(){
        return activityMainBinding.appBarMain.btnToolbar;
    }

    public Toolbar getToolbar(){
        return activityMainBinding.appBarMain.toolbar;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mToggle.syncState();
    }

    @Override
    public void onChanged(Object obj) {
        int result = (Integer) obj;
        switch (result) {
            case Codes.HOME_SCREEN:
                break;
            case Codes.TOOLBAR_SEARCH_CLICK:
                showMessage("search");
                break;
        }
    }
}
