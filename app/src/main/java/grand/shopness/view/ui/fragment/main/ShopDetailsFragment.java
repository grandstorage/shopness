package grand.shopness.view.ui.fragment.main;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentShopDetailsBinding;
import grand.shopness.model.shops.details.ProductsItem;
import grand.shopness.viewmodel.fragment.main.ShopDetailsViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopDetailsFragment extends BaseFragment implements Observer<Object> {

    private FragmentShopDetailsBinding binding;
    private ShopDetailsViewModel viewModel;

    public ShopDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_details, container, false);
        binding.swipeRefresh.setColorSchemeResources(R.color.g_blue, R.color.g_red, R.color.g_yellow, R.color.g_green);
        settingViewModel();
        initUI();
        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new ShopDetailsViewModel(getArguments().getInt(Params.SHOP_ID));
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        viewModel.getCategoryAdapter().getMutableLiveData().observe(this, o -> {
            //on category click
            viewModel.getProductsData(o.getId());
        });
        viewModel.getProductAdapter().getMutableLiveData().observe(this, o -> {
            //on product
            if (o instanceof Integer) {
                int result = (Integer) o;
                if (result == View.VISIBLE || result == View.GONE) {
                    showProgressBar(result);
                } else if (result == Codes.SHOW_MESSAGE) {
                    showMessage(viewModel.getProductAdapter().getMessage());

                }
            } else if (o instanceof ProductsItem) {
                Bundle bundle = new Bundle();
                bundle.putInt(Params.PRODUCT_ID, ((ProductsItem) o).getId());
                bundle.putInt(Params.IS_CLOTHES, ((ProductsItem) o).getIsClothes());
                MovementManager.startDetailsActivity(getContext(), Codes.PRODUCT_DETAILS, bundle);
            }
        });
    }

    private void initUI() {

    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
//            showProgressBar(result);
            showProgressAnimation(result);
        } else if (result == Codes.REVIEWS_SCREEN) {
            Bundle bundle = new Bundle();
            bundle.putInt(Params.SHOP_ID, viewModel.getShopDetailsItem().getId());
            MovementManager.startDetailsActivity(getContext(), Codes.REVIEWS_SCREEN, bundle);
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        }


    }
}
