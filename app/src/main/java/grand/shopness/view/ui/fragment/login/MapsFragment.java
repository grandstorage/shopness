package grand.shopness.view.ui.fragment.login;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentMapsBinding;
import grand.shopness.util.MapsUtily;
import grand.shopness.viewmodel.fragment.login.MapsFragmentViewModel;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends BaseFragment implements OnMapReadyCallback, Observer<Object>, GoogleMap.OnCameraIdleListener {

    private LatLng latLong = null;
    private double currentLat = 0, currentLong = 0;
    private SupportMapFragment mapFragment;
    FragmentMapsBinding mapsBinding;
    MapsFragmentViewModel mapsFragmentViewModel;
    private GoogleMap mMap;
    private Disposable disposable;
    private ReactiveLocationProvider locationProvider;
    private Marker currentMarker;

    public MapsFragment() {
        // Required empty public constructor
    }

    private void initGMap() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null && getFragmentManager() != null) {
            mapFragment = SupportMapFragment.newInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.map, mapFragment).commit();
        }
    }

    private void checkForGPSEnabled() {
        if (isGPSEnabled()) {
            if (hasPermission()) {
                mapFragment.getMapAsync(this);
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationProvider = new ReactiveLocationProvider(getContext());
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mapsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_maps, container, false);
        mapsFragmentViewModel = new MapsFragmentViewModel();
        mapsFragmentViewModel.getMutableLiveData().observe(this, this);
        mapsBinding.setViewModel(mapsFragmentViewModel);
        initGMap();
        return mapsBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startMap();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Codes.ALERT_DIALOGUE_LOCATION_REQ:
                if (resultCode == RESULT_OK) {
                    if (hasPermission()) {
                        mapFragment.getMapAsync(this);
                    }
                }
                break;
            case Codes.ALERT_DIALOGUE_GPS_REQ:
                if (resultCode == RESULT_OK) {
                    MovementManager.openGPSSetting(this);
                }
                break;
            case Codes.GPS_SETTINGS_REQ_CODE:
                //if result is ok or not we also make a check
                if (isGPSEnabled()) {
                    if (hasPermission()) {
                        mapFragment.getMapAsync(this);
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Codes.PERMISSION_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    mapFragment.getMapAsync(this);
                } else {
                    //permission denied
                    showLocationPermissionAlertDialogue();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onStop() {
        if (disposable != null)
            disposable.dispose();
        super.onStop();
    }

    private void startMap() {
        checkForGPSEnabled();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setCurrentLocationMarker();
        MapsUtily.getInstance(mMap).showCurrentLocationBtn(true);
        MapsUtily.getInstance(mMap).showZoomBtn(true);
        MapsUtily.getInstance(mMap).showCompassBtn(true);

        mMap.setOnCameraIdleListener(this);

        mapsBinding.btnSave.setOnClickListener(view -> {
            Intent n = new Intent();
            n.putExtra("lat", latLong.latitude);
            n.putExtra("lng", latLong.longitude);
            n.putExtra("address", getString(R.string.add_address_details));
            Objects.requireNonNull(getActivity()).setResult(Codes.BUNDLE_RESULT_CODE, n);
            getActivity().finish();
        });

    }


    @SuppressLint("MissingPermission")
    private void setCurrentLocationMarker() {
        if (currentLat == 0 && currentLong == 0) {
            disposable = locationProvider.getLastKnownLocation()
                    .subscribe(location -> {
                        Timber.e("location: %s", location);
                        latLong = new LatLng(location.getLatitude(), location.getLongitude());
                        setCurrentMarkerNow("", latLong);
                    });
        }
    }


    private boolean firstAnimate = false;

    private void setCurrentMarkerNow(String title, LatLng location) {
        mMap.clear();
        MarkerOptions options = new MarkerOptions();
        options.title(title)
                .position(location);
        if (!firstAnimate) {
            firstAnimate = true;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    location, // coordinates
                    17); // zoom
            mMap.animateCamera(cameraUpdate);
        }
        currentMarker = mMap.addMarker(options);
        mapsBinding.cardView.setVisibility(View.VISIBLE);
        mapsBinding.addressText.setText(String.format("%s%s\n%s%s",
                getString(R.string.lat), location.latitude,
                getString(R.string.lon), location.longitude));

    }

    @Override
    public void onChanged(Object o) {
        //mutable live data
    }


    @Override
    public void onCameraIdle() {
        if (mMap != null && !mMap.getCameraPosition().target.equals(latLong)) {
//            getAddress(locationProvider, mMap.getCameraPosition().target);
            setCurrentMarkerNow("", mMap.getCameraPosition().target);
            latLong = mMap.getCameraPosition().target;
        }

    }
}
