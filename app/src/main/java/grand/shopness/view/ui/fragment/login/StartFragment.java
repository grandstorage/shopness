package grand.shopness.view.ui.fragment.login;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentStartBinding;
import grand.shopness.model.startfrag.StartScreenHandler;
import grand.shopness.view.ui.activity.MainActivity;
import grand.shopness.viewmodel.StartScreenViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartFragment extends BaseFragment implements Observer<Object> {
    private FragmentStartBinding binding;
    private StartScreenHandler startScreenHandler;
    private StartScreenViewModel startScreenViewModel;

    public StartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start, container, false);
        startScreenViewModel = new StartScreenViewModel();
        startScreenViewModel.getMutableLiveData().observe(this, this);
        binding.setViewModel(startScreenViewModel);
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        binding.tvWelcome.setText(getString(R.string.welcome_to));
        binding.tvShopness.setText(getString(R.string.app_name));
        binding.tvBio.setText(getString(R.string.bio_start_screen));
    }

    @Override
    public void onChanged(Object res) {
        if (res instanceof Integer) {
            switch ((Integer) res) {
                case Codes.HOME_SCREEN:
                    MovementManager.startMainActivity(getContext(), Codes.HOME_SCREEN);
                    break;
                case Codes.LOGIN_SCREEN:
                    MovementManager.startActivity(getContext(), Codes.LOGIN_SCREEN);
                    break;
            }
        }
    }

}
