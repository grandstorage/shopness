package grand.shopness.view.ui.activity;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.ParentActivity;
import grand.shopness.databinding.ActivitySplashScreenBinding;
import grand.shopness.viewmodel.SplashScreenViewModel;

public class SplashScreen extends ParentActivity implements Observer<Object> {
    ActivitySplashScreenBinding activitySplashScreenBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        settingViewModel();
    }

    private void settingViewModel() {
        SplashScreenViewModel splashScreenViewModel = new SplashScreenViewModel();
        splashScreenViewModel.startApp();
        splashScreenViewModel.getMutableLiveData().observe(this, this);
    }

    @Override
    public void onChanged(Object value) {
        if (((Integer) value) == Codes.HOME_SCREEN)
            MovementManager.startMainActivity(SplashScreen.this, (Integer) value);
        else
            MovementManager.startActivity(SplashScreen.this, (Integer) value);
        finish();
    }
}
