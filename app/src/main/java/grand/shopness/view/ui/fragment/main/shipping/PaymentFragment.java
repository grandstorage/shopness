package grand.shopness.view.ui.fragment.main.shipping;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.material.shape.RoundedCornerTreatment;
import com.google.android.material.shape.ShapeAppearanceModel;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentPaymentBinding;
import grand.shopness.model.payment.PaymentItem;
import grand.shopness.viewmodel.fragment.main.PaymentViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends BaseFragment implements Observer<Object> {

    FragmentPaymentBinding binding;
    PaymentViewModel viewModel;
    private int payId;
    CheckoutFragment fragment;
    private LinearLayout tab;

    public PaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert this.getParentFragment() != null;
        fragment = ((CheckoutFragment) this.getParentFragment());
        tab = ((LinearLayout) fragment.binding.tabLayout.getChildAt(0));
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false);
        initUI();
        settingViewModel();
       return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new PaymentViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        viewModel.getPaymentAdapter().getMutableLiveData().observe(this, paymentItem -> {
            UserPreferenceHelper.setPayIt(paymentItem.getId());
        });
    }

    private void initUI() {
        ShapeAppearanceModel shapePathModel = new ShapeAppearanceModel();
        shapePathModel.setBottomLeftCorner(new RoundedCornerTreatment(0));
        shapePathModel.setTopLeftCorner(new RoundedCornerTreatment(0));
        shapePathModel.setBottomRightCorner(new RoundedCornerTreatment(8));
        shapePathModel.setTopRightCorner(new RoundedCornerTreatment(8));
        binding.btnApply.setShapeAppearanceModel(shapePathModel);
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE){
            showProgressAnimation(result);
        }else if (result == Codes.CONFIRMATION_SCREEN){
            fragment.binding.viewPager.setCurrentItem(1,true);
            tab.getChildAt(1).setAlpha(1);
        }
    }
}
