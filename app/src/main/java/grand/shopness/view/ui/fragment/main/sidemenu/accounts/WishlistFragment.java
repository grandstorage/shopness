package grand.shopness.view.ui.fragment.main.sidemenu.accounts;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentWishlistBinding;
import grand.shopness.viewmodel.fragment.main.WishlistViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishlistFragment extends BaseFragment implements Observer<Object> {

    FragmentWishlistBinding binding;
    private WishlistViewModel viewModel;

    public WishlistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wishlist, container, false);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new WishlistViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        }
    }
}
