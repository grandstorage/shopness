package grand.shopness.view.ui.fragment.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.jaredrummler.materialspinner.MaterialSpinner;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.BaseSpinner;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentProductDetailBinding;
import grand.shopness.model.product.size.SizeItem;
import grand.shopness.viewmodel.fragment.main.ProductDetailViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends BaseFragment implements Observer<Object> {

    FragmentProductDetailBinding binding;
    ProductDetailViewModel viewModel;
    private int productId, isClothes;

    public ProductDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productId = getArguments().getInt(Params.PRODUCT_ID);
            isClothes = getArguments().getInt(Params.IS_CLOTHES);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_detail, container, false);
        settingSpinnerValue();
        settingViewModel();
        return binding.getRoot();
    }

    private void settingSpinnerValue() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add("" + (i + 1));
        }
        BaseSpinner.setMaterialSpinner(binding.spinner, list).setOnItemSelectedListener((view, position, id, item) -> {
            viewModel.getAddCartRequest().setQty(Integer.valueOf(item.toString()));
        });
    }

    private void settingViewModel() {
        viewModel = new ProductDetailViewModel(productId, isClothes);
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        viewModel.getSizesAdapter().getMutableLiveData().observe(this, o -> {
            //size mutable data
            if (o instanceof SizeItem)
                viewModel.setSizeItemId(((SizeItem) o).getId());
        });
        viewModel.getColorsAdapter().getMutableLiveData().observe(this, item -> {
            viewModel.setColorId(item.getId());
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onChanged(Object o) {
        //main live data
        int res = (Integer) o;
        if (res == View.VISIBLE || res == View.GONE) {
            showProgressAnimation(res);
        } else if (res == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        } else if (res == Codes.PRESS_BACK) {
            Objects.requireNonNull(getActivity()).finish();
        }
    }

}
