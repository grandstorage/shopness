package grand.shopness.view.ui.fragment.main.bottombar;


import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentMainShopsBinding;
import grand.shopness.viewmodel.fragment.main.MainShopsViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainShopsFragment extends BaseFragment implements Observer<Object> {

    private FragmentMainShopsBinding binding;
    private MainShopsViewModel shopsViewModel;

    public MainShopsFragment() {
        //Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_shops, container, false);
        setMainViewModel();
        return binding.getRoot();
    }

    private void setMainViewModel() {
        shopsViewModel = new MainShopsViewModel();
        binding.setViewModel(shopsViewModel);
        shopsViewModel.getMutableLiveData().observe(this, this);
        shopsViewModel.getCategoryAdapter().getMutableLiveData().observe(this, o -> {
            //on item category click
            UserPreferenceHelper.setServiceId(o.getId());
            Bundle bundle = new Bundle();
            bundle.putString(Params.TOOLBAR_TITLE, o.getServiceName());
            MovementManager.startDetailsActivity(getContext(), Codes.SHOPS_FRAGMENT, bundle);
        });
    }

    @Override
    public void onChanged(Object o) {
        if (o instanceof Integer) {
            int result = (Integer) o;
            if (result == View.VISIBLE || result == View.GONE) {
                showProgressAnimation(result);
            } else if (result == Codes.ON_ALL_CLICK) {
                //text all click
                MovementManager.startDetailsActivity(getContext(), Codes.SHOPS_FRAGMENT);
            }
        } else if (o instanceof SparseIntArray) {
            binding.indicator.setViewPager(binding.pager);
            binding.pager.setCurrentItem(((SparseIntArray) o).get(Codes.MOVE_SLIDER), true);
        }
    }

    @Override
    public void onResume() {
        getSearchIcon().setVisibility(View.VISIBLE);
        super.onResume();
    }


    @Override
    public void onPause() {
        getSearchIcon().setVisibility(View.INVISIBLE);
        super.onPause();
    }
}
