package grand.shopness.view.ui.fragment.main.shipping;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentConfirmationBinding;
import grand.shopness.viewmodel.fragment.main.ConfirmationViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmationFragment extends BaseFragment implements Observer<Object> {

    private FragmentConfirmationBinding binding;
    private ConfirmationViewModel viewModel;

    public ConfirmationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirmation, container, false);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new ConfirmationViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }
    }
}
