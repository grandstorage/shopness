package grand.shopness.view.ui.fragment.main.sidemenu.accounts;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentChangeAreaBinding;
import grand.shopness.databinding.FragmentChangePasswordBinding;
import grand.shopness.viewmodel.fragment.main.ChangePasswordViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends BaseFragment implements Observer<Object> {

    private FragmentChangePasswordBinding binding;
    private ChangePasswordViewModel viewModel;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        viewModel = new ChangePasswordViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        return binding.getRoot();
    }


    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressBar(result);
        }else if (result == Codes.BLOCK){
            binding.btnSave.setEnabled(false);
        }else if (result == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        }else if(result == Codes.CHANGE_PASSWORD_SCREEN){
            MovementManager.startMainActivity(getContext(), result);
        }else if(result == Codes.SELECT_PROFILE_IMAGE){
            pickImage(result);
        }else if (result == Codes.DONE){
            binding.btnSave.setEnabled(true);
        }
    }
}
