package grand.shopness.view.ui.fragment.main.sidemenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import grand.shopness.R;
import grand.shopness.base.BaseSpinner;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentChangeAreaBinding;

import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeAreaFragment extends BaseFragment implements OnMapReadyCallback {
    private FragmentChangeAreaBinding binding;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private boolean showNearestShops;

    public ChangeAreaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_area, container, false);

        initGMap();
        initUI();
        return binding.getRoot();
    }


    private void initUI() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Select distance");
        list.add("12 km");
        list.add("108 km");
        list.add("1145 km");
        BaseSpinner.setSpinner(binding.spinner, list, R.layout.single_item_spinner_change_area).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    showMessage(list.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initGMap() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null && getFragmentManager() != null) {
            mapFragment = SupportMapFragment.newInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.map, mapFragment).commit();
            mapFragment.getMapAsync(this);
//            checkForGPSEnabled();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}
