package grand.shopness.view.ui.fragment.main.sidemenu;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentAboutUsBinding;
import grand.shopness.viewmodel.fragment.main.AboutUsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends BaseFragment implements Observer<Object> {

    FragmentAboutUsBinding aboutUsBinding;
    AboutUsViewModel aboutUsViewModel;
    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        aboutUsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_us, container, false);
        AboutUsViewModel  aboutUsViewModel = new AboutUsViewModel();
        aboutUsBinding.setViewModel(aboutUsViewModel);
        aboutUsViewModel.getMutableLiveData().observe(this, this);
        return aboutUsBinding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }
    }
}
