package grand.shopness.view.ui.fragment.main.sidemenu;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MyClipBoardManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentPromoCodeBinding;
import grand.shopness.viewmodel.fragment.main.PromoCodeViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PromoCodeFragment extends BaseFragment implements Observer<Object> {

    FragmentPromoCodeBinding promoCodeBinding;
    PromoCodeViewModel codeViewModel;

    public PromoCodeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        promoCodeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_promo_code, container, false);
        codeViewModel = new PromoCodeViewModel();
        promoCodeBinding.setViewModel(codeViewModel);
        codeViewModel.getMutableLiveData().observe(this, this);
        return promoCodeBinding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }

        switch (result) {
            case WebServices.SUCCESS:
                promoCodeBinding.tvData.setText(getString(R.string.long_text));
                break;

            case Codes.SAVE_TO_SHARED_PREFERENCE:
                MyClipBoardManager.getInstance(getContext()).copyToClipboard(promoCodeBinding.tvCode.getText().toString());
                showMessage(getString(R.string.copied));
                break;
        }

    }
}
