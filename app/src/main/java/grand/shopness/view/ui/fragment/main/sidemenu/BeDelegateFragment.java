package grand.shopness.view.ui.fragment.main.sidemenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.FileOperations;
import grand.shopness.base.filesutils.VolleyFileObject;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentBeDelegateBinding;
import grand.shopness.viewmodel.fragment.main.BeDelegateViewModel;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeDelegateFragment extends BaseFragment implements Observer<Object> {

    FragmentBeDelegateBinding delegateBinding;
    BeDelegateViewModel viewModel;

    public BeDelegateFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        delegateBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_be_delegate, container, false);
        viewModel = new BeDelegateViewModel();
        delegateBinding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);

        return delegateBinding.getRoot();
    }


    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressBar(result);
        } else if (result == Codes.SELECT_PROFILE_IMAGE) {
            pickImage(Codes.SELECT_PROFILE_IMAGE);
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        } else if (result == Codes.ON_LICENSE_CLICK) {
            pickImage(Codes.ON_LICENSE_CLICK);
        } else if (result == Codes.ON_NATIONAL_ID_CLICK) {
            pickImage(Codes.ON_NATIONAL_ID_CLICK);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Codes.PERMISSION_IMAGE_REQUEST_CODE:
                    if (grantResults[0] == PERMISSION_GRANTED) {
                        //not need
//                        FileOperations.pickImage(getActivity(), requestCode);
                    }
                    break;
            }//end switch
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Codes.SELECT_PROFILE_IMAGE:
                addToVolleyObj(data, Codes.SELECT_PROFILE_IMAGE);
                break;
            case Codes.ON_LICENSE_CLICK:
                addToVolleyObj(data, Codes.ON_LICENSE_CLICK);
                break;
            case Codes.ON_NATIONAL_ID_CLICK:
                addToVolleyObj(data, Codes.ON_NATIONAL_ID_CLICK);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addToVolleyObj(Intent data, int reqCode) {
        VolleyFileObject volleyFileObject = null;
        if (reqCode == Codes.SELECT_PROFILE_IMAGE) {
            volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.USER_IMAGE, Codes.TYPE_IMAGE_REQUEST_CODE);
            delegateBinding.imgProfile.setImageBitmap(Objects.requireNonNull(volleyFileObject).getCompressObject().getImage());
        }
        else if(reqCode == Codes.ON_LICENSE_CLICK){
            volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.License, Codes.TYPE_IMAGE_REQUEST_CODE);
            delegateBinding.etLicense.setText(volleyFileObject.getFilePath());
        }else if(reqCode == Codes.ON_NATIONAL_ID_CLICK){
            volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.DELEGATE_ID, Codes.TYPE_IMAGE_REQUEST_CODE);
            delegateBinding.etNationalId.setText(volleyFileObject.getFilePath());
        }
        viewModel.getVolleyFileObjects().add(volleyFileObject);
    }
}
