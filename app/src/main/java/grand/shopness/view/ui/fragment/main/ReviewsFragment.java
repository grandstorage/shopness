package grand.shopness.view.ui.fragment.main;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentReviewsBinding;
import grand.shopness.viewmodel.fragment.main.ReviewsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends BaseFragment implements Observer<Object> {
    FragmentReviewsBinding reviewsBinding;
    private ReviewsViewModel reviewsViewModel;
    private int view;

    public ReviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        reviewsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reviews, container, false);
        settingViewModel();
        reviewsBinding.tilWriteComment.setEndIconOnClickListener(v -> {
            if (!TextUtils.isEmpty(reviewsBinding.etWriteComment.getText().toString().trim())) {
                reviewsViewModel.onSendClick();
            } else {
                YoYo.with(Techniques.Shake).playOn(reviewsBinding.tilWriteComment);
            }

        });
        return reviewsBinding.getRoot();
    }

    private void settingViewModel() {
        reviewsViewModel = new ReviewsViewModel(getArguments().getInt(Params.SHOP_ID));
        reviewsBinding.setViewModel(reviewsViewModel);
        reviewsViewModel.getMutableLiveData().observe(this, this);
        reviewsViewModel.getReviewsAdapter().getMutableLiveData().observe(this, o -> {
            //on item click
        });
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            view = result;
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(reviewsViewModel.getMessage());
        }
        if (result == Codes.ANIM) {
            accessLoadAnimation(R.raw.send_comment_anim, view);
        } else {
            showProgressAnimation(view);
        }
    }
}
