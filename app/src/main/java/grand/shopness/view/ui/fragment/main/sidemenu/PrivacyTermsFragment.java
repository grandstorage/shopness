package grand.shopness.view.ui.fragment.main.sidemenu;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentPrivacyTermsBinding;
import grand.shopness.viewmodel.fragment.main.PrivacyTermsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyTermsFragment extends BaseFragment implements Observer<Object> {

    FragmentPrivacyTermsBinding fragmentPrivacyTermsBinding;
    PrivacyTermsViewModel privacyTermsViewModel;

    public PrivacyTermsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentPrivacyTermsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_privacy_terms, container, false);
        PrivacyTermsViewModel privacyTermsViewModel = new PrivacyTermsViewModel();
        fragmentPrivacyTermsBinding.setViewModel(privacyTermsViewModel);
        privacyTermsViewModel.getMutableLiveData().observe(this, this);
        return fragmentPrivacyTermsBinding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }


    }

}
