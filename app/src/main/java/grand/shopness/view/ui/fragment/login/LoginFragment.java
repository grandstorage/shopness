package grand.shopness.view.ui.fragment.login;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentLoginBinding;
import grand.shopness.view.ui.activity.MainActivity;
import grand.shopness.viewmodel.fragment.login.LoginViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements Observer<Object> {

    private FragmentLoginBinding binding;
    private LoginViewModel loginViewModel;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        loginViewModel = new LoginViewModel();
        binding.setViewModel(loginViewModel);
        loginViewModel.getMutableLiveData().observe(this, this);
        return binding.getRoot();
    }

    @Override
    public void onChanged(Object result) {
        int resultInt = (int) result;
        if (resultInt == View.VISIBLE || resultInt == View.GONE) {
            showProgressBar(resultInt);
        }
        switch (resultInt) {
            case Codes.PRESS_SKIP:
                //go to main
                MovementManager.startMainActivity(getContext(), Codes.HOME_SCREEN);
                break;
            case Codes.FORGOT_PASSWORD_SCREEN:
                UserPreferenceHelper.setForgotPass(true);
                MovementManager.startActivity(getContext(),Codes.FORGOT_PASSWORD_SCREEN );
                break;
            case Codes.HOME_SCREEN:
                //go to main
                MovementManager.startMainActivity(getContext(), Codes.HOME_SCREEN);
                break;
            case Codes.ENTER_CODE_SCREEN:
                MovementManager.startActivity(getContext(),Codes.ENTER_CODE_SCREEN );
                break;
            case Codes.REGISTER_SCREEN:
                UserPreferenceHelper.setForgotPass(false);
                MovementManager.startActivity(getContext(),Codes.REGISTER_SCREEN );
                break;
            case Codes.SHOW_MESSAGE:
                showMessage(loginViewModel.getMessage());
                break;
        }
    }
}
