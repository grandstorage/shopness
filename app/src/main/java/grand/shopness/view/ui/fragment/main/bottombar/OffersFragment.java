package grand.shopness.view.ui.fragment.main.bottombar;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentOffersBinding;
import grand.shopness.viewmodel.fragment.main.OffersViewModel;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class OffersFragment extends BaseFragment implements Observer<Object> {

    FragmentOffersBinding binding;
    OffersViewModel viewModel;
    private ReactiveLocationProvider locationProvider;

    public OffersFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationProvider = new ReactiveLocationProvider(getContext());
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false);
        settingViewModel();
        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new OffersViewModel();
        binding.setViewModel(viewModel);
        viewModel.setLocationProvider(locationProvider);
        viewModel.startPopulateData(); // must set here
        viewModel.getMutableLiveData().observe(this, this);
    }

    @Override
    public void onChanged(Object o) {
        //mutable live data
        int res = ((Integer) o);
        if (res == VISIBLE || res == GONE){
            showProgressAnimation(res);
        }else if(Codes.SHOW_MESSAGE == res){
            showMessage(viewModel.getMessage());
        }
    }
}
