package grand.shopness.view.ui.fragment.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentAllShopsBinding;
import grand.shopness.viewmodel.fragment.main.AllShopsViewModel;
import io.reactivex.disposables.Disposable;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllShopsFragment extends BaseFragment implements Observer<Object> {

    private FragmentAllShopsBinding fragmentAllShopsBinding;
    private AllShopsViewModel allShopsViewModel;
    private ReactiveLocationProvider locationProvider;

    public AllShopsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationProvider = new ReactiveLocationProvider(getContext());
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentAllShopsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_all_shops, container, false);
        fragmentAllShopsBinding.swipeRefresh.setColorSchemeResources(R.color.g_blue, R.color.g_red, R.color.g_yellow, R.color.g_green);
        settingViewModel();
        return fragmentAllShopsBinding.getRoot();
    }

    private void settingViewModel() {
        allShopsViewModel = new AllShopsViewModel();
        fragmentAllShopsBinding.setAllShopsViewModel(allShopsViewModel);
        allShopsViewModel.setLocationProvider(locationProvider);
        allShopsViewModel.startPopulateData(); // must set here
        allShopsViewModel.getMutableLiveData().observe(this, this);
        allShopsViewModel.getAllShopsAdapter().mutableLiveData.observe(this, o -> {
            //on item click
            Bundle bundle = new Bundle();
            bundle.putString(Params.TOOLBAR_TITLE, o.getShopName());
            bundle.putInt(Params.SHOP_ID, o.getId());
            MovementManager.startDetailsActivity(getContext(), Codes.SHOP_DETAILS_SCREEN, bundle);

        });
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == VISIBLE || result == GONE) {
            accessLoadAnimation(R.raw.location_find, result);
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(allShopsViewModel.getMessage());
        }
    }
}
