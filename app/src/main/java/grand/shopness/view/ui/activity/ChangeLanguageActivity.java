package grand.shopness.view.ui.activity;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import grand.shopness.R;
import grand.shopness.application.MyApplication;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.view.ParentActivity;
import grand.shopness.databinding.ActivityChangeLanguageBinding;
import timber.log.Timber;

public class ChangeLanguageActivity extends ParentActivity {

    ActivityChangeLanguageBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding =   DataBindingUtil.setContentView(this, R.layout.activity_change_language);

       binding.btnAr.setOnClickListener(v->{
           if(!UserPreferenceHelper.getCurrentLanguage().equals("ar")) {
               Timber.e("ss");
               UserPreferenceHelper.setLanguage(MyApplication.getInstance().getApplicationContext(), "ar");
               MovementManager.startActivity(this, SplashScreen.class);
           }
           Timber.e("ar");
           finish();
       });

        binding.btnEn.setOnClickListener(v->{
            if(!UserPreferenceHelper.getCurrentLanguage().equals("en")) {
                Timber.e("sssss");
                UserPreferenceHelper.setLanguage(MyApplication.getInstance().getApplicationContext(), "en");
                MovementManager.startActivity(this, SplashScreen.class);
            }
            Timber.e("en");
            finish();
        });

        binding.llRoot.setOnClickListener(v->{
            Timber.e("here");
            finish();
        });


    }
}
