package grand.shopness.view.ui.fragment.main.sidemenu.accounts;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentMyOrdersBinding;
import grand.shopness.model.myorders.MyOrderItem;
import grand.shopness.viewmodel.fragment.main.MyOrdersViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrdersFragment extends BaseFragment implements Observer<Object> {

    private FragmentMyOrdersBinding binding;
    private MyOrdersViewModel viewModel;
    public MyOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);
        settingViewModel();

        return binding.getRoot();
    }

    private void settingViewModel() {
        viewModel = new MyOrdersViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        viewModel.getMyOrdersAdapter().getMutableLiveData().observe(this, myOrderItem -> {
            Bundle bundle = new Bundle();
            bundle.putInt(Params.ORDER_ID, myOrderItem.getId());
            MovementManager.startDetailsActivity(getContext(), Codes.ORDER_DETAILS, bundle);
        });

    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE){
            showProgressAnimation(result);
        }
    }
}
