package grand.shopness.view.ui.fragment.main.shipping;


import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.view.BaseActivity;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentShippingBinding;
import grand.shopness.viewmodel.fragment.main.ShippingViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShippingFragment extends BaseFragment implements Observer<Object> {

    FragmentShippingBinding binding;
    private ShippingViewModel viewModel;
    CheckoutFragment fragment;
    private LinearLayout tab;
    private double lat, lng;

    public ShippingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shipping, container, false);
        settingViewModel();
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        assert this.getParentFragment() != null;
        fragment = ((CheckoutFragment) this.getParentFragment());
        tab = ((LinearLayout) fragment.binding.tabLayout.getChildAt(0));
        tab.getChildAt(1).setAlpha(.5f);
        tab.getChildAt(2).setAlpha(.5f);

        binding.tilAddress.setEndIconOnClickListener(v -> openMapScreen());
    }

    private void settingViewModel() {
        viewModel = new ShippingViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
    }

    private void openMapScreen() {
        Intent intent = new Intent(getContext(), BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE, Codes.MAP_SCREEN);
        startActivityForResult(intent, Codes.BUNDLE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Codes.BUNDLE_RESULT_CODE) {
            setData(data);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void setData(Intent data) {
        if (data != null) {
            lat = data.getDoubleExtra("lat", 0);
            lng = data.getDoubleExtra("lng", 0);
            viewModel.getShippingRequest().setLat(lat);
            viewModel.getShippingRequest().setLng(lng);
            Timber.e(lat + " " + lng);
        }

    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressAnimation(result);
        }else if (result == Codes.PAYMENT_SCREEN){
            fragment.binding.viewPager.setCurrentItem(1,true);
            tab.getChildAt(1).setAlpha(1);
        }
    }

}
