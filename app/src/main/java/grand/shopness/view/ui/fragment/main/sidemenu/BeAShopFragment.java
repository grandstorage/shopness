package grand.shopness.view.ui.fragment.main.sidemenu;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.interfaces.OnHomeBackBtnCallBack;
import grand.shopness.view.ui.fragment.main.ShopsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeAShopFragment extends BaseFragment implements OnHomeBackBtnCallBack {


    public BeAShopFragment() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onHomeBtnBackClick() {
        MovementManager.replaceFragment(getContext(), new ShopsFragment(), "");
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_be_ashop, container, false);
    }

}
