package grand.shopness.view.ui.fragment.main.shipping;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayoutMediator;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.BasePagerAdapter;
import grand.shopness.databinding.FragmentCheckoutBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckoutFragment extends Fragment {

    FragmentCheckoutBinding binding;
    private ArrayList<Fragment> list = new ArrayList<>();

    public CheckoutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list.add(new ShippingFragment());
        list.add(new PaymentFragment());
        list.add(new ConfirmationFragment());
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkout, container, false);
        initUI();
        initViewPager();
        return binding.getRoot();
    }

    private void initUI() {
        binding.viewPager.setUserInputEnabled(false);
    }

    private void initViewPager() {
        binding.viewPager.setAdapter(new BasePagerAdapter(Objects.requireNonNull(getChildFragmentManager()),
                getLifecycle(), list));
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(binding.tabLayout, binding.viewPager, true,
                (tab, position) -> {
                    // position of the current tab and that tab
                    switch (position) {
                        case 0:
                            tab.setText(getString(R.string.shipping));
                            break;
                        case 1:
                            tab.setText(getString(R.string.payment));
                            break;
                        case 2:
                            tab.setText(getString(R.string.confirmation));
                            break;
                    }
                });
        tabLayoutMediator.attach();
        LinearLayout tabStrip = ((LinearLayout)binding.tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }

    }

}
