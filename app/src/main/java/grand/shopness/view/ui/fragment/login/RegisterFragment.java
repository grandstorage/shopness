package grand.shopness.view.ui.fragment.login;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.FileOperations;
import grand.shopness.base.filesutils.VolleyFileObject;
import grand.shopness.base.view.BaseActivity;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentRegisterBinding;
import grand.shopness.view.ui.activity.DialogActivity;
import grand.shopness.viewmodel.fragment.login.RegisterViewModel;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends BaseFragment implements Observer<Object> {

    FragmentRegisterBinding binding;
    private RegisterViewModel viewModel;
    private String address;
    private double lat, lng;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        viewModel = new RegisterViewModel();
        binding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        binding.tilLocation.setEndIconOnClickListener(v -> openMapScreen());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Codes.SELECT_PROFILE_IMAGE) {
            addToVolleyObj(data);
        } else if (requestCode == Codes.ALERT_DIALOGUE_LOCATION_REQ) {
            if (resultCode == RESULT_OK) {
                Timber.d("result ok");
            } else if (resultCode == RESULT_CANCELED) {
                Timber.d("result cancel");
            }
        } else if (requestCode == Codes.BUNDLE_RESULT_CODE) {
            setData(data);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void addToVolleyObj(Intent data) {
        VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.USER_IMAGE, Codes.TYPE_IMAGE_REQUEST_CODE);
        binding.imgProfile.setImageBitmap(Objects.requireNonNull(volleyFileObject).getCompressObject().getImage());
        viewModel.getVolleyFileObjects().add(volleyFileObject);
    }

    private void setData(Intent data) {
        if (data != null) {
            address = data.getStringExtra("address") != null ? data.getStringExtra("address") : "";
            lat = data.getDoubleExtra("lat", 0);
            lng = data.getDoubleExtra("lng", 0);
            viewModel.getRegisterRequest().setLat(lat);
            viewModel.getRegisterRequest().setLng(lng);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Codes.PERMISSION_IMAGE_REQUEST_CODE:
                    if (grantResults[0] == PERMISSION_GRANTED) {
                        FileOperations.pickImage(getActivity(), Codes.SELECT_PROFILE_IMAGE);
                    }
                    break;
            }//end switch
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onChanged(Object result) {
        if (result.equals(View.VISIBLE) || result.equals(View.GONE)) {
            showProgressBar((Integer) result);
        } else if (result.equals(Codes.SELECT_PROFILE_IMAGE)) {
            pickImage(Codes.SELECT_PROFILE_IMAGE);
        } else if (result.equals(Codes.SHOW_MESSAGE)) {
            showMessage(viewModel.getMessage());
        } else if (result.equals(Codes.TERMS_TEXT_CLICK)) {
            MovementManager.startActivity(getContext(), DialogActivity.class);
        } else if (result.equals(Codes.ENTER_CODE_SCREEN)) {
            MovementManager.startActivity(getActivity(), Codes.ENTER_CODE_SCREEN);
        } else if (result.equals(Codes.PRESS_BACK)) {
            MovementManager.startActivity(getContext(), Codes.LOGIN_SCREEN);
        }
    }

    private void openMapScreen() {
        Intent intent = new Intent(getContext(), BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE, Codes.MAP_SCREEN);
        startActivityForResult(intent, Codes.BUNDLE_RESULT_CODE);
    }
}
