package grand.shopness.view.ui.fragment.login;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentForgotPasswordBinding;
import grand.shopness.viewmodel.fragment.login.ForgotPasswordViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPasswordFragment extends BaseFragment implements Observer<Object> {

    FragmentForgotPasswordBinding binding;
    ForgotPasswordViewModel forgotPasswordViewModel;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false);
        forgotPasswordViewModel = new ForgotPasswordViewModel();
        binding.setViewModel(forgotPasswordViewModel);
        forgotPasswordViewModel.getMutableLiveData().observe(this, this);
        return binding.getRoot();
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressBar(result);
        }
        switch (result) {
            case Codes.ENTER_CODE_SCREEN:
                MovementManager.startActivity(getContext(), Codes.ENTER_CODE_SCREEN);
                break;
            case Codes.LOGIN_SCREEN:
                MovementManager.startActivity(getContext(), Codes.LOGIN_SCREEN);
                break;
            case Codes.SHOW_MESSAGE:
                showMessage(forgotPasswordViewModel.getMessage());
                break;
        }
    }
}
