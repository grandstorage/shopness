package grand.shopness.view.ui.fragment.main.sidemenu.accounts;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import grand.shopness.R;
import grand.shopness.base.MovementManager;
import grand.shopness.base.UserPreferenceHelper;
import grand.shopness.base.constantsutils.Codes;
import grand.shopness.base.constantsutils.Params;
import grand.shopness.base.constantsutils.WebServices;
import grand.shopness.base.filesutils.FileOperations;
import grand.shopness.base.filesutils.VolleyFileObject;
import grand.shopness.base.view.BaseActivity;
import grand.shopness.base.view.BaseFragment;
import grand.shopness.databinding.FragmentProfileBinding;
import grand.shopness.viewmodel.fragment.main.ProfileViewModel;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements Observer<Object> {

    FragmentProfileBinding profileBinding;
    ProfileViewModel viewModel;
    private double lat, lng;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        profileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        settingViewModel();
        initUI();
        return profileBinding.getRoot();
    }

    private void initUI() {
        profileBinding.tilLocation.setEndIconOnClickListener(v -> openMapScreen());
    }

    private void openMapScreen() {
        Intent intent = new Intent(getContext(), BaseActivity.class);
        intent.putExtra(Params.INTENT_PAGE, Codes.MAP_SCREEN);
        startActivityForResult(intent, Codes.BUNDLE_RESULT_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Codes.SELECT_PROFILE_IMAGE) {
            addToVolleyObj(data);
        } else if (requestCode == Codes.ALERT_DIALOGUE_LOCATION_REQ) {
            if (resultCode == RESULT_OK) {
                Timber.d("result ok");
            } else if (resultCode == RESULT_CANCELED) {
                Timber.d("result cancel");
            }
        } else if (requestCode == Codes.BUNDLE_RESULT_CODE) {
            setData(data);
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void addToVolleyObj(Intent data) {
        VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, WebServices.USER_IMAGE,
                Codes.TYPE_IMAGE_REQUEST_CODE);
        profileBinding.imgProfile.setImageBitmap(Objects.requireNonNull(volleyFileObject).getCompressObject().getImage());
        viewModel.getVolleyFileObjects().add(volleyFileObject);
    }

    private void setData(Intent data) {
        if (data != null) {
            lat = data.getDoubleExtra("lat", 0);
            lng = data.getDoubleExtra("lng", 0);
            UserPreferenceHelper.getUserLoginDetails().setLat(String.valueOf(lat));
            UserPreferenceHelper.getUserLoginDetails().setLng(String.valueOf(lng));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            if (requestCode == Codes.PERMISSION_IMAGE_REQUEST_CODE) {
                if (grantResults[0] == PERMISSION_GRANTED) {
                    FileOperations.pickImage(getActivity(), Codes.SELECT_PROFILE_IMAGE);
                }
            }//end switch
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void settingViewModel() {
        viewModel = new ProfileViewModel();
        profileBinding.setViewModel(viewModel);
        viewModel.getMutableLiveData().observe(this, this);
    }

    @Override
    public void onChanged(Object o) {
        int result = (Integer) o;
        if (result == View.VISIBLE || result == View.GONE) {
            showProgressBar(result);
        } else if (result == Codes.SHOW_MESSAGE) {
            showMessage(viewModel.getMessage());
        }else if(result == Codes.CHANGE_PASSWORD_SCREEN){
            MovementManager.startMainActivity(getContext(), result);
        }else if(result == Codes.SELECT_PROFILE_IMAGE){
            pickImage(result);
        }
    }
}
